package lkc_parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import lkc_parser.hardresults.Results;
import lkc_parser.kerneltools.Linux;
import lkc_parser.kerneltools.GNUMake.MakeVersionException;
import lkc_parser.kerneltools.Linux.LinuxException;
import lkc_parser.parser.Arch;

public class Main {
	private static final Logger errorLogger = Logger.getLogger("");
	private static final ConsoleHandler consoleHandler = new ConsoleHandler();
	private static final Formatter errorFormatter = new SimpleFormatter();

	static {
		for (Handler handler : errorLogger.getHandlers()) {
			errorLogger.removeHandler(handler);
		}
		consoleHandler.setFormatter(errorFormatter);
		consoleHandler.setLevel(Settings.MAX_LOG_LEVEL);
		errorLogger.addHandler(consoleHandler);
		errorLogger.setLevel(Settings.MAX_LOG_LEVEL);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException,
			MakeVersionException, LinuxException {
		final File target = new File(System.getProperty("user.dir")).getCanonicalFile();
		Settings.parse(args);

		if (Settings.isHelp()) {
			Settings.usage();
			System.exit(0);
		}

		errorLogger.setLevel(Settings.getLogLevel());
		Results.resultsLogger.setLevel(Settings.getLogLevel());
		Settings.setBasePath(target.getPath());

		if (Settings.isFileMode()) {
			String rootFileName = Settings.getRootFileName();
			File rootFile = new File(rootFileName);

			if (!rootFile.isAbsolute()) {
				rootFile = new File(target, rootFileName);
			}

			if (Settings.isLinuxMode()) {
				Arch.setKernelVersionString(Settings.getKernelVersion());
			}

			processFile(rootFile);
		} else if (Settings.isLinuxMode()) {
			Arch.setKernelVersionString(Linux.getKernelVersion());
			processDirectory(target);
		} else {
			errorLogger.log(Level.SEVERE, "need explicit root file when not in Linux mode");
			System.exit(-1);
		}
		Results.printResults();
	}

	private static void process(ArrayList<Arch> arches) {
		for (Arch arch : new TreeSet<Arch>(arches)) {
			//Results.resultsLogger.log(Level.INFO, arch.toString() + " " + tpof.numberOfAtoms()
			//		+ " " + tpof.numberOfVars() + " " + tpof.numberOfAuxVars() + " " + ppof.numberOfAtoms() + " " + ppof.numberOfVars());

			try {
				arch.calculateAll();
			} catch (FileNotFoundException e) {
				errorLogger.log(Level.WARNING, "entry point " + arch.getEntryPoint()
						+ " for architecture " + arch.getName() + " not found");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				errorLogger.log(Level.SEVERE,
						"architecture " + arch.getName() + ": " + e.toString());
				e.printStackTrace();
			} catch (InstantiationException e) {
				errorLogger.log(Level.SEVERE,
						"architecture " + arch.getName() + ": " + e.toString());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				errorLogger.log(Level.SEVERE,
						"architecture " + arch.getName() + ": " + e.toString());
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				errorLogger.log(Level.SEVERE,
						"architecture " + arch.getName() + ": " + e.toString());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void processFile(File lkcRootFile) throws FileNotFoundException {
		if (!lkcRootFile.isFile()) {
			throw new FileNotFoundException();
		}
		final ArrayList<Arch> arches = new ArrayList<Arch>();

		arches.add(new Arch("N_A", lkcRootFile));
		process(arches);
	}

	private static void processDirectory(File lkcRootDirectory) throws IOException,
			ClassNotFoundException, MakeVersionException, LinuxException {
		if (!lkcRootDirectory.isDirectory()) {
			throw new FileNotFoundException();
		}
		final HashMap<String, File> entryFiles = Linux.detectEntryFiles(lkcRootDirectory);
		final ArrayList<Arch> arches = new ArrayList<Arch>();

		for (Entry<String, File> e : entryFiles.entrySet()) {
			arches.add(new Arch(e.getKey(), e.getValue()));
		}

		process(arches);
		Results.setDetectedArchitectures(new TreeSet<Arch>(arches));
	}
}
