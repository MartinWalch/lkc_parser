package lkc_parser.testingtools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;
import lkc_parser.tristate.TriSymbol;
import lkc_parser.tristate.TristateConstant;
import lkc_parser.tristatestar.TriSEquivalence;
import lkc_parser.zenglermodel2010.Type;

public class FormulaTools {
	public static void createIndividiualSymbolAssignments(TypesMap typesMap, PVariableDatabase vDB,
			LinkedHashMap<String, Integer> exportTable, File directory) throws IOException {
		for (Entry<String, Type> entry : typesMap.entrySet()) {
			final String symbolName = entry.getKey();
			final Type symbolType = entry.getValue();

			switch (symbolType) {
			case TRISTATE:
				TriSEquivalence e = new TriSEquivalence(new TriSymbol(symbolName),
						TristateConstant.YES);
				PFormula formula = e.calcPi0(typesMap, vDB);
				File file = new File(directory, e.toString());
				exportTable.get(vDB.getP0(symbolName).name);
				exportTable.get(vDB.getP1(symbolName).name);

				BufferedWriter writer = new BufferedWriter(new FileWriter(file));

				System.err.println(new TriSEquivalence(new TriSymbol(symbolName),
						TristateConstant.MOD));
				System.err.println(new TriSEquivalence(new TriSymbol(symbolName),
						TristateConstant.NO));
			}
		}
	}
}
