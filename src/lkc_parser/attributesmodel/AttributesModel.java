package lkc_parser.attributesmodel;

import lkc_parser.parser.Arch;
import lkc_parser.zenglermodel2010.ZenglerModel;

public class AttributesModel {
	private final ZenglerModel origin;
	private final AttributesSymbolDatabase symbols = new AttributesSymbolDatabase();
	private final AttributesChoiceDatabase choiceGroups = new AttributesChoiceDatabase();
	private final TypesMap typesMap = new TypesMap();

	public AttributesModel(ZenglerModel origin) {
		this.origin = origin;
	}

//	private ExpressionSymbol fixBoundary(Type type, ExpressionSymbol boundary,
//			HashMap<String, AttributesSymbol> symbols, SymbolDescriptor desc) {
//		assert (type == Type.INT || type == Type.HEX);
//		ExpressionSymbol res = boundary;
//
//		if (boundary instanceof VariableSymbol) {
//			String boundaryName = ((VariableSymbol) boundary).value();
//			AttributesSymbol boundSymbol = symbols.get(boundaryName);
//			if (boundSymbol != null) {
//				if (!((boundSymbol.getType() == Type.INT) || (boundSymbol.getType() == Type.HEX))) {
//					Results.badBoundarySymbol(arch, boundSymbol, desc);
//					res = ZERO;
//				} else if (boundSymbol.getType() != type) {
//					Results.conversionOnBoundarySymbol(arch, boundSymbol, desc);
//				}
//			} else {
//				final String constVal = strtoll(boundaryName, type);
//
//				if (constVal != null) {
//					if (!looksLikeNumber(boundaryName, type)) {
//						// TODO: warn in Results
//						System.err.println("IMPLEMENT ME");
//					}
//					res = new ConstantSymbol(constVal);
//				} else {
//					Results.undeclaredBoundarySymbol(arch, boundaryName, desc);
//					// res = ConstantSymbol.UNSET;
//				}
//			}
//		}
//
//		// there are two places where numerical values are constrained in the
//		// original parser:
//		// - symbol.c: sym_string_within_range, converts input with strtoll
//		// - symbol.c: sym_string_valid, checking individual chars
//		else {
//			final String value = ((ConstantSymbol) res).value();
//			final String valueProcessed = strtoll(value, type);
//
//			if (valueProcessed != null) {
//				if (!looksLikeNumber(value, type)) {
//					// TODO: warn
//					System.err.println("IMPLEMENT ME");
//				}
//
//				res = new ConstantSymbol(valueProcessed);
//			} else {
//				res = ZERO;
//			}
//		}
//		if (res instanceof ConstantSymbol) {
//			if (type == Type.HEX) {
//				String normalisedValue = res.value().toLowerCase();
//				if (!(normalisedValue.charAt(0) == '0' && normalisedValue.charAt(1) == 'x')) {
//					normalisedValue = "0x" + normalisedValue;
//					res = new ConstantSymbol(normalisedValue);
//				}
//			}
//		}
//		return res;
//	}

//	private static String strtoll(String string, Type type) {
//		// TODO: implement
//		// this is quick & dirty to make things work somehow, but to be
//		// accurate, an adequate simulation of strtoll is needed
//		String res;
//		if (!looksLikeNumber(string, type)) {
//			return null;
//		}
//		if (type == Type.INT) {
//			res = string;
//		} else {
//			res = string;
//		}
//		return res;
//	}

	public Arch getArch() {
		return origin.getArch();
	}

	public AttributesSymbolDatabase getSymbolDatabase() {
		return symbols;
	}

	public AttributesChoiceDatabase getChoiceDatabase() {
		return choiceGroups;
	}

	public TypesMap getTypesMap() {
		return typesMap;
	}

	public void dump() {
		symbols.dump();
		choiceGroups.dump();
	}
}
