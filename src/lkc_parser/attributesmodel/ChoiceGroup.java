package lkc_parser.attributesmodel;

import java.util.ArrayList;
import java.util.Collection;

import lkc_parser.zenglermodel2010.ChoiceDescriptor;
import lkc_parser.zenglermodel2010.PromptProperty;
import lkc_parser.zenglermodel2010.SymbolDescriptor;
import lkc_parser.zenglermodel2010.Type;

public class ChoiceGroup {
	private final Type t;
	private final ChoiceDescriptor origin;
	private final boolean optional;
	private final Prompt prompt;
	private final ArrayList<String> symbolNames = new ArrayList<String>();

	public ChoiceGroup(Type t, ChoiceDescriptor choiceDescriptor,
			Collection<SymbolDescriptor> descriptors, AttributesSymbolDatabase symbols) {
		ArrayList<PromptProperty> promptPropertiesList = choiceDescriptor.getPromptProperties();
		PromptProperty promptProperty = null;

		this.t = t;
		this.origin = choiceDescriptor;
		this.optional = choiceDescriptor.isOptional();

		for (PromptProperty property : promptPropertiesList) {
			if (promptProperty == null) {
				promptProperty = property;
			} else {
				if (property.getIndex() < promptProperty.getIndex()) {
					promptProperty = property;
				}
			}
		}

		if (promptProperty != null) {
			this.prompt = new Prompt(promptProperty, choiceDescriptor.getDependencies());
		} else {
			System.err.println("(WW)");
			this.prompt = null;
		}

		for (SymbolDescriptor desc : descriptors) {
			final String symbolName = desc.getSymbolName();
			final Type symbolType = symbols.get(symbolName).getType();
			if ((symbolType == Type.BOOL) || (symbolType == Type.TRISTATE)) {
				symbolNames.add(symbolName);
			} else {
				// TODO
				System.err.println("(WW)");
			}
		}
	}

	public Type getT() {
		return t;
	}

	public ChoiceDescriptor getOrigin() {
		return origin;
	}

	public boolean isOptional() {
		return optional;
	}

	public Prompt getPrompt() {
		return prompt;
	}

	public ArrayList<String> getSymbolNames() {
		return symbolNames;
	}

	public void dump() {
		System.out.print("(" + t + ", " + (optional ? 1 : 0) + ", ");
		prompt.dump();
		System.out.print(", {");
		for (String s : symbolNames) {
			System.out.print(s);
			System.out.print(", ");
		}
		System.out.print("})");
	}
}
