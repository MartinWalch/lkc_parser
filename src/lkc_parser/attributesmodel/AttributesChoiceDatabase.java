package lkc_parser.attributesmodel;

import java.util.ArrayList;

public class AttributesChoiceDatabase extends ArrayList<ChoiceGroup> {

	public void dump() {
		System.out.println("{");
		for (ChoiceGroup choiceGroup : this) {
			choiceGroup.dump();
			System.out.println(", ");
		}
		System.out.println("}");
	}
}
