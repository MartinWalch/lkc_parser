package lkc_parser.attributesmodel;

import static lkc_parser.tristate.TristateConstant.MOD;
import static lkc_parser.tristate.TristateConstant.NO;
import static lkc_parser.tristate.TristateConstant.YES;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import lkc_parser.hardresults.Results;
import lkc_parser.tokentree.Context;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.ExpressionsList;
import lkc_parser.tristate.TriAnd;
import lkc_parser.tristate.TriEqual;
import lkc_parser.tristate.TriSymbol;
import lkc_parser.tristate.TriUnequal;
import lkc_parser.zenglermodel2010.ChoiceDescriptor;
import lkc_parser.zenglermodel2010.DefaultProperty;
import lkc_parser.zenglermodel2010.PromptProperty;
import lkc_parser.zenglermodel2010.RangeProperty;
import lkc_parser.zenglermodel2010.SelectProperty;
import lkc_parser.zenglermodel2010.SymbolDescriptor;
import lkc_parser.zenglermodel2010.Type;
import lkc_parser.zenglermodel2010.ZenglerChoiceDatabase;
import lkc_parser.zenglermodel2010.ZenglerDependencies;
import lkc_parser.zenglermodel2010.ZenglerModel;
import lkc_parser.zenglermodel2010.ZenglerSymbol;
import lkc_parser.zenglermodel2010.ZenglerSymbolDatabase;

public class AttributesModelFactory {
	private static AttributesSymbolDatabase symbols = null;
	private static AttributesChoiceDatabase choiceGroups = null;
	private static TypesMap typesMap = null;

	public static AttributesModel produce(ZenglerModel zenglerModel) {
		final ZenglerSymbolDatabase symbolDatabase = zenglerModel.getSymbolDatabase();
		final ZenglerChoiceDatabase choiceDatabase = zenglerModel.getChoiceDatabase();
		final HashMap<String, ZenglerSymbol> zenglerSymbols = symbolDatabase.getDatabase();
		final AttributesModel res = new AttributesModel(zenglerModel);
		final Results results = Results.getResults(zenglerModel.getArch());

		symbols = res.getSymbolDatabase();
		choiceGroups = res.getChoiceDatabase();
		typesMap = res.getTypesMap();

		for (ZenglerSymbol zenglerSymbol : zenglerSymbols.values()) {
			ArrayList<SymbolDescriptor> descriptors = zenglerSymbol.getDescriptors();
			SymbolDescriptor typeDescriptor = null;
			Type t;

			for (SymbolDescriptor desc : descriptors) {
				if (desc.getType() != Type.UNKNOWN) {
					if (typeDescriptor == null) {
						typeDescriptor = desc;
					} else {
						if (desc.getIndex() < typeDescriptor.getIndex()) {
							typeDescriptor = desc;
						}
					}
				}
			}

			if (typeDescriptor != null) {
				t = typeDescriptor.getType();
			} else {
				t = Type.UNKNOWN;
			}

			symbols.put(zenglerSymbol.getName(), new AttributesSymbol(zenglerSymbol, t));
		}

		for (final Entry<String, AttributesSymbol> entry : symbols.entrySet()) {
			final String name = entry.getKey();
			final ZenglerSymbol zenglerSymbol = zenglerSymbols.get(name);
			final AttributesSymbol attributesSymbol = entry.getValue();
			final Type symbolType = attributesSymbol.getType();

			for (SymbolDescriptor desc : zenglerSymbol.getDescriptors()) {
				ZenglerDependencies dep = desc.getDependencies();
				ArrayList<PromptProperty> promptPropertiesList = desc.getPromptProperties();
				PromptProperty promptProperty = null;

				for (PromptProperty property : promptPropertiesList) {
					if (promptProperty == null) {
						promptProperty = property;
					} else {
						if (property.getIndex() < promptProperty.getIndex()) {
							promptProperty = property;
						}
					}
				}

				if (promptProperty != null) {
					attributesSymbol.appendPrompt(new Prompt(promptProperty, dep));
				}

				for (DefaultProperty defaultProperty : desc.getDefaultProperties()) {
					attributesSymbol.appendDefault(new SymbolDefault(defaultProperty, dep));
				}

				for (SelectProperty selectProperty : desc.getSelectProperties()) {
					final String selectedSymbolName = selectProperty.getSymbol();
					final AttributesSymbol selectedSymbol = symbols.get(selectedSymbolName);

					if (selectedSymbol != null) {
						selectedSymbol.addSelect(new Select(selectProperty, dep, name));
					} else {
						results.undeclaredSymbolSelected(selectedSymbolName,
								selectProperty.getLocation());
					}
				}

				for (RangeProperty rangeProperty : desc.getRangeProperties()) {
					if ((symbolType == Type.INT) || (symbolType == Type.HEX)) {
						attributesSymbol.appendRange(new Range(rangeProperty, desc, symbols,
								results));
					} else {
						results.badTypeWithRange(zenglerSymbol, desc);
					}
				}
			}
		}

		for (ChoiceDescriptor choiceDescriptor : choiceDatabase.getDatabase()) {
			ArrayList<SymbolDescriptor> descriptors = choiceDescriptor.getSymbolDescriptors();
			Type t = choiceDescriptor.getType();
			Iterator<SymbolDescriptor> descIter = descriptors.iterator();
			LinkedList<TriSymbol> stack = new LinkedList<TriSymbol>();
			SymbolDescriptor typeDefiningDescriptor = null;

			if (descriptors.size() == 0) {
				continue;
			}

			// the first type declaration of the first symbol with a type is
			// decisive. ↑menu.c:menu_finalize(struct menu *parent)
			// we can be pretty sure that this is not HEX or INT as this just
			// segfaults LKC
			// an accidently assigned STRING force-deactivates the menu
			// nb: this must be done *before* any symbol is moved into
			// a submenu. Yes, that is right: the type may be inherited
			// from a symbol that is not directly part of any choice option.

			while ((t == Type.UNKNOWN) && descIter.hasNext()) {
				SymbolDescriptor currentDesc = descIter.next();
				String currentSymbolName = currentDesc.getSymbolName();
				AttributesSymbol attributesSymbol = symbols.get(currentSymbolName);
				Type symbolType = attributesSymbol.getType();

				if (symbolType != Type.UNKNOWN) {
					ZenglerSymbol zenglerSymbol = zenglerSymbols.get(currentSymbolName);

					t = symbolType;
					for (SymbolDescriptor descriptor : zenglerSymbol.getDescriptors()) {
						if (descriptor.getType() != Type.UNKNOWN) {
							typeDefiningDescriptor = descriptor;
							break;
						}
					}
					if ((typeDefiningDescriptor == null) || (typeDefiningDescriptor != currentDesc)) {
						Results.getResults(zenglerModel.getArch()).choiceTypeExternallySet(
								choiceDescriptor, currentDesc);
					}
				}
			}
			// sic!
			// a symbol type may be propagated across distinct choice blocks
			// with the consequence that a) the type of the whole choice block
			// depends on the type of another choice block and b) case a)
			// depends on the sort order of the choice blocks.
			switch (t) {
			case BOOL:
				//$FALL-THROUGH$
			case TRISTATE:
				//$FALL-THROUGH$
			case STRING:
				propagateType(t, descriptors);
				// do *not* create choiceGroups here as type propagation
				// may not be completely settled
				break;
			default:
			}

			descIter = descriptors.iterator();

			while (descIter.hasNext()) {
				TriSymbol variableSymbol;
				ZenglerDependencies dependencies;
				ExpressionsList expressionsList;
				SymbolDescriptor current = descIter.next();

				variableSymbol = new TriSymbol(current.getSymbolName());
				dependencies = current.getDependencies();
				expressionsList = Context.toExpressionsList(dependencies.getContexts());

				while (!stack.isEmpty()
						&& !expr_depends_symbol(expressionsList.andConcatenation(), stack.peek())) {
					stack.pop();
				}
				if (!stack.isEmpty()) {
					descIter.remove();
				}

				stack.push(variableSymbol);
			}
			if (!((t == Type.BOOL) || (t == Type.TRISTATE))) {
				Results.getResults(zenglerModel.getArch()).badChoiceType(choiceDescriptor, t);
			} else {
				choiceGroups.add(new ChoiceGroup(t, choiceDescriptor, descriptors, symbols));
			}
		}

		for (Entry<String, AttributesSymbol> symbolEntry : symbols.entrySet()) {
			String symbolName = symbolEntry.getKey();
			Type symbolType = symbolEntry.getValue().getType();

			if (symbolType.isValid) {
				typesMap.put(symbolName, symbolType);
			}
		}
		return res;
	}

	private static boolean expr_depends_symbol(Expression dep, TriSymbol sym) {
		if (dep == null)
			return false;

		if (dep instanceof TriAnd) {
			return expr_depends_symbol(((TriAnd) dep).left, sym)
					|| expr_depends_symbol(((TriAnd) dep).right, sym);
		} else if (dep instanceof TriSymbol) {
			return ((TriSymbol) dep).equals(sym);
		} else if (dep instanceof TriEqual) {
			if (((TriEqual) dep).left.equals(sym)) {
				if (((TriEqual) dep).right.equals(YES) || ((TriEqual) dep).right.equals(MOD)) {
					return true;
				}
			}
		} else if (dep instanceof TriUnequal) {
			if (((TriUnequal) dep).left.equals(sym)) {
				if (((TriUnequal) dep).right.equals(NO)) {
					return true;
				}
			}
		}
		return false;
	}

	private static void propagateType(final Type t, final Collection<SymbolDescriptor> descriptors) {
		for (SymbolDescriptor desc : descriptors) {
			String symbolName = desc.getSymbolName();
			AttributesSymbol symbol = symbols.get(symbolName);
			if (symbol.getType() == Type.UNKNOWN) {
				symbol.setType(t);
			}
		}
	}
}
