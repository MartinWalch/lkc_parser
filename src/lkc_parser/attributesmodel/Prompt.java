package lkc_parser.attributesmodel;

import lkc_parser.tokentree.Context;
import lkc_parser.tristate.ExpressionsList;
import lkc_parser.zenglermodel2010.PromptProperty;
import lkc_parser.zenglermodel2010.ZenglerDependencies;

public class Prompt extends Attribute {
	private final ExpressionsList dependencies;

	public Prompt(PromptProperty promptProperty, ZenglerDependencies dependencies) {
		super(promptProperty);
		this.dependencies = Context.toExpressionsList(dependencies.getContexts());
		if (promptProperty.getGuard() != null) {
			this.dependencies.add(promptProperty.getGuard());
		}
	}

	public ExpressionsList getDependencies() {
		return dependencies;
	}

	public void dump() {
		System.out.print("(");
		ExpressionsList.dump(dependencies);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
