package lkc_parser.attributesmodel;

import java.util.ArrayList;

import lkc_parser.hardresults.Results;
import lkc_parser.parser.Arch;
import lkc_parser.zenglermodel2010.OptionBitVector;
import lkc_parser.zenglermodel2010.SymbolDescriptor;
import lkc_parser.zenglermodel2010.Type;
import lkc_parser.zenglermodel2010.ZenglerSymbol;

//TODO: add origin of type so we can check in choice groups with implicit type
// where a type originates from
public class AttributesSymbol {
	private final String s;
	private Type t;
	private final ArrayList<Prompt> prompts = new ArrayList<Prompt>();
	private final ArrayList<Select> selects = new ArrayList<Select>();
	private final ArrayList<SymbolDefault> defaults = new ArrayList<SymbolDefault>();
	private final ArrayList<Range> ranges = new ArrayList<Range>();
	private final OptionBitVector opt;

	private final ZenglerSymbol origin;

	public AttributesSymbol(ZenglerSymbol zenglerSymbol, Type type) {
		OptionBitVector finalOpt = new OptionBitVector();

		this.origin = zenglerSymbol;
		this.s = zenglerSymbol.getName();
		this.t = type;

		for (SymbolDescriptor desc : zenglerSymbol.getDescriptors()) {
			finalOpt = finalOpt.or(desc.getOpt());
		}

		this.opt = finalOpt;
	}

	public Arch getArch() {
		return origin.getArch();
	}

	public void appendPrompt(Prompt prompt) {
		prompts.add(prompt);
	}

	public void addSelect(Select select) {
		if (!((getType() == Type.BOOL) || (getType() == Type.TRISTATE))) {
			Results.getResults(getArch()).selectOnWrongType(origin, select.getLocation());
		} else {
			selects.add(select);
		}
	}

	public void appendDefault(SymbolDefault symbolDefault) {
		defaults.add(symbolDefault);
	}

	public void appendRange(Range range) {
		ranges.add(range);
	}

	public String getName() {
		return s;
	}

	public void setType(Type type) {
		this.t = type;
	}

	public Type getType() {
		return t;
	}

	public ArrayList<Prompt> getPrompts() {
		return prompts;
	}

	public ArrayList<Select> getSelects() {
		return selects;
	}

	public ArrayList<SymbolDefault> getDefaults() {
		return defaults;
	}

	public ArrayList<Range> getRanges() {
		return ranges;
	}

	public OptionBitVector getOpt() {
		return opt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((s == null) ? 0 : s.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributesSymbol other = (AttributesSymbol) obj;
		if (s == null) {
			if (other.s != null)
				return false;
		} else if (!s.equals(other.s))
			return false;
		return true;
	}

	public void dump() {
		System.out.print("(" + s + ", " + t + ", ");
		System.out.print("{");
		for (Prompt prompt : prompts) {
			prompt.dump();
		}
		System.out.print("}, {");
		for (Select select : selects) {
			select.dump();
		}
		System.out.print("}, {");
		for (SymbolDefault def : defaults) {
			def.dump();
		}
		System.out.print("}, {");
		for (Range range : ranges) {
			range.dump();
		}
		System.out.print("}, ");
		opt.dump();
		System.out.print("})");
	}
}
