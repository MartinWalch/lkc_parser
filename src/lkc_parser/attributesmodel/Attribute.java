package lkc_parser.attributesmodel;

import lkc_parser.tokentree.Location;
import lkc_parser.zenglermodel2010.ZenglerProperty;

public abstract class Attribute {
	private final ZenglerProperty origin;
	private final int index;

	public Attribute(ZenglerProperty origin) {
		this.origin = origin;
		this.index = origin.getIndex();
	}

	public Location getLocation() {
		return origin.getLocation();
	}

	public int getIndex() {
		return index;
	}
}
