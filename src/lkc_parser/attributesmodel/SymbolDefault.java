package lkc_parser.attributesmodel;

import lkc_parser.tokentree.Context;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.ExpressionsList;
import lkc_parser.zenglermodel2010.DefaultProperty;
import lkc_parser.zenglermodel2010.ZenglerDependencies;

public class SymbolDefault extends Attribute {
	private final ExpressionsList dependencies;
	private final Expression value;

	public SymbolDefault(DefaultProperty defaultProperty, ZenglerDependencies dependencies) {
		super(defaultProperty);
		Expression guard = defaultProperty.getGuard();

		this.dependencies = Context.toExpressionsListSkipVisible(dependencies.getContexts());
		if (guard != null) {
			this.dependencies.add(guard);
		}

		this.value = defaultProperty.getValue();
	}

	public ExpressionsList getDependencies() {
		return dependencies;
	}

	public Expression getValue() {
		return value;
	}

	public void dump() {
		System.out.print("(");
		ExpressionsList.dump(dependencies);
		System.out.print(", ");
		Expression.dump(value);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
