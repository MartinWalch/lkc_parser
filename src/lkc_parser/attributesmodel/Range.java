package lkc_parser.attributesmodel;

import static lkc_parser.parser.data.Symbol.looksLikeHex;
import static lkc_parser.parser.data.Symbol.looksLikeInt;

import lkc_parser.hardresults.Results;
import lkc_parser.tokentree.Context;
import lkc_parser.tokentree.Location;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.ExpressionSymbol;
import lkc_parser.tristate.ExpressionsList;
import lkc_parser.zenglermodel2010.RangeProperty;
import lkc_parser.zenglermodel2010.SymbolDescriptor;
import lkc_parser.zenglermodel2010.Type;
import lkc_parser.zenglermodel2010.ZenglerDependencies;

public class Range extends Attribute {
	private final ExpressionsList dependencies;
	private final ExpressionSymbol from;
	private final ExpressionSymbol to;

	public Range(RangeProperty rangeProperty, SymbolDescriptor desc,
			AttributesSymbolDatabase symbols, Results results) {
		super(rangeProperty);

		final ZenglerDependencies dep = desc.getDependencies();
		final Location location = rangeProperty.getLocation();
		from = rangeProperty.getFrom();
		to = rangeProperty.getTo();

		this.dependencies = Context.toExpressionsListSkipVisible(dep.getContexts());

		if (rangeProperty.getGuard() != null) {
			this.dependencies.add(rangeProperty.getGuard());
		}

		checkBoundary(from, location, desc, symbols, results);
		checkBoundary(to, location, desc, symbols, results);
	}

	private static void checkBoundary(ExpressionSymbol in, Location location,
			SymbolDescriptor desc, AttributesSymbolDatabase symbols, Results results) {
		final Type symbolType = symbols.get(desc.getSymbolName()).getType();
		final String inName = in.name;
		final AttributesSymbol inSymbol = symbols.get(inName);

		if (inSymbol == null) {
			if ((symbolType == Type.INT) && !looksLikeInt(inName)
					|| ((symbolType == Type.HEX) && !looksLikeHex(inName))) {
				results.undeclaredBoundarySymbol(inName, location);
			}
			// res = new StringConstant(inName);
		} else {
			Type inType = inSymbol.getType();
			switch (inType) {
			case INT:
			case HEX:
				if (inType != symbolType) {
					results.conversionOnBoundarySymbol(symbols.get(inName), location);
				}
				break;
			default:
				results.badBoundarySymbol(symbols.get(inName), location);
			}
		}
	}

	public ExpressionsList getDependencies() {
		return dependencies;
	}

	public ExpressionSymbol getFrom() {
		return from;
	}

	public ExpressionSymbol getTo() {
		return to;
	}

	public void dump() {
		System.out.print("(");
		ExpressionsList.dump(dependencies);
		System.out.print(", ");
		Expression.dump(from);
		System.out.print(", ");
		Expression.dump(to);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
