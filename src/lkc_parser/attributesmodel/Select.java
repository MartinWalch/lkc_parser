package lkc_parser.attributesmodel;

import lkc_parser.tokentree.Context;
import lkc_parser.tristate.ExpressionsList;
import lkc_parser.zenglermodel2010.SelectProperty;
import lkc_parser.zenglermodel2010.ZenglerDependencies;

public class Select extends Attribute {
	private final ExpressionsList dependencies;
	private final String selectingSymbol;

	public Select(SelectProperty selectProperty, ZenglerDependencies dependencies,
			String selectingSymbol) {
		super(selectProperty);

		this.dependencies = Context.toExpressionsListSkipVisible(dependencies.getContexts());

		if (selectProperty.getGuard() != null) {
			this.dependencies.add(selectProperty.getGuard());
		}

		this.selectingSymbol = selectingSymbol;
	}

	public ExpressionsList getDependencies() {
		return dependencies;
	}

	public String getSelectingSymbol() {
		return selectingSymbol;
	}

	public void dump() {
		System.out.print("(");
		ExpressionsList.dump(dependencies);
		System.out.print(", ");
		System.out.print(selectingSymbol);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
