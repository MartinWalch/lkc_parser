package lkc_parser.attributesmodel;

import java.util.HashMap;

public class AttributesSymbolDatabase extends HashMap<String, AttributesSymbol> {

	public void dump() {
		System.out.println("{");
		for (AttributesSymbol attributesSymbol : this.values()) {
			attributesSymbol.dump();
			System.out.println(", ");
		}
		System.out.println("}");
	}
}
