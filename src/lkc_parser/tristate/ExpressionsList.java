package lkc_parser.tristate;

import static lkc_parser.tristate.TristateConstant.NO;
import static lkc_parser.tristate.TristateConstant.YES;

import java.util.ArrayList;
import java.util.Iterator;

public class ExpressionsList extends ArrayList<Expression> {

	public ExpressionsList(ExpressionsList l) {
		super(l);
	}

	public ExpressionsList() {
	}

	@Override
	public boolean add(Expression expression) {
		assert (expression != null);
		return super.add(expression);
	}

	public Expression andConcatenation() {
		if (size() == 0) {
			return YES;
		}
		final Iterator<Expression> iterator = iterator();
		Expression res = iterator.next();
		while (iterator.hasNext()) {
			res = new TriAnd(res, iterator.next());
		}

		return res;
	}

	public Expression orConcatenation() {
		Expression res;
		if (size() == 0) {
			res = NO;
		} else {
			Iterator<Expression> iterator = iterator();
			res = iterator.next();

			while (iterator.hasNext()) {
				res = new TriOr(res, iterator.next());
			}
		}
		return res;
	}

	public static void dump(ExpressionsList dependencies) {
		System.out.print("{");
		for (Expression e : dependencies) {
			Expression.dump(e);
			System.out.print(", ");
		}
		System.out.print("}");
	}
}
