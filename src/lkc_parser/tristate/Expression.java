package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;

public abstract class Expression {
	private PFormula resPi0 = null;
	private PFormula resPi1 = null;

	/**
	 * Replace any references to undeclared symbols by their names as strings.
	 * 
	 * A symbol reference is kept iff names contains the name of the symbol.
	 * 
	 * @param names
	 *            list of all declared symbol names
	 * @return an Expression with any references to symbols with names other
	 *         than those in names removed. May be the input object.
	 */
	public abstract boolean contains(ExpressionSymbol expressionSymbol);

	public abstract PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB);

	public abstract PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB);

	public PFormula pi0(TypesMap typesMap, PVariableDatabase vDB) {
		if (resPi0 == null) {
			resPi0 = calcPi0(typesMap, vDB);
			if (resPi0 == null) {
				throw new NullPointerException();
			}
		}
		return resPi0;
	}

	public PFormula pi1(TypesMap typesMap, PVariableDatabase vDB) {
		if (resPi1 == null) {
			resPi1 = calcPi1(typesMap, vDB);
		}
		return resPi1;
	}

	public static void dump(Expression formula) {
		if (formula == null) {
			System.out.print("null");
			return;
		}
		if (formula instanceof TriBinaryExpression) {
			TriBinaryExpression binaryExpression = (TriBinaryExpression) formula;
			System.out.print('(');
			dump(binaryExpression.left);
			System.out.print(' ');
			System.out.print(binaryExpression.operatorString());
			System.out.print(' ');
			dump(binaryExpression.right);
			System.out.print(')');
		} else if (formula instanceof TriUnaryExpression) {
			TriUnaryExpression unaryExpression = (TriUnaryExpression) formula;
			System.out.print(unaryExpression.operatorString());
			dump(unaryExpression.operand);
		} else if (formula instanceof TriVariable) {
			TriVariable variable = (TriVariable) formula;
			System.out.print(variable);
		} else {
			assert (formula instanceof TriConstantSymbol);
			TriConstantSymbol constantSymbol = (TriConstantSymbol) formula;
			System.out.print(constantSymbol);
		}
	}
}
