package lkc_parser.tristate;

public abstract class ExpressionSymbol extends TristateAtom {
	public final String name;

	public ExpressionSymbol(String name) {
		this.name = name;
		if (name == null) {
			throw new NullPointerException();
		}
	}

	@Override
	public boolean contains(ExpressionSymbol expressionSymbol) {
		return this.equals(expressionSymbol);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExpressionSymbol other = (ExpressionSymbol) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
