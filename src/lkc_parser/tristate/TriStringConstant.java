package lkc_parser.tristate;

import static lkc_parser.tristate.TristateConstant.MOD;
import static lkc_parser.tristate.TristateConstant.NO;
import static lkc_parser.tristate.TristateConstant.YES;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;

public class TriStringConstant extends TriConstantSymbol {
	public static final TriStringConstant ε = new TriStringConstant("");

	public TriStringConstant(String value) {
		super(value);
	}

	public TristateConstant toTristateConstant() {
		final TristateConstant res;

		if (name.equals(YES.name)) {
			res = YES;
		} else if (name.equals(MOD.name)) {
			res = MOD;
		} else {
			res = NO;
		}

		return res;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		throw new RuntimeException("this must never happen");
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		throw new RuntimeException("this must never happen");
	}
}
