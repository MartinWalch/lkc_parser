package lkc_parser.tristate;

import java.util.ArrayList;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PFormulasList;
import lkc_parser.propositional.POr;
import lkc_parser.propositional.PVariableDatabase;

public class TriAnd extends TriBinaryExpression implements TristateOperation {
	public TriAnd(Expression left, Expression right) {
		super(left, right);
		assert (left != null);
		assert (right != null);
	}

	@Override
	public String operatorString() {
		return "&&";
	}

	private static ArrayList<Expression> collectChain(Expression expression) {
		ArrayList<Expression> res;
		if (expression instanceof TriAnd) {
			TriAnd triAnd = (TriAnd) expression;
			res = collectChain(triAnd.left);
			res.addAll(collectChain(triAnd.right));
		} else {
			res = new ArrayList<Expression>();
			res.add(expression);
		}
		return res;
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		ArrayList<Expression> chain = collectChain(this);
		PFormulasList resList = new PFormulasList();

		for (Expression expression : chain) {
			resList.add(expression.pi0(typesMap, vDB));
		}
		return resList.andConcatenation();
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		ArrayList<Expression> chain = collectChain(this);
		PFormulasList resList = new PFormulasList();
		PFormulasList orList = new PFormulasList();

		for (Expression expression : chain) {
			PFormula expression_pi1 = expression.pi1(typesMap, vDB);
			resList.add(POr.produce(expression.pi0(typesMap, vDB), expression_pi1));
			orList.add(expression_pi1);
		}
		resList.add(orList.orConcatenation());

		return resList.andConcatenation();
	}
}
