package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PAnd;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PNot;
import lkc_parser.propositional.PVariableDatabase;

public class TriNot extends TriUnaryExpression implements TristateOperation {
	public TriNot(Expression operand) {
		super(operand);
	}

	@Override
	public String operatorString() {
		return "!";
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		return PAnd.produce(PNot.produce(operand.calcPi0(typesMap, vDB)),
				PNot.produce(operand.calcPi1(typesMap, vDB)));
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return operand.calcPi1(typesMap, vDB);
	}
}
