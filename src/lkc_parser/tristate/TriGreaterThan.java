package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PConstant;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;

public class TriGreaterThan extends TriBinaryExpression {

	public TriGreaterThan(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String operatorString() {
		return ">";
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return PConstant.BOTTOM;
	}
}
