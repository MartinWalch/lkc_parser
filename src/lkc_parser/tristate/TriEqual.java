package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PConstant;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;
import lkc_parser.tristatestar.TriSEquivalence;

public class TriEqual extends TriBinaryExpression implements TriStringOperation {
	public TriEqual(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String operatorString() {
		return "=";
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		TriSymbol var;
		TriConstantSymbol con;

		if (left instanceof TriSymbol) {
			if (right instanceof TriSymbol) {
				TriSymbol X1 = (TriSymbol) left;
				TriSymbol X2 = (TriSymbol) right;

				if (typesMap.get(X1.name).isStringLike) {
					if (!typesMap.get(X2.name).isStringLike) {
						throw new RuntimeException(
								"Comparison of string typed symbol and non-string typed symbol is not implemented.");
					}
					return vDB.getSymbolEqSymbol(X1.name, X2.name);
				}
				if (typesMap.get(X2.name).isStringLike) {
					throw new RuntimeException(
							"Comparison of string typed symbol and non-string typed symbol is not implemented.");
				}
				return new TriSEquivalence(X1, X2).calcPi0(typesMap, vDB);
			}
		}
		if ((left instanceof TriConstantSymbol) && (right instanceof TriConstantSymbol)) {
			return (left.equals(right)) ? PConstant.TOP : PConstant.BOTTOM;
		}

		if (left instanceof TriSymbol) {
			var = (TriSymbol) left;
			con = (TriConstantSymbol) right;
		} else {
			var = (TriSymbol) right;
			con = (TriConstantSymbol) left;
		}

		if (typesMap.get(var.name).isStringLike) {
			if (con instanceof TristateConstant) {
				con = ((TristateConstant) con).toStringConstant();
			}
			return vDB.getSymbolEqString(var.name, con.name);
		}

		if (!(con instanceof TristateConstant)) {
			assert (con instanceof TriStringConstant);
			con = TristateConstant.NO;
		}
		return new TriSEquivalence(var, con).calcPi0(typesMap, vDB);
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return PConstant.BOTTOM;
	}
}
