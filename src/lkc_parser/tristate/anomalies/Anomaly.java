package lkc_parser.tristate.anomalies;

public class Anomaly {
	private final String description;

	public Anomaly(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return description;
	}
}
