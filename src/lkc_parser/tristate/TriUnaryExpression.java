package lkc_parser.tristate;

public abstract class TriUnaryExpression extends Expression {
	public final Expression operand;

	public TriUnaryExpression(Expression operand) {
		this.operand = operand;
	}

	@Override
	public boolean contains(ExpressionSymbol expressionSymbol) {
		return operand.contains(expressionSymbol);
	}

	public abstract String operatorString();

	@Override
	public String toString() {
		return operatorString() + operand.toString();
	}
}
