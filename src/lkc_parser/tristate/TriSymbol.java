package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;

public class TriSymbol extends ExpressionSymbol implements TriVariable {
	public TriSymbol(String identifier) {
		super(identifier);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		return vDB.getP0(name);
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return vDB.getP1(name);
	}
}
