package lkc_parser.tristate;

import java.util.ArrayList;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PFormulasList;
import lkc_parser.propositional.PNot;
import lkc_parser.propositional.PVariableDatabase;

public class TriOr extends TriBinaryExpression implements TristateOperation {
	public TriOr(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String operatorString() {
		return "||";
	}

	private static ArrayList<Expression> collectChain(Expression expression) {
		ArrayList<Expression> res;
		if (expression instanceof TriOr) {
			TriOr TriOr = (TriOr) expression;
			res = collectChain(TriOr.left);
			res.addAll(collectChain(TriOr.right));
		} else {
			res = new ArrayList<Expression>();
			res.add(expression);
		}
		return res;
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		ArrayList<Expression> chain = collectChain(this);
		PFormulasList resList = new PFormulasList();

		for (Expression expression : chain) {
			resList.add(expression.calcPi0(typesMap, vDB));
		}

		return resList.orConcatenation();
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		ArrayList<Expression> chain = collectChain(this);
		PFormulasList resList = new PFormulasList();
		PFormulasList orList = new PFormulasList();

		for (Expression expression : chain) {
			resList.add(PNot.produce(expression.calcPi0(typesMap, vDB)));
			orList.add(expression.calcPi1(typesMap, vDB));
		}

		resList.add(orList.orConcatenation());

		return resList.andConcatenation();
	}
}
