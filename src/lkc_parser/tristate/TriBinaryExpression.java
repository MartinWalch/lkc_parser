package lkc_parser.tristate;

public abstract class TriBinaryExpression extends Expression {
	public final Expression left;
	public final Expression right;

	public TriBinaryExpression(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public boolean contains(ExpressionSymbol expressionSymbol) {
		return left.contains(expressionSymbol) || right.contains(expressionSymbol);
	}

	public abstract String operatorString();

	@Override
	public String toString() {
		return '(' + left.toString() + ' ' + operatorString() + ' ' + right.toString() + ')';
	}
}
