package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PConstant;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;

public class TristateConstant extends TriConstantSymbol {
	public static final TristateConstant NO = new TristateConstant("n");
	public static final TristateConstant MOD = new TristateConstant("m");
	public static final TristateConstant YES = new TristateConstant("y");

	private TristateConstant(String value) {
		super(value);
	}

	public TriStringConstant toStringConstant() {
		return new TriStringConstant(name);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		return (this == YES) ? PConstant.TOP : PConstant.BOTTOM;
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return (this == MOD) ? PConstant.TOP : PConstant.BOTTOM;
	}
}
