package lkc_parser.tristate;

import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Expr_data;
import lkc_parser.parser.data.Symbol;

public class ExprConverter {
	public static Expression exprToExpression(Expr_data expr_data) {
		Expression res = null;
		if (expr_data instanceof Expr) {
			final Expr expr = (Expr) expr_data;
			switch (expr.type) {
			case E_SYMBOL:
				res = exprToExpression(expr.left);
				break;
			case E_NOT:
				res = new TriNot(exprToExpression(expr.left));
				break;
			case E_EQUAL:
				res = new TriEqual(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_UNEQUAL:
				res = new TriUnequal(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_LEQ:
				res = new TriLessOrEqual(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_LTH:
				res = new TriLessThan(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_GEQ:
				res = new TriGreaterOrEqual(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_GTH:
				res = new TriGreaterThan(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_OR:
				res = new TriOr(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_AND:
				res = new TriAnd(exprToExpression(expr.left), exprToExpression(expr.right));
				break;
			case E_RANGE:
				throw new UnsupportedOperationException(
						"E_RANGE encountered. As Ranges are implemented differently, this must"
								+ "not occur.");
			case E_LIST:
				throw new UnsupportedOperationException(
						"E_LIST encountered. As this is part of LKC's choice magic, and we"
								+ "handle choices differently, this must not occur");
			case E_NONE:
				throw new UnsupportedOperationException(
						"E_NONE encountered. Alright, no type. Can not occur. Even in LKC"
								+ "it is only a dummy value.");
			default:
				throw new UnsupportedOperationException(
						"Expression with unknown type? This must be a serious programming error.");
			}
		} else if (expr_data instanceof Symbol) {
			final Symbol symbol = (Symbol) expr_data;
			final String name = symbol.getName();

			if ((symbol.getFlags() & Symbol.SYMBOL.CONST) == 0) {
				assert (!(name.equals("y") || name.equals("m") || name.equals("n")));
				res = new TriSymbol(name);
			} else {
				if (symbol == Symbol.symbol_yes) {
					res = TristateConstant.YES;
				} else if (symbol == Symbol.symbol_mod) {
					res = TristateConstant.MOD;
				} else if (symbol == Symbol.symbol_no) {
					res = TristateConstant.NO;
				} else {
					res = new TriStringConstant(name);
				}
			}
		}
		return res;
	}
}
