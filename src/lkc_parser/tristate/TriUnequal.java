package lkc_parser.tristate;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PConstant;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PNot;
import lkc_parser.propositional.PVariableDatabase;

public class TriUnequal extends TriBinaryExpression implements TriStringOperation {
	public TriUnequal(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String operatorString() {
		return "!=";
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		return PNot.produce(new TriEqual(left, right).calcPi0(typesMap, vDB));
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return PConstant.BOTTOM;
	}
}
