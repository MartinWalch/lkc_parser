package lkc_parser;

import java.util.ArrayList;
import java.util.logging.Level;

public class Settings {
	private static final Level[] LOG_LEVELS = new Level[] { Level.OFF, Level.SEVERE, Level.WARNING,
			Level.INFO, Level.CONFIG, Level.FINE, Level.FINER, Level.FINEST, Level.ALL };

	public static final Level MAX_LOG_LEVEL = LOG_LEVELS[LOG_LEVELS.length - 1];

	private abstract static class Argument<T> {
		private T value;
		private final char shortSwitch;
		private final String longSwitch;
		private final T defaultValue;
		private final String description;

		Argument(char shortSwitch, String longSwitch, T defaultValue, String description) {
			value = defaultValue;
			this.shortSwitch = shortSwitch;
			this.longSwitch = longSwitch;
			this.defaultValue = defaultValue;
			this.description = description;
		}

		abstract void parseArg(String input);

		T value() {
			return value;
		}

		void set(T value) {
			this.value = value;
		}

		char getShortSwitch() {
			return shortSwitch;
		}

		public Object getLongSwitch() {
			return longSwitch;
		}

		T getDefaultValue() {
			return defaultValue;
		}

		public String getDescription() {
			return description;
		}
	}

	private static class BoolArg extends Argument<Boolean> {
		BoolArg(Boolean defaultValue, char shortSwitch, String longSwitch, String description) {
			super(shortSwitch, longSwitch, defaultValue, description);
		}

		@Override
		public void parseArg(String input) {
			set(Boolean.valueOf(input));
		}
	}

	private static class IntArg extends Argument<Integer> {
		IntArg(Integer defaultValue, char shortSwitch, String longSwitch, String description) {
			super(shortSwitch, longSwitch, defaultValue, description);
		}

		@Override
		public void parseArg(String input) {
			set(Integer.valueOf(input));
		}
	}

	private static class StringArg extends Argument<String> {
		StringArg(String defaultValue, char shortSwitch, String longSwitch, String description) {
			super(shortSwitch, longSwitch, defaultValue, description);
		}

		@Override
		public void parseArg(String input) {
			set(input);
		}
	}

	private static ArrayList<Argument<?>> possibleArguments = new ArrayList<Argument<?>>();

	private static Argument<Boolean> forceEntryDetection = new BoolArg(
			Boolean.FALSE,
			'a',
			"automatic",
			"force automatic detection of root configuration files, even with make version >=3.82 and old kernels");
	private static Argument<Boolean> refreshCache = new BoolArg(Boolean.FALSE, 'c', "clearcache",
			"do not use existing architecture cache, but create a new one");
	private static Argument<String> rootFile = new StringArg(null, 'f', "file",
			"explicit single root file, no automatic detections (mandatory when not in Linux mode)");
	private static Argument<Boolean> help = new BoolArg(Boolean.FALSE, 'h', "help",
			"show short help on usage");
	private static Argument<String> kernelVersion = new StringArg(null, 'k', "kernel",
			"set kernel version, skip detection (only in Linux mode, mandatory for Linux mode with -f)");
	private static Argument<Boolean> linuxMode = new BoolArg(Boolean.TRUE, 'l', "linux",
			"processing Linux kernel");
	private static Argument<Integer> verbosity = new IntArg(Integer.valueOf(5), 'v', "verbose",
			"verbosity (0-" + (LOG_LEVELS.length - 1) + ")");
	private static String basePath;

	static {
		possibleArguments.add(forceEntryDetection);
		possibleArguments.add(refreshCache);
		possibleArguments.add(rootFile);
		possibleArguments.add(help);
		possibleArguments.add(kernelVersion);
		possibleArguments.add(linuxMode);
		possibleArguments.add(verbosity);
	}

	public static boolean isForceEntryDetection() {
		return forceEntryDetection.value().booleanValue();
	}

	public static boolean isRefreshCache() {
		return refreshCache.value().booleanValue();
	}

	public static boolean isFileMode() {
		return rootFile.value() != null;
	}

	public static String getRootFileName() {
		return rootFile.value();
	}

	public static boolean isHelp() {
		return help.value().booleanValue();
	}

	public static String getKernelVersion() {
		return kernelVersion.value();
	}

	public static boolean isLinuxMode() {
		return linuxMode.value().booleanValue();
	}

	public static int getVerbosity() {
		return verbosity.value().intValue();
	}

	public static void setBasePath(String newBasePath) {
		basePath = newBasePath;
	}

	public static String getBasePath() {
		return basePath;
	}

	public static void parse(String[] args) {
		int pos = 0;
		if (args.length % 2 == 1) {
			usage();
		}
		nextArg: while (pos < args.length) {
			String next = args[pos++];
			if (next.matches("-[^-]")) {
				char shortSwitch = next.charAt(1);
				for (Argument<?> argument : possibleArguments) {
					if (argument.getShortSwitch() == shortSwitch) {
						argument.parseArg(args[pos++]);
						continue nextArg;
					}
				}
				usage();
			} else if (next.matches("--.*")) {
				String longSwitch = next.substring(2);
				for (Argument<?> argument : possibleArguments) {
					if (longSwitch.equals(argument.getLongSwitch())) {
						argument.parseArg(args[pos++]);
					}
					continue nextArg;
				}
				usage();
			} else {
				usage();
			}
		}
	}

	public static void usage() {
		for (Argument<?> argument : possibleArguments) {
			String defaultValue = (argument.getDefaultValue() != null)? "[" + argument.getDefaultValue() + "]" : "<not set>";
			System.out.println("-" + argument.getShortSwitch() + ", --" + argument.getLongSwitch()
					+ ", " + defaultValue);
			System.out.println("  " + argument.getDescription());
		}
		System.exit(-1);
	}

	public static Level getLogLevel() {
		int index = verbosity.value().intValue();

		index = Math.max(index, 0);
		index = Math.min(index, LOG_LEVELS.length);

		return LOG_LEVELS[index];
	}
}
