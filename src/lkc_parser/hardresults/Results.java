package lkc_parser.hardresults;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import lkc_parser.Settings;
import lkc_parser.attributesmodel.AttributesModel;
import lkc_parser.attributesmodel.AttributesSymbol;
import lkc_parser.attributesmodel.ChoiceGroup;
import lkc_parser.parser.Arch;
import lkc_parser.parser.data.Symbol;
import lkc_parser.parser.data.Symbol_type;
import lkc_parser.propositional.PPOF;
import lkc_parser.tokentree.ChoiceBlock;
import lkc_parser.tokentree.ConfigBlock;
import lkc_parser.tokentree.DescribingBlock;
import lkc_parser.tokentree.Location;
import lkc_parser.tokentree.SourceDirective;
import lkc_parser.tokentree.SourceFile;
import lkc_parser.tristate.anomalies.AnomaliesCollection;
import lkc_parser.tristate.anomalies.Anomaly;
import lkc_parser.tristatestar.TriSPOF;
import lkc_parser.zenglermodel2010.ChoiceDescriptor;
import lkc_parser.zenglermodel2010.PromptProperty;
import lkc_parser.zenglermodel2010.SymbolDescriptor;
import lkc_parser.zenglermodel2010.Type;
import lkc_parser.zenglermodel2010.ZenglerModel;
import lkc_parser.zenglermodel2010.ZenglerSymbol;

public class Results {
	private static final String RESULTS = "results";
	public static final Logger resultsLogger = Logger.getLogger(RESULTS);
	private static final ResultsFormatter formatter = new ResultsFormatter();
	private static final StdOutHandler stdOutHandler = new StdOutHandler(formatter);

	static {
		resultsLogger.setUseParentHandlers(false);
		stdOutHandler.setFormatter(formatter);
		for (Handler handler : resultsLogger.getHandlers()) {
			resultsLogger.removeHandler(handler);
		}
		stdOutHandler.setLevel(Settings.getLogLevel());
		resultsLogger.addHandler(stdOutHandler);
		resultsLogger.setLevel(Settings.getLogLevel());
	}

	private final Logger logger;

	private static final TreeMap<String, ArrayList<Location>> usedButUndeclaredSymbols = new TreeMap<String, ArrayList<Location>>();
	private static final TreeMap<String, ArrayList<Location>> probablyUnquotedConstants = new TreeMap<String, ArrayList<Location>>();
	private static final TreeMap<String, ArrayList<Location>> symbolsWithConstantsNames = new TreeMap<String, ArrayList<Location>>();
	private static final TreeMap<String, ArrayList<Location>> symbolsWithCONFIG_Pattern = new TreeMap<String, ArrayList<Location>>();
	private static final ArrayList<MultipleTypeInformation> severalTypesInBlocks = new ArrayList<MultipleTypeInformation>();
	private static final HashMap<Location, String> legacyMainmenuStatements = new HashMap<Location, String>();
	private static final ArrayList<DescribingBlock> multipleHelpTexts = new ArrayList<DescribingBlock>();
	private static final ArrayList<SourceDirective> deadSourceDirectives = new ArrayList<SourceDirective>();
	private static final ArrayList<ChoiceBlock> emptyChoiceBlocks = new ArrayList<ChoiceBlock>();
	private static final HashMap<ChoiceBlock, ArrayList<Location>> superOptionalChoiceBlocks = new HashMap<ChoiceBlock, ArrayList<Location>>();
	private static final TreeMap<Arch, Results> resultsMap = new TreeMap<Arch, Results>();
	private static final ArrayList<SourceFile> filesWithoutNewlineAtEOF = new ArrayList<SourceFile>();
	private static final ArrayList<Location> emptyStringsAsDefaults = new ArrayList<Location>();
	private static final TreeMap<String, ArrayList<Location>> symbolNamesWithHyphen = new TreeMap<String, ArrayList<Location>>();
	private static final HashMap<String, HashMap<Type, TreeSet<Arch>>> globalTypesMap = new HashMap<String, HashMap<Type, TreeSet<Arch>>>();
	private static final HashMap<Location, String> configBlocksWithMoreThanOnePrompt = new HashMap<Location, String>();
	private static final HashSet<String> symbolsWithPrompts = new HashSet<String>(16383);

	public static TreeSet<Arch> detectedArchitectures;
	public static int dependsOn = 0;
	public static int ifBlock = 0;
	public static int ifGuard = 0;
	public static int select = 0;
	public static int imply = 0;
	public static int def = 0;
	public static int range = 0;
	public static int prompt = 0;
	public static int optional = 0;
	public static int visible = 0;
	public static int bool = 0;
	public static int tristate = 0;
	public static int string = 0;
	public static int intType = 0;
	public static int hex = 0;

	private final Arch arch;
	private final HashMap<String, ArrayList<SymbolDescriptor>> symbolsWithMoreThanOnePrompt = new HashMap<String, ArrayList<SymbolDescriptor>>();
	private final HashMap<ChoiceDescriptor, SymbolDescriptor> externalChoiceType = new HashMap<ChoiceDescriptor, SymbolDescriptor>();
	private final HashMap<ChoiceDescriptor, Type> badChoiceType = new HashMap<ChoiceDescriptor, Type>();
	private final ArrayList<ChoiceGroup> noRealChoice = new ArrayList<ChoiceGroup>();
	private final HashMap<String, ArrayList<SymbolDescriptor>> symbolsWithoutType = new HashMap<String, ArrayList<SymbolDescriptor>>();
	private final HashMap<String, ArrayList<SymbolDescriptor>> symbolsWithRedundantType = new HashMap<String, ArrayList<SymbolDescriptor>>();
	private final HashMap<String, ArrayList<SymbolDescriptor>> symbolsWithContradictoryType = new HashMap<String, ArrayList<SymbolDescriptor>>();
	private final HashMap<Type, Integer> typeCounter = new HashMap<Type, Integer>();
	private final HashMap<SourceFile, ArrayList<SourceDirective>> repeatedFileIncludes = new HashMap<SourceFile, ArrayList<SourceDirective>>();
	private final HashMap<ZenglerSymbol, ArrayList<SymbolDescriptor>> notNumberTypeSymbolsWithRanges = new HashMap<ZenglerSymbol, ArrayList<SymbolDescriptor>>();
	private final HashMap<String, ArrayList<Location>> undeclaredBoundariesSymbols = new HashMap<String, ArrayList<Location>>();
	private final HashMap<AttributesSymbol, ArrayList<Location>> badBoundariesSymbols = new HashMap<AttributesSymbol, ArrayList<Location>>();
	private final HashMap<AttributesSymbol, ArrayList<Location>> conversionOnBoundariesSymbols = new HashMap<AttributesSymbol, ArrayList<Location>>();
	private final HashMap<ZenglerSymbol, ArrayList<Location>> selectedSymbolsWithWrongType = new HashMap<ZenglerSymbol, ArrayList<Location>>();
	private final HashMap<String, ArrayList<Location>> selectedUndeclaredSymbols = new HashMap<String, ArrayList<Location>>();
	private final TreeMap<Location, AnomaliesCollection> anomaliesTable = new TreeMap<Location, AnomaliesCollection>();

	private ArrayList<SourceFile> includedFiles;
	private int numberOfDeclaredSymbols;
	private int numberOfConfigBlocks;
	private int numberOfChoiceBlocks;
	private long amountOfVarsInTPOF;
	private long amountOfAuxVarsInTPOF;
	private long amountOfVarsInPPOF;
	private long totalAmountOfVarsInTPOF;

	private Results(Arch arch) {
		logger = Logger.getLogger(RESULTS + "." + arch.getName());
		this.arch = arch;

		for (Type t : Type.values()) {
			typeCounter.put(t, Integer.valueOf(0));
		}
	}

	public static Results registerArch(Arch arch) {
		Results res;
		if (resultsMap.containsKey(arch)) {
			throw new RuntimeException("arch already in list");
		}
		res = new Results(arch);
		resultsMap.put(arch, res);
		return res;
	}

	public static Results getResults(Arch arch) {
		return resultsMap.get(arch);
	}

	public static void setDetectedArchitectures(TreeSet<Arch> architectures) {
		detectedArchitectures = architectures;
	}

	public static void severalTypesInBlock(DescribingBlock describingBlock, Symbol_type oldType,
			Symbol_type newType) {
		severalTypesInBlocks.add(new MultipleTypeInformation(describingBlock, oldType, newType));
	}

	public static void legacyMainmenuStatement(Location location, String prompt) {
		legacyMainmenuStatements.put(location, prompt);
	}

	public static void multipleHelpTexts(DescribingBlock declarationBlock) {
		multipleHelpTexts.add(declarationBlock);
	}

	public static void deadSourceFile(SourceDirective sourceDirective) {
		deadSourceDirectives.add(sourceDirective);
	}

	public static void emptyChoice(ChoiceBlock choice) {
		emptyChoiceBlocks.add(choice);
	}

	public static void repeatedOptionalChoice(ChoiceBlock choiceBlock) {
		if (!superOptionalChoiceBlocks.containsKey(choiceBlock)) {
			superOptionalChoiceBlocks.put(choiceBlock, new ArrayList<Location>());
		}
		superOptionalChoiceBlocks.get(choiceBlock).add(choiceBlock.getLocation());
	}

	public static void noNewlineAtEOF(SourceFile sourceFile) {
		filesWithoutNewlineAtEOF.add(sourceFile);
	}

	public static void emtpyStringAsDefault(Location currentLocation) {
		emptyStringsAsDefaults.add(currentLocation);
	}

	private static void globalSyntacticChecks() {
		final HashMap<String, ArrayList<ConfigBlock>> globalConfigBlockList = ConfigBlock
				.getGlobalList();
		final HashMap<String, ArrayList<Location>> variableSymbolsOccurrences = Symbol
				.getVariableSymbolsOccurrences();

		for (Entry<String, ArrayList<ConfigBlock>> symbolEntry : globalConfigBlockList.entrySet()) {
			final String symbolName = symbolEntry.getKey();
			final ArrayList<ConfigBlock> configBlocks = symbolEntry.getValue();

			if (Symbol.looksMuchLikeNumber(symbolName)) {
				ArrayList<Location> locations = new ArrayList<Location>();
				for (ConfigBlock configBlock : configBlocks) {
					locations.add(configBlock.getLocation());
				}
				symbolsWithConstantsNames.put(symbolName, locations);
			}
			if (Pattern.matches(".*CONFIG_.*", symbolName)) {
				ArrayList<Location> locations = new ArrayList<Location>();
				for (ConfigBlock configBlock : configBlocks) {
					locations.add(configBlock.getLocation());
				}
				symbolsWithCONFIG_Pattern.put(symbolName, locations);
			}
			if (symbolName.contains("-")) {
				ArrayList<Location> locations = new ArrayList<Location>();
				for (ConfigBlock configBlock : configBlocks) {
					locations.add(configBlock.getLocation());
				}
				symbolNamesWithHyphen.put(symbolName, locations);
			}
		}

		for (Entry<String, ArrayList<Location>> entry : variableSymbolsOccurrences.entrySet()) {
			final String name = entry.getKey();
			final ArrayList<Location> locations = entry.getValue();

			if (!globalConfigBlockList.containsKey(name)) {
				if (Symbol.looksLikeConstant(name)) {
					probablyUnquotedConstants.put(name, locations);
				} else {
					usedButUndeclaredSymbols.put(name, locations);
				}
			}
		}
	}

	private static void printBasicStatistics() {
		Collection<SourceFile> includedFiles = SourceFile.getAllSourceFiles();
		int numberOfFiles = includedFiles.size();

		resultsLogger.log(Level.INFO, "_Basic Numbers_");
		if (Settings.isLinuxMode()) {
			resultsLogger.log(Level.INFO,
					"Detected kernel version: " + Arch.getKernelVersionString());
		}
		if (!Settings.isFileMode()) {
			final HashMap<String, ArrayList<ConfigBlock>> globalConfigBlockList = ConfigBlock
					.getGlobalList();
			final ArrayList<ChoiceBlock> globalChoiceList = ChoiceBlock.getGloballist();

			int numberOfConfigBlocks = 0;
			ChoiceBlock biggestChoiceBlock = globalChoiceList.get(0);
			for (ArrayList<ConfigBlock> blockList : globalConfigBlockList
					.values()) {
				numberOfConfigBlocks += blockList.size();
			}

			for (ChoiceBlock choiceBlock : globalChoiceList) {
				int size = choiceBlock.getChildren().size();

				if (size > biggestChoiceBlock.getChildren().size()) {
					biggestChoiceBlock = choiceBlock;
				}
			}

			resultsLogger
					.log(Level.INFO, "Detected architectures: " + detectedArchitectures.size());
			resultsLogger.log(Level.INFO, "Analyzed architectures: " + resultsMap.size());
			resultsLogger.log(Level.INFO, "All declared symbols: " + globalConfigBlockList.size());
			resultsLogger.log(Level.INFO, "All symbol declaring configuration blocks: "
					+ numberOfConfigBlocks);
			resultsLogger.log(Level.INFO, "All symbols with at least one prompt: " + symbolsWithPrompts.size());
			resultsLogger.log(Level.INFO, "All declared choices: " + globalChoiceList.size());
			if (globalChoiceList.size() > 0) {
				resultsLogger.log(Level.INFO, "biggest choice block: "
						+ biggestChoiceBlock.getLocation() + " ("
						+ biggestChoiceBlock.getChildren().size() + " options)");
			}
		}
		resultsLogger.log(Level.INFO, "Unique files: " + numberOfFiles);
		resultsLogger.log(Level.INFO, "");
		resultsLogger.log(Level.INFO, "occurrencens of depends on: " + dependsOn);
		resultsLogger.log(Level.INFO, "ifBlock: " + ifBlock);
		resultsLogger.log(Level.INFO, "ifGuard: " + ifGuard);
		resultsLogger.log(Level.INFO, "select: " + select);
		resultsLogger.log(Level.INFO, "def: " + def);
		resultsLogger.log(Level.INFO, "range: " + range);
		resultsLogger.log(Level.INFO, "prompt: " + prompt);
		resultsLogger.log(Level.INFO, "optional: " + optional);
		resultsLogger.log(Level.INFO, "visible: " + visible);
		resultsLogger.log(Level.INFO, "bool: " + bool);
		resultsLogger.log(Level.INFO, "tristate: " + tristate);
		resultsLogger.log(Level.INFO, "string: " + string);
		resultsLogger.log(Level.INFO, "intType: " + intType);
		resultsLogger.log(Level.INFO, "hex: " + hex);
		resultsLogger.log(Level.INFO, "");
	}

	private static void printInterfaceProblems() {
		final int patternCONFIG_ = symbolsWithCONFIG_Pattern.size();

		resultsLogger.log(Level.INFO, "_Problems Concerning the User Interfaces_");
		// if there is more than one help text, one is masked, so this
		// should be cleaned up
		resultsLogger.log(Level.INFO,
				"More than one help text in one block: " + multipleHelpTexts.size());
		for (DescribingBlock describingBlock : multipleHelpTexts) {
			resultsLogger.log(Level.FINE, "  at " + describingBlock.getLocation().toString()
					+ " in declaration of " + describingBlock.getName());
		}
		// mconf strips any leading CONFIG_ substring when searching for
		// a symbol
		resultsLogger.log(Level.INFO,
				"Actually declared symbols containing the string \"CONFIG_\": " + patternCONFIG_);
		for (Entry<String, ArrayList<Location>> entry : symbolsWithCONFIG_Pattern.entrySet()) {
			resultsLogger.log(Level.FINE, "  " + entry.getKey() + " at");
			for (Location occurrence : entry.getValue()) {
				resultsLogger.log(Level.FINE, "    " + occurrence);
			}
		}
		resultsLogger.log(Level.INFO, "");
	}

	private static void printLegacyWarnings() {
		final int namesWithHyphen = symbolNamesWithHyphen.size();
		final int numberOfConfigBlocksWithMoreThanOnePrompt = configBlocksWithMoreThanOnePrompt.size();

		resultsLogger.log(Level.INFO, "_Old or Deprecated Syntax_");
		// there SHOULD be only one mainmenu statement and it SHOULD be the very
		// first statement
		resultsLogger.log(Level.INFO,
				"Old style mainmenu entries: " + legacyMainmenuStatements.size());
		for (Entry<Location, String> legacyEntry : legacyMainmenuStatements.entrySet()) {
			resultsLogger.log(Level.FINE,
					"  \"" + legacyEntry.getValue() + "\" at " + legacyEntry.getKey());
		}

		// bad for parsers, bloats grammars, makes people begin files with a
		// newline
		resultsLogger.log(Level.INFO,
				"Files without newline at EOF: " + filesWithoutNewlineAtEOF.size());
		for (SourceFile sourceFile : filesWithoutNewlineAtEOF) {
			resultsLogger.log(Level.FINE, "  " + sourceFile);
		}

		// empty strings violate assertion of print function in LKC
		resultsLogger.log(Level.INFO, "\"default\" properties with empty string: "
				+ emptyStringsAsDefaults.size());
		for (Location location : emptyStringsAsDefaults) {
			resultsLogger.log(Level.FINE, "  " + location);
		}

		// hyphens in symbol names may cause problems when sourcing individual
		// configurations in a bash (and probably many other shells)
		resultsLogger.log(Level.INFO, "Actually declared symbols containing a hyphen: "
				+ namesWithHyphen);
		for (Entry<String, ArrayList<Location>> entry : symbolNamesWithHyphen.entrySet()) {
			resultsLogger.log(Level.FINE, "  " + entry.getKey() + " at");
			for (Location occurrence : entry.getValue()) {
				resultsLogger.log(Level.FINE, "    " + occurrence);
			}
		}

		// all prompts in a config block except the last one are ignored
		resultsLogger.log(Level.INFO, "Config blocks with more than one prompt: "
				+ numberOfConfigBlocksWithMoreThanOnePrompt);
		for (Entry<Location, String> entry : configBlocksWithMoreThanOnePrompt.entrySet()) {
			resultsLogger.log(Level.FINE, "  For symbol " + entry.getValue() + " at");
			resultsLogger.log(Level.FINE, "    " + entry.getKey());
		}
		resultsLogger.log(Level.INFO, "");
	}

	private static void printCosmeticProblems() {
		final int amountOfRepeatedOptional = superOptionalChoiceBlocks.size();

		resultsLogger.log(Level.INFO, "_Purely Cosmetic Flaws_");

		// Does not hurt, but is redundant. It is probably a good idea to drop
		// repetitions.
		resultsLogger.log(Level.INFO, "Repeated optional attributes in choice blocks: "
				+ amountOfRepeatedOptional);
		for (Entry<ChoiceBlock, ArrayList<Location>> entry : superOptionalChoiceBlocks.entrySet()) {
			resultsLogger.log(Level.FINE, "  at");
			for (Location occurrence : entry.getValue()) {
				resultsLogger.log(Level.FINE, "    " + occurrence);
			}
		}
		resultsLogger.log(Level.INFO, "");
	}

	private static void printLogicalProblems() {
		final int amountOfUndeclared = usedButUndeclaredSymbols.size();
		final int constNames = symbolsWithConstantsNames.size();
		final int amountOfConstantMade = probablyUnquotedConstants.size();
		final TreeMap<String, HashMap<Type, TreeSet<Arch>>> differentTypesOnDifferentArches = new TreeMap<String, HashMap<Type, TreeSet<Arch>>>();

		for (Entry<String, HashMap<Type, TreeSet<Arch>>> entry : globalTypesMap.entrySet()) {
			assert (entry.getValue().size() >= 1);
			if (entry.getValue().size() > 1) {
				differentTypesOnDifferentArches.put(entry.getKey(), entry.getValue());
			}
		}

		resultsLogger.log(Level.INFO, "_Problems Concerning the Configuration Logic_");

		// dead source directives are bad
		resultsLogger.log(Level.INFO, "Dead source commands: " + deadSourceDirectives.size());
		for (SourceDirective sourceDirective : deadSourceDirectives) {
			resultsLogger.log(Level.FINE, "  " + sourceDirective.toString() + " at ");
			resultsLogger.log(Level.FINE, "    " + sourceDirective.getLocation().toString());
		}

		// empty choice block: did someone forget to add content?
		resultsLogger.log(Level.INFO, "Empty choice blocks: " + emptyChoiceBlocks.size());
		for (ChoiceBlock choiceBlock : emptyChoiceBlocks) {
			resultsLogger.log(Level.FINE, "  " + choiceBlock.toString() + " at ");
			resultsLogger.log(Level.FINE, "    " + choiceBlock.getLocation().toString());
		}

		// documentation wants constant symbols to be quoted, which is a good
		// idea otherwise it is only possible at runtime to safely decide
		// whether a symbol is constant
		resultsLogger.log(Level.INFO,
				"Symbols that are probably meant to be constant due to their names: "
						+ amountOfConstantMade);
		for (Entry<String, ArrayList<Location>> entry : probablyUnquotedConstants.entrySet()) {
			resultsLogger.log(Level.FINE, "  " + entry.getKey() + " at");
			for (Location occurrence : entry.getValue()) {
				resultsLogger.log(Level.FINE, "    " + occurrence.toString());
			}
		}

		// dangerous, especially together with unquoted constants even worse if
		// referenced on arches where it is not declared
		resultsLogger.log(Level.INFO, "Actually declared symbols with names of constants: "
				+ constNames);
		for (Entry<String, ArrayList<Location>> entry : symbolsWithConstantsNames.entrySet()) {
			resultsLogger.log(Level.FINE, "  " + entry.getKey() + " at");
			for (Location occurrence : entry.getValue()) {
				resultsLogger.log(Level.FINE, "    " + occurrence.toString());
			}
		}

		// redundancy is bad. leads to overhead and may be the source of future
		// inconsistencies
		resultsLogger.log(Level.INFO, "Multiple type declarations in symbol declaring sections: "
				+ severalTypesInBlocks.size());
		for (MultipleTypeInformation multipleTypeInformation : severalTypesInBlocks) {
			resultsLogger.log(Level.FINE, "  " + multipleTypeInformation + " at");
			resultsLogger
					.log(Level.FINE, "    " + multipleTypeInformation.getLocation().toString());
		}

		// Symbols that are not declared on any arch. Either they are used
		// externally (e. g. in other trees) or they are dead and can be
		// removed.
		resultsLogger.log(Level.INFO, "Symbols used, but not declared: " + amountOfUndeclared);
		for (Entry<String, ArrayList<Location>> entry : usedButUndeclaredSymbols.entrySet()) {
			resultsLogger.log(Level.FINE, "  Symbol: " + entry.getKey() + " at");
			for (Location occurrence : entry.getValue()) {
				resultsLogger.log(Level.FINE, "    " + occurrence.toString());
			}
		}

		// Symbols with different types on different architectures, e. g.
		// symbol A is declared on arm as a tristate symbol and on sparc as a
		// bool symbol.
		// This is not necessarily broken, but it might be a hint for a problem
		// like:
		// - accidental naming conflict
		// - change of redundant type declarations has missed an architecture
		// It might also be intended (It's a feature!). However, this massively
		// increases complexity and should be reconsidered.
		resultsLogger.log(Level.INFO, "Symbols with different types on different architectures: "
				+ differentTypesOnDifferentArches.size());
		for (Entry<String, HashMap<Type, TreeSet<Arch>>> entry : differentTypesOnDifferentArches
				.entrySet()) {
			resultsLogger.log(Level.FINE, "  Symbol " + entry.getKey());
			for (Entry<Type, TreeSet<Arch>> symbolEntry : entry.getValue().entrySet()) {
				resultsLogger.log(Level.FINE, "    " + symbolEntry.getKey() + " on");
				for (Arch arch : symbolEntry.getValue()) {
					resultsLogger.log(Level.FINE, "      " + arch);
				}
			}
		}

		resultsLogger.log(Level.INFO, "");
	}

	public static void printResults() {
		globalSyntacticChecks();
		printBasicStatistics();
		printInterfaceProblems();
		printLegacyWarnings();
		printCosmeticProblems();
		printLogicalProblems();

		for (Results results : resultsMap.values()) {
			results.print();
		}
		//latexTabularPrint() ;
	}

//	private static void latexTabularPrint() {
//		int counter = 0;
//		for (Entry<Arch, Results> entry : resultsMap.entrySet()) {
//			Results results = entry.getValue();
//			System.out.println("\\arch{" + entry.getKey() + "}" + " & " + results.typeCounter.get(Type.TRISTATE)
//					+ " & " + results.typeCounter.get(Type.BOOL) + " & "
//					+ results.typeCounter.get(Type.STRING) + " & "
//					+ results.typeCounter.get(Type.INT) + " & " + results.typeCounter.get(Type.HEX)
//					+ " & " + results.numberOfDeclaredSymbols + "\\\\");
//
//			counter++;
//
//			if (counter == 4) {
//				System.out.println("\\midrule");
//				counter=0;
//			}
//			if (results.numberOfDeclaredSymbols != (results.typeCounter.get(Type.TRISTATE)
//					+ results.typeCounter.get(Type.BOOL) + results.typeCounter.get(Type.STRING)
//					+ results.typeCounter.get(Type.INT) + results.typeCounter.get(Type.HEX))) {
//				System.out.println("BAD");
//			} else {
//				System.out.println("OK");
//			}
//		}
//	}

	public void print() {
//		int numberOfAnomalies = 0;

		int filesWithoutSourceDirectives = 0;

		logger.log(Level.INFO, "_Results for Architecture " + arch + "_");
		//logger.log(Level.SEVERE, arch.toString());

		for (SourceFile sourceFile : includedFiles) {
			ArrayList<SourceDirective> sourceDirectives = sourceFile.getSourceDirectives(arch);
			if (sourceDirectives != null) {
				if (sourceDirectives.size() > 1) {
					repeatedFileIncludes.put(sourceFile, sourceDirectives);
				}
			} else {
				filesWithoutSourceDirectives++;
			}
		}

		if (filesWithoutSourceDirectives != 1) {
			throw new IllegalStateException("Not exactly one file without source directive.");
		}

		logger.log(Level.INFO, "  Files: " + includedFiles.size());

		logger.log(Level.INFO,
				"  Repeated inclusions of same files: " + repeatedFileIncludes.size());
		for (Entry<SourceFile, ArrayList<SourceDirective>> entry : repeatedFileIncludes.entrySet()) {
			logger.log(Level.FINE, "    " + entry.getKey() + " at");
			for (SourceDirective desc : entry.getValue()) {
				logger.log(Level.FINE, "      " + desc.getLocation());
			}
		}

		logger.log(Level.INFO, "  Config blocks: " + numberOfConfigBlocks);

		logger.log(Level.INFO, "  Declared symbols: " + numberOfDeclaredSymbols);
		for (Entry<Type, Integer> entry : typeCounter.entrySet()) {
			Type type = entry.getKey();
			if (type != Type.UNKNOWN && type != Type.INVALID) {
				logger.log(Level.INFO, "    " + type + ": " + entry.getValue());
			}
		}

		logger.log(Level.INFO, "  Missing type for declared symbol: " + symbolsWithoutType.size());
		for (Entry<String, ArrayList<SymbolDescriptor>> entry : symbolsWithoutType.entrySet()) {
			logger.log(Level.FINE, "  " + entry.getKey() + " at");
			for (SymbolDescriptor desc : entry.getValue()) {
				logger.log(Level.FINE, "    " + desc.getLocation());
			}
		}

		logger.log(Level.INFO, "  More than one prompt for declared symbol: "
				+ symbolsWithMoreThanOnePrompt.size());
		for (Entry<String, ArrayList<SymbolDescriptor>> entry : symbolsWithMoreThanOnePrompt
				.entrySet()) {
			logger.log(Level.FINE, "    " + entry.getKey() + " at");
			for (SymbolDescriptor desc : entry.getValue()) {
				logger.log(Level.FINE, "      " + desc.getLocation());
			}
		}

		logger.log(Level.INFO,
				"  Redundant type for declared symbol: " + symbolsWithRedundantType.size());
		for (Entry<String, ArrayList<SymbolDescriptor>> entry : symbolsWithRedundantType.entrySet()) {
			logger.log(Level.FINE, "    " + entry.getKey() + " at");
			for (SymbolDescriptor desc : entry.getValue()) {
				logger.log(Level.FINE, "      " + desc.getLocation());
			}
		}

		logger.log(Level.INFO, "  Contradictory type for declared symbol: "
				+ symbolsWithContradictoryType.size());
		for (Entry<String, ArrayList<SymbolDescriptor>> entry : symbolsWithContradictoryType
				.entrySet()) {
			logger.log(Level.FINE, "    " + entry.getKey() + " at");
			for (SymbolDescriptor desc : entry.getValue()) {
				logger.log(Level.FINE, "      " + desc.getLocation());
			}
		}

		logger.log(Level.INFO, "  Symbols that are not bool or tristate but are select targets: "
				+ this.selectedSymbolsWithWrongType.size());
		for (Entry<ZenglerSymbol, ArrayList<Location>> entry : selectedSymbolsWithWrongType
				.entrySet()) {
			logger.log(Level.FINE, "    symbol " + entry.getKey().getName() + " at ");
			for (Location location : entry.getValue()) {
				logger.log(Level.FINE, "      " + location);
			}
		}

		logger.log(Level.INFO, "  Symbols that are select targets, but not declared at all: "
				+ this.selectedUndeclaredSymbols.size());
		for (Entry<String, ArrayList<Location>> entry : selectedUndeclaredSymbols.entrySet()) {
			logger.log(Level.FINE, "    symbol " + entry.getKey() + " at ");
			for (Location location : entry.getValue()) {
				logger.log(Level.FINE, "      " + location);
			}
		}

		logger.log(Level.INFO, "  Symbols that are not int or hex but have range properties: "
				+ notNumberTypeSymbolsWithRanges.size());
		for (Entry<ZenglerSymbol, ArrayList<SymbolDescriptor>> entry : notNumberTypeSymbolsWithRanges
				.entrySet()) {
			logger.log(Level.FINE, "    symbol " + entry.getKey().getName()
					+ " in config block at ");
			for (SymbolDescriptor desc : entry.getValue()) {
				logger.log(Level.FINE, "      " + desc.getLocation());
			}
		}

		logger.log(Level.INFO,
				"  Undeclared symbols or unconverted unquoted constant values as range boundaries: "
						+ undeclaredBoundariesSymbols.size());
		for (Entry<String, ArrayList<Location>> entry : undeclaredBoundariesSymbols.entrySet()) {
			logger.log(Level.FINE, "    boundary " + entry.getKey() + " at ");
			for (Location location : entry.getValue()) {
				logger.log(Level.FINE, "      " + location);
			}
		}

		logger.log(Level.INFO, "  Symbols as ranges that are not int or hex: "
				+ badBoundariesSymbols.size());
		for (Entry<AttributesSymbol, ArrayList<Location>> entry : badBoundariesSymbols.entrySet()) {
			logger.log(Level.FINE, "    symbol " + entry.getKey().getName()
					+ " at ");
			for (Location location : entry.getValue()) {
				logger.log(Level.FINE, "      " + location);
			}
		}

		logger.log(Level.INFO, "  Symbols as ranges that will lead to conversion: "
				+ conversionOnBoundariesSymbols.size());
		for (Entry<AttributesSymbol, ArrayList<Location>> entry : conversionOnBoundariesSymbols
				.entrySet()) {
			AttributesSymbol symbol = entry.getKey();
			logger.log(Level.FINE, "    symbol " + symbol.getName() + "(" + symbol.getType() + ")"
					+ " at ");
			for (Location location : entry.getValue()) {
				logger.log(Level.FINE, "      " + location);
			}
		}

		logger.log(Level.INFO, "  Choice blocks: " + numberOfChoiceBlocks);

		logger.log(Level.INFO, "  Choice blocks with bad type: " + badChoiceType.size());
		for (Entry<ChoiceDescriptor, Type> entry : badChoiceType.entrySet()) {
			logger.log(Level.FINE, "    bad type " + entry.getValue().toString()
					+ " in choice block at ");
			logger.log(Level.FINE, "      " + entry.getKey().getLocation());
		}

		logger.log(Level.INFO,
				"  Choice blocks with externally set type: " + externalChoiceType.size());
		for (Entry<ChoiceDescriptor, SymbolDescriptor> entry : externalChoiceType.entrySet()) {
			logger.log(Level.FINE, "    choice block at " + entry.getKey().getLocation());
			logger.log(Level.FINE, "      via symbol descriptor \""
					+ entry.getValue().getSymbolName() + "\" at " + entry.getValue().getLocation());
		}

		logger.log(Level.INFO,
				"  Choice groups with only one entry: " + noRealChoice.size());
		for (ChoiceGroup group : noRealChoice) {
			logger.log(Level.FINE, "    choice block at " + group.getOrigin().getLocation());
		}

//		for (AnomaliesCollection anomalies : anomaliesTable.values()) {
//			numberOfAnomalies += anomalies.size();
//		}
//
//		logger.log(Level.INFO, "  Anomalies in expressions: " + numberOfAnomalies);
		for (Entry<Location, AnomaliesCollection> entry : anomaliesTable.entrySet()) {
			logger.log(Level.FINE, "    at " + entry.getKey().toString());
			for (Anomaly anomaly : entry.getValue()) {
				logger.log(Level.FINE, "      " + anomaly);
			}
		}

		logger.log(Level.INFO, "  Amount of regular variables in TPOF: " + amountOfVarsInTPOF);
		logger.log(Level.INFO, "  Amount of auxiliary variables in TPOF: " + amountOfAuxVarsInTPOF);
		logger.log(Level.INFO, "  Total amount of variables in TPOF: " + totalAmountOfVarsInTPOF);
		logger.log(Level.INFO, "  Amount of variables in PPOF: " + amountOfVarsInPPOF);

		//logger.log(Level.SEVERE, amountOfVarsInTPOF + " & " + amountOfAuxVarsInTPOF + " & " + totalAmountOfVarsInTPOF + " & " + amountOfVarsInPPOF);

		logger.log(Level.INFO, "");
	}

	public void setIncludedFiles(ArrayList<SourceFile> includedFiles) {
		if (this.includedFiles != null) {
			throw new IllegalStateException("architecture has already a file list");
		}
		this.includedFiles = includedFiles;
	}

	public void choiceTypeExternallySet(ChoiceDescriptor choice, SymbolDescriptor desc) {
		externalChoiceType.put(choice, desc);
	}

	private void symbolWithMoreThanOnePrompt(String name,
			ArrayList<SymbolDescriptor> descriptorsWithPrompts) {
		symbolsWithMoreThanOnePrompt.put(name, descriptorsWithPrompts);
	}

	private void symbolWithoutType(ZenglerSymbol zenglerSymbol) {
		symbolsWithoutType.put(zenglerSymbol.getName(), zenglerSymbol.getDescriptors());
	}

	private void symbolWithContradictoryType(String name, ArrayList<SymbolDescriptor> descriptors) {
		symbolsWithContradictoryType.put(name, descriptors);
	}

	private void symbolWithRedundantType(String name, ArrayList<SymbolDescriptor> descriptors) {
		symbolsWithRedundantType.put(name, descriptors);
	}

	public void badTypeWithRange(ZenglerSymbol zenglerSymbol, SymbolDescriptor descWithRange) {
		if (!notNumberTypeSymbolsWithRanges.containsKey(zenglerSymbol)) {
			notNumberTypeSymbolsWithRanges.put(zenglerSymbol, new ArrayList<SymbolDescriptor>());
		}
		notNumberTypeSymbolsWithRanges.get(zenglerSymbol).add(descWithRange);
	}

	public void selectOnWrongType(ZenglerSymbol zenglerSymbol, Location location) {
		if (!selectedSymbolsWithWrongType.containsKey(zenglerSymbol)) {
			selectedSymbolsWithWrongType.put(zenglerSymbol, new ArrayList<Location>());
		}
		selectedSymbolsWithWrongType.get(zenglerSymbol).add(location);
	}

	public void undeclaredSymbolSelected(String name, Location location) {
		if (!selectedUndeclaredSymbols.containsKey(name)) {
			selectedUndeclaredSymbols.put(name, new ArrayList<Location>());
		}
		selectedUndeclaredSymbols.get(name).add(location);
	}

	public void undeclaredBoundarySymbol(String boundName, Location location) {
		if (!undeclaredBoundariesSymbols.containsKey(boundName)) {
			undeclaredBoundariesSymbols.put(boundName, new ArrayList<Location>());
		}
		undeclaredBoundariesSymbols.get(boundName).add(location);
	}

	public void badBoundarySymbol(AttributesSymbol symbol, Location location) {
		if (!badBoundariesSymbols.containsKey(symbol)) {
			badBoundariesSymbols.put(symbol, new ArrayList<Location>());
		}
		badBoundariesSymbols.get(symbol).add(location);
	}

	public void conversionOnBoundarySymbol(AttributesSymbol symbol, Location location) {
		if (!conversionOnBoundariesSymbols.containsKey(symbol)) {
			conversionOnBoundariesSymbols.put(symbol, new ArrayList<Location>());
		}
		conversionOnBoundariesSymbols.get(symbol).add(location);
	}

	public void anomalies(Location location, AnomaliesCollection anomalies) {
		if (!anomaliesTable.containsKey(location)) {
			anomaliesTable.put(location, new AnomaliesCollection());
		}
		anomaliesTable.get(location).addAll(anomalies);
	}

	public void badChoiceType(ChoiceDescriptor choiceDescriptor, Type t) {
		badChoiceType.put(choiceDescriptor, t);
	}

	private void noRealChoice(ChoiceGroup group) {
		noRealChoice.add(group);
	}

	public void evaluateZenglerModel(ZenglerModel zenglerModel) {
		HashMap<String, ZenglerSymbol> symbolDatabase = zenglerModel.getSymbolDatabase().getDatabase();
		ArrayList<ChoiceDescriptor> choiceDatabase = zenglerModel.getChoiceDatabase().getDatabase();
		numberOfChoiceBlocks = choiceDatabase.size();
		numberOfDeclaredSymbols = symbolDatabase.size();

		for (ZenglerSymbol zenglerSymbol : zenglerModel.getSymbolDatabase().getDatabase().values()) {
			final String symbolName = zenglerSymbol.getName();
			ArrayList<SymbolDescriptor> descriptorsWithTypes = new ArrayList<SymbolDescriptor>();
			ArrayList<SymbolDescriptor> descriptorsWithPrompts = new ArrayList<SymbolDescriptor>();

			for (SymbolDescriptor desc : zenglerSymbol.getDescriptors()) {
				Type descriptorType = desc.getType();
				ArrayList<PromptProperty> descriptorPromptProperties = desc.getPromptProperties();

				if (descriptorType != Type.UNKNOWN) {
					descriptorsWithTypes.add(desc);
				}
				if (descriptorPromptProperties.size() > 0) {
					descriptorsWithPrompts.add(desc);
					if (descriptorPromptProperties.size() > 1) {
						configBlockWithMoreThanOnePrompt(desc);
					}
				}
//				if (desc.getPromptProperties().size() > 0 && desc.getType() == Type.TRISTATE && desc.getSelectProperties().size() > 0 && desc.getDefaultProperties().size() > 0) {
//					System.err.println(desc.getSymbolName());
//				}
			}

			if (descriptorsWithPrompts.size() > 1) {
				symbolWithMoreThanOnePrompt(symbolName, descriptorsWithPrompts);
			}

			if (descriptorsWithTypes.size() > 0) {
				Type symbolType = descriptorsWithTypes.get(0).getType();

				compareTypes: if (descriptorsWithTypes.size() > 1) {
					for (SymbolDescriptor desc : descriptorsWithTypes) {
						if (desc.getType() != symbolType) {
							symbolWithContradictoryType(symbolName, descriptorsWithTypes);
							break compareTypes;
						}
					}
					symbolWithRedundantType(symbolName, descriptorsWithTypes);
				}

				if (descriptorsWithPrompts.size() > 0) {
					symbolsWithPrompts.add(symbolName);
				}
			} else {
				symbolWithoutType(zenglerSymbol);
			}
		}

		for (ZenglerSymbol zenglerSymbol : symbolDatabase.values()) {
			final ArrayList<SymbolDescriptor> descriptors = zenglerSymbol.getDescriptors();
			numberOfConfigBlocks += descriptors.size();
		}
	}

	private static void configBlockWithMoreThanOnePrompt(SymbolDescriptor desc) {
		final Location location = desc.getLocation();
		if (!configBlocksWithMoreThanOnePrompt.containsKey(location)) {
			configBlocksWithMoreThanOnePrompt.put(location, desc.getSymbolName());
		}
	}

	public void evaluateAttributesModel(AttributesModel attributesModel) {
		HashMap<String, Type> symbolsTypes = attributesModel.getTypesMap();

		for (AttributesSymbol attributesSymbol : attributesModel.getSymbolDatabase().values()) {
			final Type t = attributesSymbol.getType();
			typeCounter.put(t, Integer.valueOf(typeCounter.get(t).intValue() + 1));
		}

		for (Entry<String, Type> typeEntry : symbolsTypes.entrySet()) {
			String symbolName = typeEntry.getKey();
			Type symbolType = typeEntry.getValue();
			HashMap<Type, TreeSet<Arch>> globalTypesMapValue = globalTypesMap.get(symbolName);
			TreeSet<Arch> archesList;

			if (globalTypesMapValue == null) {
				globalTypesMapValue = new HashMap<Type, TreeSet<Arch>>();
				globalTypesMap.put(symbolName, globalTypesMapValue);
			}

			archesList = globalTypesMapValue.get(symbolType);

			if (archesList == null) {
				archesList = new TreeSet<Arch>();
				globalTypesMapValue.put(symbolType, archesList);
			}

			archesList.add(arch);
		}

		for (ChoiceGroup group : attributesModel.getChoiceDatabase()) {
			if (group.getSymbolNames().size() == 1) {
				noRealChoice(group);
			}
		}
	}

	public void evaluateTPOF(TriSPOF tpof) {
		amountOfVarsInTPOF = tpof.numberOfVars();
		amountOfAuxVarsInTPOF = tpof.numberOfAuxVars();
		totalAmountOfVarsInTPOF = amountOfVarsInTPOF + amountOfAuxVarsInTPOF;
	}

	public void evaluatePPOF(PPOF ppof) {
		amountOfVarsInPPOF = ppof.numberOfVars();
	}
}
