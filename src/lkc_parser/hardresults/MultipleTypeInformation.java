package lkc_parser.hardresults;

import lkc_parser.parser.data.Symbol_type;
import lkc_parser.tokentree.DescribingBlock;
import lkc_parser.tokentree.Location;

public class MultipleTypeInformation {
	private final DescribingBlock describingBlock;
	private final Symbol_type oldType;
	private final Symbol_type newType;

	public MultipleTypeInformation(DescribingBlock describingBlock, Symbol_type oldType,
			Symbol_type newType) {
		this.describingBlock = describingBlock;
		this.oldType = oldType;
		this.newType = newType;
	}

	public Location getLocation() {
		return describingBlock.getLocation();
	}

	@Override
	public String toString() {
		String res;
		if (oldType == newType) {
			res = "Redundant type information (" + newType;
		} else {
			res = "Contradictory type information (" + oldType + " <> " + newType;
		}
		res += ") in declaration of symbol " + describingBlock.getName();
		return res;
	}
}
