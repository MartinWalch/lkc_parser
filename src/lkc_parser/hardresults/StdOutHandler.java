package lkc_parser.hardresults;

import java.util.logging.Formatter;
import java.util.logging.StreamHandler;

public class StdOutHandler extends StreamHandler {
	public StdOutHandler(Formatter formatter) {
		super(System.out, formatter);
	}

	@Override
	public synchronized void close() {
		flush();
	}
}
