package lkc_parser.hardresults;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class ResultsFormatter extends Formatter {

	@Override
	public String format(LogRecord record) {
		String res = record.getMessage();
		if (res == null) {
			res = "";
		}
		return res + "\n";
	}
}
