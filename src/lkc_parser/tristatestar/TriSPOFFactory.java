package lkc_parser.tristatestar;

import static lkc_parser.tristate.TriStringConstant.ε;
import static lkc_parser.tristate.TristateConstant.MOD;
import static lkc_parser.tristate.TristateConstant.NO;
import static lkc_parser.tristate.TristateConstant.YES;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import lkc_parser.attributesmodel.AttributesChoiceDatabase;
import lkc_parser.attributesmodel.AttributesModel;
import lkc_parser.attributesmodel.AttributesSymbol;
import lkc_parser.attributesmodel.AttributesSymbolDatabase;
import lkc_parser.attributesmodel.ChoiceGroup;
import lkc_parser.attributesmodel.Prompt;
import lkc_parser.attributesmodel.Select;
import lkc_parser.attributesmodel.SymbolDefault;
import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.parser.Arch;
import lkc_parser.scanner.kconf_id;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.ExpressionsList;
import lkc_parser.tristate.TriAnd;
import lkc_parser.tristate.TriEqual;
import lkc_parser.tristate.TriNot;
import lkc_parser.tristate.TriOr;
import lkc_parser.tristate.TriStringConstant;
import lkc_parser.tristate.TriSymbol;
import lkc_parser.zenglermodel2010.OptionBitVector;
import lkc_parser.zenglermodel2010.Type;

public class TriSPOFFactory {
	public static TriSPOF produce(AttributesModel attributesModel) {
		AttributesSymbolDatabase symbolDatabase = attributesModel.getSymbolDatabase();
		AttributesChoiceDatabase choiceDatabase = attributesModel.getChoiceDatabase();
		TypesMap typesMap = attributesModel.getTypesMap();
		ExpressionsList symbolEncodings = new ExpressionsList();
		ExpressionsList choiceEncodings = new ExpressionsList();
		TriSymbol modulesSym = new TriSymbol(attributesModel.getArch().getModulesSymName());

		for (AttributesSymbol attributesSymbol : symbolDatabase.values()) {
			Type type = typesMap.get(attributesSymbol.getName());
			if ((type != null) && (type.isValid)) {
				symbolEncodings.add(encodeSymbol(attributesSymbol, attributesModel.getArch(),
						modulesSym));
			}
		}

		for (ChoiceGroup choiceGroup : choiceDatabase) {
			choiceEncodings.add(encodeChoiceGroup(choiceGroup, modulesSym));
		}

		return new TriSPOF(attributesModel.getArch(), typesMap, new TriAnd(
				symbolEncodings.andConcatenation(), choiceEncodings.andConcatenation()));
	}

	private static Expression encodeSymbol(AttributesSymbol attributesSymbol, Arch arch,
			TriSymbol modulesSym) {
		Expression res;
		OptionBitVector opt = attributesSymbol.getOpt();
		if (opt.getBit(kconf_id.ENV.name)) {
			res = encodeEnvironmentSymbol(attributesSymbol, arch);
		} else {
			switch (attributesSymbol.getType()) {
			case TRISTATE:
				res = encodeTristateSymbol(attributesSymbol, modulesSym);
				break;
			case BOOL:
				res = encodeBooleanSymbol(attributesSymbol);
				break;
			case STRING:
			case INT:
			case HEX:
				res = encodeStringSymbol(attributesSymbol);
				break;
			default:
				throw new RuntimeException("invalid type ");
			}
		}

		return res;
	}

	private static Expression encodeChoiceGroup(ChoiceGroup choiceGroup, TriSymbol modulesSym) {
		Expression res;

		switch (choiceGroup.getT()) {
		case TRISTATE:
			res = encodeTristateChoiceGroup(choiceGroup, modulesSym);
			break;
		case BOOL:
			res = encodeBooleanChoiceGroup(choiceGroup);
			break;
		default:
			res = null;
		}

		return res;
	}

	private static Expression encodeTristateChoiceGroup(ChoiceGroup choiceGroup,
			TriSymbol modulesSym) {
		ExpressionsList resList = new ExpressionsList();
		Expression choiceDeps = choiceGroup.getPrompt().getDependencies().andConcatenation();
		Expression choiceDepsNotZero = new TriNot(new TriSEquivalence(choiceDeps, NO));
		ExpressionsList allSymbolExpressions = new ExpressionsList();
		ExpressionsList allSymbolYesExpressions = new ExpressionsList();
		Expression anySymbolIsYes;

		for (String symbolName : choiceGroup.getSymbolNames()) {
			allSymbolYesExpressions.add(new TriSEquivalence(new TriSymbol(symbolName), YES));
		}

		anySymbolIsYes = allSymbolYesExpressions.orConcatenation();

		for (String symbolName : choiceGroup.getSymbolNames()) {
			allSymbolExpressions.add(new TriSymbol(symbolName));
		}

		if (choiceGroup.isOptional()) {
			resList.add(new TriSImplication(new TriNot(modulesSym), new TriSImplication(
					anySymbolIsYes, choiceDepsNotZero)));
		} else {
			ExpressionsList allSymbolNotVisibleExpressions = new ExpressionsList();

			for (String symbolName : choiceGroup.getSymbolNames()) {
				TriSAuxVariable P = new TriSAuxVariable(symbolName + "-T_AUX_P");
				allSymbolNotVisibleExpressions.add(new TriSEquivalence(P, NO));
			}
			resList.add(new TriSImplication(new TriNot(modulesSym), new TriOr(new TriSEquivalence(
					anySymbolIsYes, choiceDepsNotZero), allSymbolNotVisibleExpressions
					.andConcatenation())));
		}

		resList.add(new TriSImplication(modulesSym, new TriSImplication(allSymbolExpressions
				.orConcatenation(), choiceDeps)));
		resList.add(exclusivenessConstraint(choiceGroup));

		return resList.andConcatenation();
	}

	private static Expression encodeBooleanChoiceGroup(ChoiceGroup choiceGroup) {
		ExpressionsList resList = new ExpressionsList();
		ExpressionsList allSymbolYesExpressions = new ExpressionsList();
		Expression choiceDepsNotZero = new TriNot(new TriSEquivalence(choiceGroup.getPrompt()
				.getDependencies().andConcatenation(), NO));
		Expression anySymbolIsYes;

		for (String symbolName : choiceGroup.getSymbolNames()) {
			allSymbolYesExpressions.add(new TriSEquivalence(new TriSymbol(symbolName), YES));
		}
		anySymbolIsYes = allSymbolYesExpressions.orConcatenation();

		if (choiceGroup.isOptional()) {
			resList.add(new TriSImplication(anySymbolIsYes, choiceDepsNotZero));
		} else {
			ExpressionsList allSymbolNotVisibleExpressions = new ExpressionsList();

			for (String symbolName : choiceGroup.getSymbolNames()) {
				TriSAuxVariable P = new TriSAuxVariable(symbolName + "-T_AUX_P");
				allSymbolNotVisibleExpressions.add(new TriSEquivalence(P, NO));
			}
			resList.add(new TriOr(new TriSEquivalence(anySymbolIsYes, choiceDepsNotZero),
					allSymbolNotVisibleExpressions.andConcatenation()));
		}

		resList.add(exclusivenessConstraint(choiceGroup));

		return resList.andConcatenation();
	}

	private static Expression exclusivenessConstraint(ChoiceGroup choiceGroup) {
		ExpressionsList exclusivenessList = new ExpressionsList();
		for (String symbolName : choiceGroup.getSymbolNames()) {
			Expression symbolIsActive = new TriSEquivalence(new TriSymbol(symbolName), YES);
			ExpressionsList otherSymbolInactiveExpressions = new ExpressionsList();

			for (String otherSymbolName : choiceGroup.getSymbolNames()) {
				if (!symbolName.equals(otherSymbolName)) {
					otherSymbolInactiveExpressions.add(new TriSEquivalence(new TriSymbol(
							otherSymbolName), NO));
				}
			}

			exclusivenessList.add(new TriSImplication(symbolIsActive,
					otherSymbolInactiveExpressions.andConcatenation()));
		}
		return exclusivenessList.andConcatenation();
	}

	private static Expression encodeEnvironmentSymbol(AttributesSymbol attributesSymbol, Arch arch) {
		String symbolName = attributesSymbol.getName();
		ExpressionsList resList = new ExpressionsList();
		HashSet<String> values = arch.getValuesForEnv(symbolName);
		TriSymbol symbol = new TriSymbol(symbolName);

		for (String s : values) {
			resList.add(new TriSEquivalence(symbol, new TriStringConstant(s)));
		}

		return resList.orConcatenation();
	}

	private static Expression encodeTristateSymbol(AttributesSymbol attributesSymbol,
			TriSymbol modulesSym) {
		ExpressionsList resList = new ExpressionsList();
		final String name = attributesSymbol.getName();
		TriSymbol s = new TriSymbol(name);
		TriSAuxVariable P = new TriSAuxVariable(name + TriSPOF.SUFFIX_AUX_PROMPT);
		TriSAuxVariable S = new TriSAuxVariable(name + TriSPOF.SUFFIX_AUX_SELECT);
		TriSAuxVariable D = new TriSAuxVariable(name + TriSPOF.SUFFIX_AUX_DEFAULT);
		TriSAuxVariable L = new TriSAuxVariable(name + TriSPOF.SUFFIX_AUX_LOWER_BOUND);
		TriSAuxVariable H = new TriSAuxVariable(name + TriSPOF.SUFFIX_AUX_UPPER_BOUND);
		Expression lowerTristateBound = lowerTristateBound(P, S, D);
		Expression upperTristateBound = upperTristateBound(P, S, D);

		resList.add(new TriSEquivalence(P, encodePromptAttributes(attributesSymbol.getPrompts())));
		resList.add(new TriSEquivalence(S, encodeSelectAttributes(attributesSymbol.getSelects())));
		resList.add(new TriSEquivalence(D, encodeDefaultAttributes(attributesSymbol.getDefaults())));

		resList.add(new TriSImplication(modulesSym, new TriSEquivalence(L, lowerTristateBound)));
		resList.add(new TriSImplication(new TriNot(modulesSym), new TriSEquivalence(L, new TriNot(
				new TriSEquivalence(L, NO)))));
		resList.add(new TriSImplication(modulesSym, new TriSEquivalence(H, upperTristateBound)));
		resList.add(new TriSImplication(new TriNot(modulesSym), new TriSEquivalence(H, new TriNot(
				new TriSEquivalence(H, NO)))));
		resList.add(new TriSImplication(new TriNot(modulesSym), new TriNot(new TriSEquivalence(s,
				MOD))));

		resList.add(new TriSImplication(L, s));
		resList.add(new TriSImplication(s, H));

		return resList.andConcatenation();
	}

	private static Expression lowerTristateBound(TriSAuxVariable P, TriSAuxVariable S,
			TriSAuxVariable D) {
		return new TriOr(new TriAnd(new TriSEquivalence(P, NO), D), S);
	}

	private static Expression upperTristateBound(TriSAuxVariable P, TriSAuxVariable S,
			TriSAuxVariable D) {
		return new TriOr(new TriOr(P, new TriAnd(new TriSEquivalence(P, NO), D)), S);
	}

	private static Expression encodeBooleanSymbol(AttributesSymbol attributesSymbol) {
		ExpressionsList resList = new ExpressionsList();
		final String name = attributesSymbol.getName();
		TriSymbol s = new TriSymbol(name);
		TriSAuxVariable P = new TriSAuxVariable(name + "-T_AUX_P");
		TriSAuxVariable S = new TriSAuxVariable(name + "-T_AUX_S");
		TriSAuxVariable D = new TriSAuxVariable(name + "-T_AUX_D");
		TriSAuxVariable L = new TriSAuxVariable(name + "-T_AUX_L");
		TriSAuxVariable H = new TriSAuxVariable(name + "-T_AUX_H");

		resList.add(new TriSEquivalence(P, encodePromptAttributes(attributesSymbol.getPrompts())));
		resList.add(new TriSEquivalence(S, encodeSelectAttributes(attributesSymbol.getSelects())));
		resList.add(new TriSEquivalence(D, encodeDefaultAttributes(attributesSymbol.getDefaults())));
		resList.add(new TriSEquivalence(L, new TriNot(new TriSEquivalence(lowerTristateBound(P, S,
				D), NO))));
		resList.add(new TriSEquivalence(H, new TriNot(new TriSEquivalence(upperTristateBound(P, S,
				D), NO))));
		resList.add(new TriSImplication(L, s));
		resList.add(new TriSImplication(s, H));
		resList.add(new TriNot(new TriSEquivalence(s, MOD)));

		return resList.andConcatenation();
	}

	private static Expression encodeStringSymbol(AttributesSymbol attributesSymbol) {
		ExpressionsList resList = new ExpressionsList();
		ArrayList<SymbolDefault> defaults = attributesSymbol.getDefaults();
		final String name = attributesSymbol.getName();
		TriSymbol s = new TriSymbol(name);
		TriSAuxVariable P = new TriSAuxVariable(name + "-T_AUX_P");
		ExpressionsList allDefDepsNo = new ExpressionsList();

		for (SymbolDefault symbolDefault : defaults) {
			ExpressionsList depsList = new ExpressionsList();
			allDefDepsNo.add(new TriSEquivalence(
					symbolDefault.getDependencies().andConcatenation(), NO));
			int k_d = symbolDefault.getIndex();

			for (SymbolDefault otherSymbolDefault : defaults) {
				int other_k_d = otherSymbolDefault.getIndex();
				if (other_k_d < k_d) {
					depsList.add(new TriSEquivalence(otherSymbolDefault.getDependencies()
							.andConcatenation(), NO));
				}
			}

			depsList.addAll(symbolDefault.getDependencies());
			depsList.add(new TriSEquivalence(P, NO));

			resList.add(new TriSImplication(depsList.andConcatenation(), new TriEqual(s,
					symbolDefault.getValue())));
		}

		resList.add(new TriSImplication(new TriAnd(new TriSEquivalence(P, NO), allDefDepsNo
				.andConcatenation()), new TriSEquivalence(s, ε)));

		return resList.andConcatenation();
	}

	private static Expression encodePromptAttributes(ArrayList<Prompt> prompts) {
		ExpressionsList resList = new ExpressionsList();
		Iterator<Prompt> iterator = prompts.iterator();

		while (iterator.hasNext()) {
			resList.add(iterator.next().getDependencies().andConcatenation());
		}

		return resList.orConcatenation();
	}

	private static Expression encodeSelectAttributes(ArrayList<Select> selects) {
		final ExpressionsList resList = new ExpressionsList();

		for (Select select : selects) {
			final ExpressionsList allDeps = new ExpressionsList(select.getDependencies());

			allDeps.add(new TriSymbol(select.getSelectingSymbol()));
			resList.add(allDeps.andConcatenation());
		}
		return resList.orConcatenation();
	}

	private static Expression encodeDefaultAttributes(ArrayList<SymbolDefault> defaults) {
		ExpressionsList resList = new ExpressionsList();

		for (SymbolDefault symbolDefault : defaults) {
			ExpressionsList depsList = new ExpressionsList();
			int k_d = symbolDefault.getIndex();

			for (SymbolDefault otherSymbolDefault : defaults) {
				int other_k_d = otherSymbolDefault.getIndex();
				if (other_k_d < k_d) {
					depsList.add(new TriSEquivalence(otherSymbolDefault.getDependencies()
							.andConcatenation(), NO));
				}
			}

			depsList.addAll(symbolDefault.getDependencies());
			depsList.add(symbolDefault.getValue());

			resList.add(depsList.andConcatenation());
		}

		return resList.orConcatenation();
	}
}
