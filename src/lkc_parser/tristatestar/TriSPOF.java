package lkc_parser.tristatestar;

import java.util.HashSet;
//import java.util.TreeSet;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.parser.Arch;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.TriBinaryExpression;
import lkc_parser.tristate.TriConstantSymbol;
import lkc_parser.tristate.TriSymbol;
import lkc_parser.tristate.TriUnaryExpression;
import lkc_parser.tristate.TriVariable;
import lkc_parser.tristate.TristateAtom;

public class TriSPOF {
	public static final String SUFFIX_AUX_PROMPT = "-T_AUX_P";
	public static final String SUFFIX_AUX_SELECT = "-T_AUX_S";
	public static final String SUFFIX_AUX_DEFAULT = "-T_AUX_D";
	public static final String SUFFIX_AUX_LOWER_BOUND = "-T_AUX_L";
	public static final String SUFFIX_AUX_UPPER_BOUND = "-T_AUX_H";

	private final Arch arch;
	private final TypesMap typesMap;
	private final Expression formula;

	private HashSet<String> vars = new HashSet<String>();
	private HashSet<String> aux = new HashSet<String>();
	private long numberOfAtoms = -1;

	public TriSPOF(Arch arch, TypesMap typesMap, Expression formula) {
		this.arch = arch;
		this.typesMap = typesMap;
		this.formula = formula;
	}

	public Arch getArch() {
		return arch;
	}

	public TypesMap getTypesMap() {
		return typesMap;
	}

	public Expression getFormula() {
		return formula;
	}

	private static String toString(Expression formula) {
		StringBuilder res = new StringBuilder();

		if (formula instanceof TriBinaryExpression) {
			TriBinaryExpression binaryExpression = (TriBinaryExpression) formula;
			res.append('(');
			res.append(toString(binaryExpression.left));
			res.append(' ');
			res.append(binaryExpression.operatorString());
			res.append(' ');
			res.append(toString(binaryExpression.right));
			res.append(')');
		} else if (formula instanceof TriUnaryExpression) {
			TriUnaryExpression unaryExpression = (TriUnaryExpression) formula;
			res.append(unaryExpression.operatorString());
			res.append(toString(unaryExpression.operand));
		} else if (formula instanceof TriVariable) {
			TriVariable variable = (TriVariable) formula;
			res.append(variable);
		} else {
			assert (formula instanceof TriConstantSymbol);
			TriConstantSymbol constantSymbol = (TriConstantSymbol) formula;
			res.append(constantSymbol);
		}

		return res.toString();
	}

	public void dump() {
		Expression.dump(formula);
	}

	@Override
	public String toString() {
		return toString(formula);
	}

	private long countAtoms(Expression formula) {
		if (formula instanceof TriBinaryExpression) {
			TriBinaryExpression bin = (TriBinaryExpression) formula;
			return countAtoms(bin.left) + countAtoms(bin.right);
		} else if (formula instanceof TriUnaryExpression) {
			TriUnaryExpression bin = (TriUnaryExpression) formula;
			return countAtoms(bin.operand);
		} else {
			assert (formula instanceof TristateAtom);
			if (formula instanceof TriSymbol) {
				vars.add(((TriSymbol) formula).toString());
			} else if (formula instanceof TriSAuxVariable) {
				aux.add(((TriSAuxVariable) formula).toString());
			}
			return 1L;
		}
	}

	private void init() {
		if (numberOfAtoms == -1) {
			numberOfAtoms = countAtoms(formula);
		}
	}

	public long numberOfAtoms() {
		init();

		return numberOfAtoms;
	}

	public long numberOfVars() {
		init();
		return vars.size();
	}

	public long numberOfAuxVars() {
		init();
		return aux.size();
	}
}
