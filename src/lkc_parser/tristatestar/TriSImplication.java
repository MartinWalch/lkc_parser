package lkc_parser.tristatestar;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PAnd;
import lkc_parser.propositional.PConstant;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PNot;
import lkc_parser.propositional.POr;
import lkc_parser.propositional.PVariableDatabase;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.TriBinaryExpression;
import lkc_parser.tristate.TristateOperation;

public class TriSImplication extends TriBinaryExpression implements TristateOperation {
	public TriSImplication(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String operatorString() {
		return "⇒";
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		return POr.produce(
				right.calcPi0(typesMap, vDB),
				PAnd.produce(
						PNot.produce(left.calcPi0(typesMap, vDB)),
						POr.produce(PNot.produce(left.calcPi1(typesMap, vDB)),
								right.calcPi1(typesMap, vDB))));
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return PConstant.BOTTOM;
	}
}
