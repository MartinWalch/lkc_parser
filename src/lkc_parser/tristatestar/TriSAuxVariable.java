package lkc_parser.tristatestar;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PVariableDatabase;
import lkc_parser.tristate.ExpressionSymbol;
import lkc_parser.tristate.TriVariable;
import lkc_parser.tristate.TristateAtom;

public class TriSAuxVariable extends TristateAtom implements TriVariable {
	public final String name;

	public TriSAuxVariable(String name) {
		this.name = name;
	}

	@Override
	public boolean contains(ExpressionSymbol expressionSymbol) {
		return false;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public PFormula calcPi0(TypesMap typesMap, PVariableDatabase vDB) {
		return vDB.getP0(name);
	}

	@Override
	public PFormula calcPi1(TypesMap typesMap, PVariableDatabase vDB) {
		return vDB.getP1(name);
	}
}
