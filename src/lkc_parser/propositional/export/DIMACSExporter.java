package lkc_parser.propositional.export;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import lkc_parser.propositional.PAnd;
import lkc_parser.propositional.PConstant;
import lkc_parser.propositional.PEquivalence;
import lkc_parser.propositional.PFormula;
import lkc_parser.propositional.PFormulaVisitor;
import lkc_parser.propositional.PImplication;
import lkc_parser.propositional.PNot;
import lkc_parser.propositional.POr;
import lkc_parser.propositional.PVariable;

public class DIMACSExporter implements PFormulaVisitor {
	private enum Line {
		COMMENT("c"), PROBLEM("p"), SOLUTION("s"), TIMING("t"), VARIABLE("v");
		private final String c;

		private Line(String c) {
			this.c = c;
		}

		@Override
		public String toString() {
			return c;
		}
	}

	public enum ProblemType {
		CNF("cnf"), GENERIC("sat"), XOR("satx"), EQUAL("sate"), XOR_EQUAL("satex"), ;

		private final String FORMAT;

		private ProblemType(String FORMAT) {
			this.FORMAT = FORMAT;
		}

		@Override
		public String toString() {
			return FORMAT;
		}
	}

	private enum Operator {
		AND("*"), OR("+"), NOT("-"), XOR("xor"), EQUAL("=");

		private final String op;

		private Operator(String op) {
			this.op = op;
		}

		@Override
		public String toString() {
			return op;
		}
	}

	private enum Parenthesis {
		LEFT("("), RIGHT(")");

		private final String parenthesis;

		private Parenthesis(String parenthesis) {
			this.parenthesis = parenthesis;
		}

		@Override
		public String toString() {
			return parenthesis;
		}
	}

	private enum DIMACSConstant {
		TRUE("*()"), FALSE("+()");

		private final String emptyFormula;

		private DIMACSConstant(String emptyFormula) {
			this.emptyFormula = emptyFormula;
		}

		@Override
		public String toString() {
			return emptyFormula;
		}
	}

	private ProblemType type;
	private StringBuilder stringBuilder;
	private LinkedHashMap<String, Integer> variablesTable = new LinkedHashMap<String, Integer>();

	public DIMACSExporter(ProblemType type) {
		this.type = type;
	}

	public LinkedHashMap<String, Integer> getVariablesTable() {
		return variablesTable;
	}

	public void exportPPOF(PFormula ppof, String path) throws IOException {
		stringBuilder = new StringBuilder();
		ppof.accept(this);
		BufferedWriter output = new BufferedWriter(new FileWriter(path));
		output.write(Line.COMMENT + " LKC Check DIMACS export");
		output.newLine();
		output.write(Line.COMMENT + " file generated on " + new Date());
		output.newLine();

		for (Entry<String, Integer> entry : variablesTable.entrySet()) {
			output.write(Line.COMMENT + " " + entry.getValue() + " " + entry.getKey());
			output.newLine();
		}

		output.write(Line.COMMENT.toString());
		output.newLine();

		output.write(Line.PROBLEM + " " + type + " ");

		output.write(Integer.toString(variablesTable.size()));
		output.newLine();
		output.write("(" + stringBuilder.toString() + ")");
		output.newLine();
		output.close();
	}

	@Override
	public void visit(PAnd and) {
		stringBuilder.append(Operator.AND);
		stringBuilder.append(Parenthesis.LEFT);
		and.leftOperand.accept(this);
		stringBuilder.append(' ');
		and.rightOperand.accept(this);
		stringBuilder.append(Parenthesis.RIGHT);
	}

	@Override
	public void visit(PConstant constant) {
		switch (constant) {
		case BOTTOM:
			stringBuilder.append(DIMACSConstant.FALSE);
			return;
		case TOP:
			stringBuilder.append(DIMACSConstant.TRUE);
			return;
		default:
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public void visit(PEquivalence equivalence) {
		switch (type) {
		case EQUAL:
		case XOR_EQUAL:
			stringBuilder.append(Operator.EQUAL);
			stringBuilder.append(Parenthesis.LEFT);
			equivalence.leftOperand.accept(this);
			stringBuilder.append(' ');
			equivalence.rightOperand.accept(this);
			stringBuilder.append(Parenthesis.RIGHT);
			return;
		case GENERIC:
		case XOR:
		case CNF:
			// A <=> B ↝ *(+(A-(B))+(-(A)B))
			PFormula left = equivalence.leftOperand;
			PFormula right = equivalence.rightOperand;
			stringBuilder.append(Operator.AND);
			stringBuilder.append(Parenthesis.LEFT);
			stringBuilder.append(Operator.OR);
			stringBuilder.append(Parenthesis.LEFT);
			left.accept(this);
			stringBuilder.append(' ');
			stringBuilder.append(Operator.NOT);
			stringBuilder.append(Parenthesis.LEFT);
			right.accept(this);
			stringBuilder.append(Parenthesis.RIGHT);
			stringBuilder.append(Parenthesis.RIGHT);
			stringBuilder.append(' ');
			stringBuilder.append(Operator.OR);
			stringBuilder.append(Parenthesis.LEFT);
			stringBuilder.append(' ');
			stringBuilder.append(Operator.NOT);
			stringBuilder.append(Parenthesis.LEFT);
			left.accept(this);
			stringBuilder.append(Parenthesis.RIGHT);
			right.accept(this);
			stringBuilder.append(Parenthesis.RIGHT);
			stringBuilder.append(Parenthesis.RIGHT);
			return;
		default:
			throw new UnsupportedOperationException();
		}
	}

	@Override
	public void visit(PImplication implication) {
		// A => B ↝ +(-(A)B)
		stringBuilder.append(Operator.OR);
		stringBuilder.append(Parenthesis.LEFT);
		stringBuilder.append(Operator.NOT);
		stringBuilder.append(Parenthesis.LEFT);
		implication.leftOperand.accept(this);
		stringBuilder.append(' ');
		stringBuilder.append(Parenthesis.RIGHT);
		implication.rightOperand.accept(this);
		stringBuilder.append(Parenthesis.RIGHT);
	}

	@Override
	public void visit(PNot not) {
		stringBuilder.append(Operator.NOT);
		stringBuilder.append(Parenthesis.LEFT);
		not.operand.accept(this);
		stringBuilder.append(Parenthesis.RIGHT);
	}

	@Override
	public void visit(POr or) {
		stringBuilder.append(Operator.OR);
		stringBuilder.append(Parenthesis.LEFT);
		or.leftOperand.accept(this);
		stringBuilder.append(' ');
		or.rightOperand.accept(this);
		stringBuilder.append(Parenthesis.RIGHT);
	}

	@Override
	public void visit(PVariable variable) {
		String varName = variable.name;
		Integer var = variablesTable.get(varName);
		if (var == null) {
			var = new Integer(variablesTable.size() + 1);
			variablesTable.put(varName, var);
		}

		stringBuilder.append(var);
	}
}
