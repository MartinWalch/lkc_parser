package lkc_parser.propositional;

import java.util.HashSet;

public class PPOF {
	private final PFormula formula;

	private long numberOfAtoms = -1;
	private HashSet<String> vars = new HashSet<String>();

	public PPOF(PFormula formula) {
		this.formula = formula;
	}

	public PFormula getFormula() {
		return formula;
	}

	private static void dump(PFormula formula) {
		if (formula instanceof PBinaryExpression) {
			PBinaryExpression binaryExpression = (PBinaryExpression) formula;
			System.out.print(binaryExpression.operatorString());
			dump(binaryExpression.leftOperand);
			System.out.print(' ');
			dump(binaryExpression.rightOperand);
		} else if (formula instanceof PUnaryExpression) {
			PUnaryExpression unaryExpression = (PUnaryExpression) formula;
			System.out.print(unaryExpression.operatorString());
			dump(unaryExpression.operand);
		} else if (formula instanceof PVariable) {
			PVariable variable = (PVariable) formula;
			System.out.print(variable.name);
		} else {
			assert (formula instanceof PConstant);
			PConstant constant = (PConstant) formula;
			System.out.print(constant.symbol);
		}
	}

	public void dump() {
		dump(formula);
	}

	private static String toString(PFormula formula) {
		StringBuilder res = new StringBuilder();

		if (formula instanceof PBinaryExpression) {
			PBinaryExpression binaryExpression = (PBinaryExpression) formula;
			res.append(binaryExpression.operatorString());
			res.append('(');
			res.append(toString(binaryExpression.leftOperand));
			res.append(' ');
			res.append(toString(binaryExpression.rightOperand));
			res.append(')');
		} else if (formula instanceof PUnaryExpression) {
			PUnaryExpression unaryExpression = (PUnaryExpression) formula;
			res.append(unaryExpression.operatorString());
			res.append(toString(unaryExpression.operand));
		} else if (formula instanceof PVariable) {
			PVariable variable = (PVariable) formula;
			res.append(variable.name);
		} else {
			assert (formula instanceof PConstant);
			PConstant constant = (PConstant) formula;
			res.append(constant.symbol);
		}
		return res.toString();
	}

	private long countAtoms(PFormula formula) {
		if (formula instanceof PBinaryExpression) {
			PBinaryExpression bin = (PBinaryExpression) formula;
			return countAtoms(bin.leftOperand) + countAtoms(bin.rightOperand);
		} else if (formula instanceof PUnaryExpression) {
			PUnaryExpression bin = (PUnaryExpression) formula;
			return countAtoms(bin.operand);
		} else {
			assert (formula instanceof PAtom);
			if (!(formula instanceof PConstant)) {
				assert (formula instanceof PVariable);
				vars.add(((PVariable) formula).name);
			}
			return 1L;
		}
	}

	private void init() {
		if (numberOfAtoms == -1) {
			numberOfAtoms = countAtoms(formula);
		}
	}

	public long numberOfAtoms() {
		init();
		return numberOfAtoms;
	}

	public long numberOfVars() {
		init();
		return vars.size();
	}

	@Override
	public String toString() {
		String res = PPOF.toString(formula);
		return res;
	}
}
