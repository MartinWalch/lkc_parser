package lkc_parser.propositional;

import static lkc_parser.propositional.PConstant.BOTTOM;
import static lkc_parser.propositional.PConstant.TOP;

public class PEquivalence extends PBinaryExpression {
	public static final char SYMBOL = '↔';

	private PEquivalence(PFormula leftOperand, PFormula rightOperand) {
		super(leftOperand, rightOperand);
	}

	public static PFormula produce(PFormula leftOperand, PFormula rightOperand) {
		// if (leftOperand == rightOperand) {
		// return TOP;
		// }

		if (leftOperand == TOP) {
			return rightOperand;
		}

		if (rightOperand == TOP) {
			return leftOperand;
		}

		if (leftOperand == BOTTOM) {
			if (rightOperand == TOP) {
				return BOTTOM;
			}
			return PNot.produce(rightOperand);
		}

		if (rightOperand == BOTTOM) {
			if (leftOperand == TOP) {
				return BOTTOM;
			}
			return PNot.produce(leftOperand);
		}

		return new PEquivalence(leftOperand, rightOperand);
	}

	@Override
	public char operatorString() {
		return SYMBOL;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}
}
