package lkc_parser.propositional;

import java.util.HashMap;
import java.util.HashSet;

public class Eq {
	private final HashMap<String, HashSet<String>> database = new HashMap<String, HashSet<String>>();

	public void declareSymbol(String name) {
		if (database.containsKey(name)) {
			throw new RuntimeException("Eq database already contains " + name);
		}
		database.put(name, new HashSet<String>());
	}

	public void add(String symbolName, String value) {
		if (!database.containsKey(symbolName)) {
			throw new RuntimeException("Eq database does not contain " + symbolName);
		}
		database.get(symbolName).add(value);
	}

	public HashSet<String> get(String name) {
		return database.get(name);
	}
}
