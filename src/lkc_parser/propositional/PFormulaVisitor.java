package lkc_parser.propositional;

public interface PFormulaVisitor {
	public void visit(PAnd and);
	public void visit(PConstant constant);
	public void visit(PEquivalence equivalence);
	public void visit(PImplication implication);
	public void visit(PNot not);
	public void visit(POr or);
	public void visit(PVariable variable);
}
