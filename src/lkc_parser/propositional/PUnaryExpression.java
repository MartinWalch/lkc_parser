package lkc_parser.propositional;

public abstract class PUnaryExpression implements PFormula {
	public final PFormula operand;

	public PUnaryExpression(PFormula operand) {
		if (operand == null) {
			throw new NullPointerException();
		}
		this.operand = operand;
	}

	public abstract char operatorString();
}
