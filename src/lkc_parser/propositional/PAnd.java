package lkc_parser.propositional;

import static lkc_parser.propositional.PConstant.BOTTOM;
import static lkc_parser.propositional.PConstant.TOP;

public class PAnd extends PBinaryExpression {
	public static final char SYMBOL = '∧';

	private PAnd(PFormula leftOperand, PFormula rightOperand) {
		super(leftOperand, rightOperand);
	}

	public static PFormula produce(PFormula leftOperand, PFormula rightOperand) {
		if ((leftOperand == BOTTOM) || (rightOperand == BOTTOM)) {
			return BOTTOM;
		}

		if (leftOperand == rightOperand) {
			return leftOperand;
		}

		if (leftOperand == TOP) {
			return rightOperand;
		} else if (rightOperand == TOP) {
			return leftOperand;
		}

		return new PAnd(leftOperand, rightOperand);
	}

	@Override
	public char operatorString() {
		return SYMBOL;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}
}
