package lkc_parser.propositional;

import static lkc_parser.propositional.PConstant.BOTTOM;
import static lkc_parser.propositional.PConstant.TOP;

public class PImplication extends PBinaryExpression {
	public static final char SYMBOL = '→';

	private PImplication(PFormula leftOperand, PFormula rightOperand) {
		super(leftOperand, rightOperand);
	}

	public static PFormula produce(PFormula leftOperand, PFormula rightOperand) {
		if (leftOperand == BOTTOM) {
			return TOP;
		}

		if (leftOperand == TOP) {
			return rightOperand;
		}

		if (rightOperand == TOP) {
			return TOP;
		}

		if (rightOperand == BOTTOM) {
			return PNot.produce(leftOperand);
		}

		return new PImplication(leftOperand, rightOperand);
	}

	@Override
	public char operatorString() {
		return SYMBOL;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}
}
