package lkc_parser.propositional;

import static lkc_parser.propositional.PConstant.BOTTOM;
import static lkc_parser.propositional.PConstant.TOP;

public class POr extends PBinaryExpression {
	public static final char SYMBOL = '∨';

	private POr(PFormula leftOperand, PFormula rightOperand) {
		super(leftOperand, rightOperand);
	}

	public static PFormula produce(PFormula leftOperand, PFormula rightOperand) {
		if ((leftOperand == TOP) || (rightOperand == TOP)) {
			return TOP;
		}

		if (leftOperand == rightOperand) {
			return leftOperand;
		}

		if (leftOperand == BOTTOM) {
			return rightOperand;
		} else if (rightOperand == BOTTOM) {
			return leftOperand;
		}

		return new POr(leftOperand, rightOperand);
	}

	@Override
	public char operatorString() {
		return SYMBOL;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}
}
