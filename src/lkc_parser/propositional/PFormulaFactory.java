package lkc_parser.propositional;

import static lkc_parser.tristate.TristateConstant.NO;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Map.Entry;

import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.tristate.ContextType;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.TriBinaryExpression;
import lkc_parser.tristate.TriConstantSymbol;
import lkc_parser.tristate.TriStringConstant;
import lkc_parser.tristate.TriStringOperation;
import lkc_parser.tristate.TriSymbol;
import lkc_parser.tristate.TriUnaryExpression;
import lkc_parser.tristate.TristateAtom;
import lkc_parser.tristate.TristateConstant;
import lkc_parser.tristate.TristateOperation;
import lkc_parser.tristatestar.TriSAuxVariable;
import lkc_parser.tristatestar.TriSPOF;
import lkc_parser.zenglermodel2010.Type;

public class PFormulaFactory {
	private final PVariableDatabase vDB = new PVariableDatabase();
	private final Eq eq = new Eq();
	private final G g = new G();
	private final TypesMap typesMap;

	public PFormulaFactory(TypesMap typesMap) {
		this.typesMap = typesMap;
	}

	public PVariableDatabase getvDB() {
		return vDB;
	}

	public PPOF toPPOF(TriSPOF trispof) throws IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		HashSet<String> stringSymbols = new HashSet<String>();
		PFormulasList basicConstraintsList = new PFormulasList();
		Expression fullTristateFormula = trispof.getFormula();
		PFormula translatedConstraints;

		for (Entry<String, Type> entry : typesMap.entrySet()) {
			final String name = entry.getKey();
			switch (entry.getValue()) {
			case TRISTATE:
				vDB.registerTristateVar(name);
				basicConstraintsList
						.add(PNot.produce(PAnd.produce(vDB.getP0(name), vDB.getP1(name))));
				break;
			case BOOL:
				vDB.registerTristateVar(name);
				basicConstraintsList.add(PNot.produce(vDB.getP1(name)));
				break;
			case STRING:
			case HEX:
			case INT:
				stringSymbols.add(name);
				eq.declareSymbol(name);
				break;
			default:
				throw new RuntimeException("invalid type in PPOFFactory");
			}
		}

		fullTristateFormula = clean(fullTristateFormula, ContextType.TRISTATE);

		g.init(stringSymbols);
		initEqAndG(fullTristateFormula);

		for (String symbol : stringSymbols) {
			HashSet<String> strings = eq.get(symbol);
			for (String value : strings) {
				vDB.declareSymbolEqString(symbol, value);
			}
		}

		// int maxG = 0;
		// HashSet<String> tmp = new HashSet<String>();
		for (HashSet<String> group : g) {
			for (String[] stringPair : getAllPairs(group)) {
				vDB.declareEqSymbol(stringPair[0], stringPair[1]);
			}
// 			if (group.size() > maxG) {
// 				maxG = group.size();
// 				tmp = group;
// 			}
		}
// 		System.err.println(trispof.getArch() + ": " + maxG);
// 		for (String s : tmp) {
// 			System.err.println(s);
// 		}

		for (String symbol : stringSymbols) {
			final HashSet<String> constants = eq.get(symbol);
			for (String constant : constants) {
				HashSet<String> otherConstants = new HashSet<String>(constants);
				otherConstants.remove(constant);
				PFormulasList othersNot = new PFormulasList();

				for (String otherConstant : otherConstants) {
					assert (!constant.equals(otherConstant));
					othersNot.add(PNot.produce(vDB.getSymbolEqString(symbol, otherConstant)));
				}
				basicConstraintsList.add(PImplication.produce(
						vDB.getSymbolEqString(symbol, constant), othersNot.andConcatenation()));
			}
		}

		for (HashSet<String> group : g) {
			for (String[] stringPair : getAllPairs(group)) {
				String X1 = stringPair[0];
				String X2 = stringPair[1];
				HashSet<String> eqX1 = eq.get(X1);
				HashSet<String> eqX2 = eq.get(X2);

				for (String s1 : eqX1) {
					for (String s2 : eqX2) {
						PVariable X1_s1 = vDB.getSymbolEqString(X1, s1);
						PVariable X2_s2 = vDB.getSymbolEqString(X2, s2);
						PVariable X1_X2 = vDB.getSymbolEqSymbol(X1, X2);

						if (s1.equals(s2)) {
							basicConstraintsList.add(PImplication.produce(
									PAnd.produce(X1_s1, X2_s2), X1_X2));
							basicConstraintsList.add(PImplication.produce(
									POr.produce(PAnd.produce(PNot.produce(X1_s1), X2_s2),
											PAnd.produce(X1_s1, PNot.produce(X2_s2))), X1_X2));
						} else {
							basicConstraintsList.add(PImplication.produce(
									PAnd.produce(X1_s1, X2_s2), PNot.produce(X1_X2)));
						}
					}
				}
			}
			for (String[] stringTriple : getAllTriples(group)) {
				String X1 = stringTriple[0];
				String X2 = stringTriple[1];
				String X3 = stringTriple[2];
				PVariable X1_X2 = vDB.getSymbolEqSymbol(X1, X2);
				PVariable X1_X3 = vDB.getSymbolEqSymbol(X1, X3);
				PVariable X2_X3 = vDB.getSymbolEqSymbol(X2, X3);

				basicConstraintsList.add(PImplication.produce(PAnd.produce(X1_X2, X1_X3), X2_X3));
				basicConstraintsList.add(PImplication.produce(PAnd.produce(X1_X2, X2_X3), X1_X3));
				basicConstraintsList.add(PImplication.produce(PAnd.produce(X1_X3, X2_X3), X1_X3));
			}
		}

		translatedConstraints = fullTristateFormula.calcPi0(typesMap, vDB);

		return new PPOF(
				PAnd.produce(basicConstraintsList.andConcatenation(), translatedConstraints));
	}

	private static HashSet<String[]> getAllPairs(HashSet<String> strings) {
		final HashSet<String[]> res = new HashSet<String[]>();
		final String[] array = strings.toArray(new String[strings.size()]);

		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				res.add(new String[] { array[i], array[j] });
			}
		}

		return res;
	}

	private static HashSet<String[]> getAllTriples(HashSet<String> strings) {
		final HashSet<String[]> res = new HashSet<String[]>();
		final String[] array = strings.toArray(new String[strings.size()]);

		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				for (int k = j + 1; k < array.length; k++) {
					res.add(new String[] { array[i], array[j], array[k] });
				}
			}
		}

		return res;
	}

	private Expression clean(Expression expression, ContextType contextType)
			throws IllegalArgumentException, InstantiationException, IllegalAccessException,
			InvocationTargetException {
		if (expression instanceof TristateAtom) {
			if (contextType == ContextType.TRISTATE) {
				if (expression instanceof TriSymbol) {
					TriSymbol variableSymbol = (TriSymbol) expression;
					String name = variableSymbol.name;
					Type type = typesMap.get(name);

					if ((type == null) || (!type.isValid)) {
						return clean(new TriStringConstant(name), contextType);
					}
					if (!((type == Type.TRISTATE) || (type == Type.BOOL))) {
						return NO;
					}
					return expression;
				} else if (expression instanceof TristateConstant) {
					return expression;
				} else if (expression instanceof TriStringConstant) {
					return ((TriStringConstant) expression).toTristateConstant();
				} else {
					if (!(expression instanceof TriSAuxVariable)) {
						throw new RuntimeException("unexpected tristate atom");
					}
					return expression;
				}
			}
			assert (contextType == ContextType.STRING);
			if (expression instanceof TriSymbol) {
				TriSymbol variableSymbol = (TriSymbol) expression;
				String name = variableSymbol.name;
				Type type = typesMap.get(name);

				if ((type == null) || (!type.isValid)) {
					return clean(new TriStringConstant(name), contextType);
				}
				// if (!((type == STRING) || (type == INT) || (type ==
				// HEX))) {
				// System.err.println("(WW) non-string symbol in symbol context");
				// } else {
				return expression;
				// }

			} else if (expression instanceof TristateConstant) {
				return expression;
			} else if (expression instanceof TriStringConstant) {
				return expression;
			} else {
				throw new RuntimeException("unexpected tristate atom");
			}
		} else if (expression instanceof TriStringOperation) {
			TriBinaryExpression comp = (TriBinaryExpression) expression;
			Expression left = comp.left;
			Expression right = comp.right;

			if (!((left instanceof TristateAtom) && (right instanceof TristateAtom))) {
				throw new RuntimeException("non atomar operand in string context");
			}
			Constructor<?>[] constructors = expression.getClass().getConstructors();
			return (Expression) constructors[0].newInstance(clean(left, ContextType.STRING),
					clean(right, ContextType.STRING));
		} else {
			assert (expression instanceof TristateOperation);
			if (expression instanceof TriUnaryExpression) {
				TriUnaryExpression comp = (TriUnaryExpression) expression;
				Constructor<?>[] constructors = expression.getClass().getConstructors();
				return (Expression) constructors[0].newInstance(clean(comp.operand,
						ContextType.TRISTATE));
			}
			assert (expression instanceof TriBinaryExpression);
			TriBinaryExpression comp = (TriBinaryExpression) expression;
			Constructor<?>[] constructors = expression.getClass().getConstructors();
			return (Expression) constructors[0].newInstance(clean(comp.left, ContextType.TRISTATE),
					clean(comp.right, ContextType.TRISTATE));
		}
	}

	private void processCompForEq(TriSymbol variableSymbol, TriConstantSymbol constantSymbol) {
		Type type = typesMap.get(variableSymbol.name);

		if ((type == Type.STRING) || (type == Type.INT) || (type == Type.HEX)) {
			TriStringConstant stringConstant;
			if (constantSymbol instanceof TriStringConstant) {
				stringConstant = (TriStringConstant) constantSymbol;
			} else {
				assert (constantSymbol instanceof TristateConstant);
				stringConstant = ((TristateConstant) constantSymbol).toStringConstant();
			}
			eq.add(variableSymbol.name, stringConstant.name);
		}
	}

	private void processCompForG(TriSymbol X1, TriSymbol X2) {
		Type type = typesMap.get(X1.name);

		if ((type == Type.STRING) || (type == Type.INT) || (type == Type.HEX)) {
			Type type2 = typesMap.get(X2.name);
			if (!((type2 == Type.STRING) || (type2 == Type.INT) || (type2 == Type.HEX))) {
				throw new RuntimeException("comparing string symbol with non-string symbol");
			}
			g.unite(X1.name, X2.name);
		}
	}

	private void initEqAndG(Expression expression) {
		if (expression instanceof TriStringOperation) {
			TriBinaryExpression comp = (TriBinaryExpression) expression;
			Expression left = comp.left;
			Expression right = comp.right;

			if ((left instanceof TriSymbol) && (right instanceof TriConstantSymbol)) {
				processCompForEq((TriSymbol) left, (TriConstantSymbol) right);
			} else if ((left instanceof TriConstantSymbol) && (right instanceof TriSymbol)) {
				processCompForEq((TriSymbol) right, (TriConstantSymbol) left);
			} else if ((left instanceof TriSymbol) && (right instanceof TriSymbol)) {
				processCompForG((TriSymbol) left, (TriSymbol) right);
			}

		} else if (expression instanceof TriBinaryExpression) {
			TriBinaryExpression binaryExpression = (TriBinaryExpression) expression;
			initEqAndG(binaryExpression.left);
			initEqAndG(binaryExpression.right);
		} else if (expression instanceof TriUnaryExpression) {
			TriUnaryExpression unaryExpression = (TriUnaryExpression) expression;
			initEqAndG(unaryExpression.operand);
		} else {
			assert (expression instanceof TristateAtom);
			if (expression instanceof TriSAuxVariable) {
				TriSAuxVariable auxiliaryVariable = (TriSAuxVariable) expression;
				vDB.registerTristateVar(auxiliaryVariable.name);
			}
		}
	}
}
