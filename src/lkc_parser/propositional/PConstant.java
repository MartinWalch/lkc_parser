package lkc_parser.propositional;

public enum PConstant implements PAtom {
	BOTTOM('⊥'), TOP('⊤');

	public final char symbol;

	private PConstant(char symbol) {
		this.symbol = symbol;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}
}
