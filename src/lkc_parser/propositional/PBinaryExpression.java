package lkc_parser.propositional;

public abstract class PBinaryExpression implements PFormula {
	public final PFormula leftOperand;
	public final PFormula rightOperand;

	public PBinaryExpression(PFormula leftOperand, PFormula rightOperand) {
		if (leftOperand == null || rightOperand == null) {
			throw new NullPointerException();
		}
		this.leftOperand = leftOperand;
		this.rightOperand = rightOperand;
	}

	public abstract char operatorString();
}
