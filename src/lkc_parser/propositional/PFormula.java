package lkc_parser.propositional;

public interface PFormula {
	public void accept(PFormulaVisitor visitor);
}
