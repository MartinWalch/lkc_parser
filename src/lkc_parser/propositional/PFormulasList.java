package lkc_parser.propositional;

import static lkc_parser.propositional.PConstant.BOTTOM;
import static lkc_parser.propositional.PConstant.TOP;

import java.util.ArrayList;
import java.util.Iterator;

public class PFormulasList extends ArrayList<PFormula> {
	public PFormula andConcatenation() {
		final Iterator<PFormula> iterator = iterator();
		PFormula res = null;

		if (size() == 0) {
			return TOP;
		}

		do {
			res = iterator.next();
		} while (res == TOP && iterator.hasNext());

		if (res == null) {
			return BOTTOM;
		}

		while (iterator.hasNext()) {
			PFormula next = iterator.next();
			if (next == BOTTOM) {
				return BOTTOM;
			}

			if (next != TOP) {
				res = PAnd.produce(res, next);
			}
		}

		return res;
	}

	public PFormula orConcatenation() {
		final Iterator<PFormula> iterator = iterator();
		PFormula res = null;

		if (size() == 0) {
			return BOTTOM;
		}

		do {
			res = iterator.next();
		} while (res == BOTTOM && iterator.hasNext());

		if (res == null) {
			return TOP;
		}

		while (iterator.hasNext()) {
			PFormula next = iterator.next();
			if (next == TOP) {
				return TOP;
			}

			if (next != BOTTOM) {
				res = POr.produce(res, next);
			}
		}

		return res;
	}
}
