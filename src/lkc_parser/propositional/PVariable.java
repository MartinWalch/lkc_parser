package lkc_parser.propositional;

public class PVariable implements PAtom {
	public final String name;

	public PVariable(String name) {
		this.name = name;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String toString() {
		return name;
	}
}
