package lkc_parser.propositional;

import static lkc_parser.propositional.PConstant.BOTTOM;
import static lkc_parser.propositional.PConstant.TOP;

public class PNot extends PUnaryExpression {
	public static final char SYMBOL = '¬';

	private PNot(PFormula operand) {
		super(operand);
	}

	public static PFormula produce(PFormula formula) {
		if (formula == TOP) {
			return BOTTOM;
		}
		if (formula == BOTTOM) {
			return TOP;
		}
		if (formula instanceof PNot) {
			return ((PNot) formula).operand;
		}
		return new PNot(formula);
	}

	@Override
	public char operatorString() {
		return SYMBOL;
	}

	@Override
	public void accept(PFormulaVisitor visitor) {
		visitor.visit(this);
	}
}
