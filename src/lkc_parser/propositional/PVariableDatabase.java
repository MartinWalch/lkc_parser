package lkc_parser.propositional;

import java.util.HashMap;

public class PVariableDatabase {
	private static final String SEPARATOR = "-";
	private static final String P0SUFFIX = SEPARATOR + "p0";
	private static final String P1SUFFIX = SEPARATOR + "p1";
	private static final String STRCONST = "EQCONST";
	private static final String STRSTR = "EQSYM";

	private final HashMap<String, PVariable> database = new HashMap<String, PVariable>();

	public PVariable getP0(String variableName) {
		return get(p0NameOfTri(variableName));
	}

	public PVariable getP1(String variableName) {
		return get(p1NameOfTri(variableName));
	}

	public void registerTristateVar(String variableName) {
		put(p0NameOfTri(variableName));
		put(p1NameOfTri(variableName));
	}

	public void declareSymbolEqString(String variableName, String value) {
		put(symbolEqStringName(variableName, value));
	}

	public PVariable getSymbolEqString(String variableName, String value) {
		return get(symbolEqStringName(variableName, value));
	}

	public PVariable getSymbolEqSymbol(String X1, String X2) {
		return get(symbolEqSymbolName(X1, X2));
	}

	public void declareEqSymbol(String X1, String X2) {
		put(symbolEqSymbolName(X1, X2));
	}

	private static String symbolEqStringName(String variableName, String value) {
		return STRCONST + SEPARATOR + variableName + SEPARATOR + value;
	}

	private static String symbolEqSymbolName(String X1, String X2) {
		assert (!X1.equals(X2));

		if (X1.compareTo(X2) < 0) {
			return STRSTR + SEPARATOR + X1 + SEPARATOR + X2;
		}
		return STRSTR + SEPARATOR + X2 + SEPARATOR + X1;
	}

	private static String p0NameOfTri(String variableName) {
		return variableName + P0SUFFIX;
	}

	private static String p1NameOfTri(String variableName) {
		return variableName + P1SUFFIX;
	}

	private PVariable get(String name) {
		PVariable res = database.get(name);

		if (res == null) {
			throw new RuntimeException("propositional variable not declared");
		}

		return res;
	}

	private void put(String name) {
		if (!database.containsKey(name)) {
			database.put(name, new PVariable(name));
		}
	}
}
