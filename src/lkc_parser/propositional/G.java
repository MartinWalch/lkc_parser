package lkc_parser.propositional;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class G implements Iterable<HashSet<String>> {
	private final HashMap<String, HashSet<String>> database = new HashMap<String, HashSet<String>>();

	public void init(Set<String> symbolSet) {
		database.clear();
		for (String X : symbolSet) {
			HashSet<String> unitSet = new HashSet<String>();
			unitSet.add(X);
			database.put(X, unitSet);
		}
	}

	public void unite(String X1, String X2) {
		assert (database.containsKey(X1));
		assert (database.containsKey(X2));

		HashSet<String> containingX1 = database.get(X1);
		HashSet<String> containingX2 = database.get(X2);

		assert (containingX1 != null);
		assert (containingX2 != null);

		if (containingX1 != containingX2) {
			containingX1.addAll(containingX2);

			for (String X : containingX2) {
				database.put(X, containingX1);
			}
		}
	}

	@Override
	public Iterator<HashSet<String>> iterator() {
		HashSet<HashSet<String>> res = new HashSet<HashSet<String>>();
		res.addAll(database.values());
		return res.iterator();
	}
}
