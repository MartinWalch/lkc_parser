package lkc_parser.tokentree;

public class Location implements Comparable<Location> {
	private final SourceFile file;
	private final int line;

	public Location(SourceFile file, int line) {
		this.file = file;
		this.line = line;
	}

	public SourceFile getFile() {
		return file;
	}

	public int getLine() {
		return line;
	}

	@Override
	public String toString() {
		return file + ":" + line;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + line;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (line != other.line)
			return false;
		return true;
	}

	@Override
	public int compareTo(Location o) {
		int res = file.compareTo(o.file);
		if (res == 0) {
			res = line - o.line;
		}
		return res;
	}
}
