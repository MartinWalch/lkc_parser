package lkc_parser.tokentree;

import java.util.ArrayList;
import java.util.HashMap;

import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Symbol;

public class ConfigBlock extends DescribingBlock {
	private static HashMap<String, ArrayList<ConfigBlock>> globalList = new HashMap<String, ArrayList<ConfigBlock>>();
	private static ConfigBlock currentConfigBlock;

	private final HashMap<String, String> optionsMap = new HashMap<String, String>();
	private final Symbol originalSymbol;

	private ConfigBlock(int line, Symbol symbol) {
		super(line, symbol);

		final String name = getName();
		originalSymbol = symbol;

		if (!globalList.containsKey(name)) {
			globalList.put(name, new ArrayList<ConfigBlock>());
		}

		globalList.get(name).add(this);
	}

	public static HashMap<String, ArrayList<ConfigBlock>> getGlobalList() {
		return globalList;
	}

	public static void next(int line, Symbol sym, Tree tree) {
		currentConfigBlock = new ConfigBlock(line, sym);
		DescribingBlock.next(currentConfigBlock);
		tree.add(currentConfigBlock);
	}

	public static void addPrompt(int line, String prompt, Expr if_expr) {
		currentConfigBlock.addProperty(new Prompt(line, currentConfigBlock, prompt, if_expr));
	}

	public static void addSelect(int line, Symbol sym, Expr if_expr) {
		currentConfigBlock.addProperty(new Select(line, currentConfigBlock, sym, if_expr));
	}

	public static void addImply(int line, Symbol sym, Expr if_expr) {
		currentConfigBlock.addProperty(new Imply(line, currentConfigBlock, sym, if_expr));
	}

	public static void addRange(int line, Symbol from, Symbol to, Expr if_expr) {
		currentConfigBlock.addProperty(new Range(line, currentConfigBlock, from, to, if_expr));
	}

	public static void addOption(String name, String value) {
		if (!currentConfigBlock.optionsMap.containsKey(name)) {
			currentConfigBlock.optionsMap.put(name, value);
		}
	}

	public static String getNameOfCurrent() {
		return currentConfigBlock.getName();
	}

	public HashMap<String, String> getOptions() {
		return optionsMap;
	}

	public Symbol getOriginalSymbol() {
		return originalSymbol;
	}
}
