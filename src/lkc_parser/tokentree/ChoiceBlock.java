package lkc_parser.tokentree;

import java.util.ArrayList;
import java.util.LinkedList;

import lkc_parser.hardresults.Results;
import lkc_parser.parser.data.Symbol;

public final class ChoiceBlock extends DescribingBlock {
	private static final ArrayList<ChoiceBlock> globalList = new ArrayList<ChoiceBlock>();
	private static final LinkedList<ChoiceBlock> stack = new LinkedList<ChoiceBlock>();

	private boolean optional = false;

	private ChoiceBlock(int line, Symbol symbol) {
		super(line, symbol);
		globalList.add(this);
	}

	public static ArrayList<ChoiceBlock> getGloballist() {
		return globalList;
	}

	public static void setOptional() {
		ChoiceBlock current = currentChoice();
		if (current.optional) {
			/*
			 * "optional" attribute applies to whole choice group. However,
			 * choice groups must be associated to a symbol to have several
			 * descriptors, which is a buggy feature. They do not seem to be in
			 * use at all.
			 */
			Results.repeatedOptionalChoice(current);
		} else {
			current.optional = true;
		}
	}

	public static ChoiceBlock currentChoice() {
		return stack.peek();
	}

	public static void push(ChoiceBlock currentChoice) {
		stack.push(currentChoice);
		DescribingBlock.next(currentChoice);
	}

	public static ChoiceBlock pop() {
		ChoiceBlock res = stack.pop();
		DescribingBlock.next(currentChoice());
		return res;
	}

	public static ChoiceBlock choiceLookup(int line, Symbol symbol) {
//		ChoiceBlock res;
//		if ((symbol != null) && (choicesMap.containsKey(symbol))) {
//			ArrayList<ChoiceBlock> value = choicesMap.get(symbol);
//			assert (value.size() == 1);
//			res = value.get(0);
//		} else {
//			res = new ChoiceBlock(symbol, location);
//		}
		if (symbol != null) {
			throw new UnsupportedOperationException();
		}
		return new ChoiceBlock(line, symbol);
	}

	public boolean isOptional() {
		return optional;
	}

	@Override
	public String toString() {
		final String name = getName();
		return (name != null) ? name : "anonymous choice";
	}
}
