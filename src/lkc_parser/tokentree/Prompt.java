package lkc_parser.tokentree;

import lkc_parser.parser.data.Expr;

public class Prompt extends Property {
	private final String prompt;
	private final Expr guard;

	/**
	 * 
	 * @param line
	 * @param prompt
	 *            not null
	 * @param guard
	 */
	public Prompt(int line, TreeNode node, String prompt, Expr guard) {
		super(line, node);
		if (prompt == null) {
			throw new NullPointerException();
		}
		this.prompt = prompt;
		this.guard = guard;
	}

	public String getPrompt() {
		return prompt;
	}

	public Expr getGuard() {
		return guard;
	}

	@Override
	public String toString() {
		return getPrompt();
	}
}
