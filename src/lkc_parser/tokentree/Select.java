package lkc_parser.tokentree;

import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Symbol;

public class Select extends Property {
	private final int line;
	private final Symbol selectedSymbol;
	private final Expr guard;

	public Select(int line, TreeNode node, Symbol selectedSymbol, Expr guard) {
		super(line, node);
		if (selectedSymbol == null) {
			throw new NullPointerException();
		}
		this.line = line;
		this.selectedSymbol = selectedSymbol;
		this.guard = guard;
	}

	public Symbol getSelectedSymbol() {
		return selectedSymbol;
	}

	public Expr getGuard() {
		return guard;
	}

	public int getLine() {
		return line;
	}
}
