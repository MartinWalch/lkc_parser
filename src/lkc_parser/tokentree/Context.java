package lkc_parser.tokentree;

import java.util.ArrayList;
import java.util.LinkedList;

import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Menu;
import lkc_parser.parser.data.Symbol;
import lkc_parser.tristate.ExprConverter;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.ExpressionsList;

public class Context {
	public enum ConstraintsType {
		DEPENDS, IF, VISIBLE
	}

	private final ConstraintsType type;
	//private final Expression constraint;
	private final Expr constraint;
	private final TreeNode origin;

	private Context(ConstraintsType type, Expr constraint, TreeNode origin) {
		this.type = type;
		this.constraint = constraint;
		this.origin = origin;
	}

	public static Context createDependsOnContext(Expr constraint, TreeNode origin) {
		return new Context(ConstraintsType.DEPENDS, constraint, origin);
	}

	public static Context createIfContext(Expr constraint, TreeNode origin) {
		return new Context(ConstraintsType.IF, constraint, origin);
	}

	public static Context createVisibleIfContext(Expr constraint, TreeNode origin) {
		return new Context(ConstraintsType.VISIBLE, constraint, origin);
	}

	public static ArrayList<Context> mergeStack(LinkedList<ArrayList<Context>> contextStack) {
		ArrayList<Context> res = new ArrayList<Context>();
		for (ArrayList<Context> contextList : contextStack) {
			res.addAll(contextList);
		}
		return res;
	}

	/**
	 * 
	 * @param contexts
	 * @return
	 */
	public static ExpressionsList toExpressionsListSkipVisible(ArrayList<Context> contexts) {
		ExpressionsList res = new ExpressionsList();
		for (Context context : contexts) {
			if (context.type != ConstraintsType.VISIBLE) {
				res.add(context.getConstraint());
			}
		}
		return res;
	}

	/**
	 * 
	 * @param contexts
	 * @return
	 */
	public static ExpressionsList toExpressionsList(ArrayList<Context> contexts) {
		ExpressionsList res = new ExpressionsList();
		for (Context context : contexts) {
			res.add(context.getConstraint());
		}
		return res;
	}

	public Context expandModSym(Symbol modulesSym) {
		return new Context(type, Menu.menu_check_dep(constraint, modulesSym), origin);
	}

	public ConstraintsType getType() {
		return type;
	}

	public Expression getConstraint() {
		return ExprConverter.exprToExpression(constraint);
	}

	public Location getLocation() {
		return origin.getLocation();
	}
}
