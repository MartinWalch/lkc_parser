package lkc_parser.tokentree;

import java.util.ArrayList;

import lkc_parser.parser.data.Expr;

public class MenuBlock extends TreeNode {
	private final String name;

	private final ArrayList<Context> constraints = new ArrayList<Context>();

	public MenuBlock(int line, String name) {
		super(line);
		if (name == null) {
			throw new NullPointerException();
		}
		this.name = name;
	}

	public void addDependsOnConstraint(Expr expr) {
		constraints.add(Context.createDependsOnContext(expr, this));
	}

	public void addVisibleIfConstraint(Expr expr) {
		constraints.add(Context.createVisibleIfContext(expr, this));
	}

	public String getName() {
		return name;
	}

	@Override
	public ArrayList<Context> getContextModifier() {
		return constraints;
	}

//	@Override
//	public void dump(String indentation) {
//		System.err.print(indentation);
//		System.err.println("menu");
//		for (TreeNode child : getChildren()) {
//			child.dump(indentation + "  ");
//		}
//	}
}
