package lkc_parser.tokentree;

import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Symbol;

public class Range extends Property {
	private final Symbol from;
	private final Symbol to;
	private final Expr guard;

	public Range(int line, TreeNode node, Symbol from, Symbol to, Expr guard) {
		super(line, node);
		if ((from == null) || (to == null)) {
			throw new NullPointerException();
		}
		this.from = from;
		this.to = to;
		this.guard = guard;
	}

	public Symbol getFrom() {
		return from;
	}

	public Symbol getTo() {
		return to;
	}

	public Expr getGuard() {
		return guard;
	}
}
