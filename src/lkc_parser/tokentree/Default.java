package lkc_parser.tokentree;

import lkc_parser.parser.data.Expr;

public class Default extends Property {
	private final int line;
	private final Expr value;
	private final Expr guard;

	public Default(int line, TreeNode node, Expr value, Expr guard) {
		super(line, node);

		if (value == null) {
			throw new NullPointerException();
		}
		this.line = line;
		this.value = value;
		this.guard = guard;
	}

	public int getLine() {
		return line;
	}

	public Expr getValue() {
		return value;
	}

	public Expr getGuard() {
		return guard;
	}
}
