package lkc_parser.tokentree;

public class SourceDirective extends TreeNode {
	private final String path;

	public SourceDirective(int line, String path) {
		super(line);
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		return getPath();
	}

//	@Override
//	public void dump(String indentation) {
//		System.err.print(indentation);
//		System.err.println("source");
//	}
}
