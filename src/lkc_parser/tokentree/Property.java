package lkc_parser.tokentree;

public abstract class Property implements Token {
	private final int line;
	private final TreeNode node;

	public Property(int line, TreeNode node) {
		this.line = line;
		this.node = node;
	}

	public TreeNode getNode() {
		return node;
	}

	public Location getLocation() {
		return new Location(node.getTree().getMySourceFile(), line);
	}
}
