package lkc_parser.tokentree;

import java.util.ArrayList;

import lkc_parser.parser.data.Expr;

public class IfBlock extends TreeNode {
	private final ArrayList<Context> constraint = new ArrayList<Context>();

	public IfBlock(int line, Expr expr) {
		super(line);
		if (constraint == null) {
			throw new NullPointerException();
		}
		this.constraint.add(Context.createIfContext(expr, this));
	}

	@Override
	public ArrayList<Context> getContextModifier() {
		return constraint;
	}

//	@Override
//	public void dump(String indentation) {
//		System.err.print(indentation);
//		System.err.println("if");
//		for (TreeNode child : getChildren()) {
//			child.dump(indentation + "  ");
//		}
//	}
}
