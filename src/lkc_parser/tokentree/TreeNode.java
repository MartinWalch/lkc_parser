package lkc_parser.tokentree;

import java.util.ArrayList;

public abstract class TreeNode implements Token {
	private final int line;
	private final ArrayList<TreeNode> children = new ArrayList<TreeNode>();
	private Tree myTree;

	public TreeNode(int line) {
		this.line = line;
	}

	public TreeNode(int line, Tree tree) {
		this(line);
		this.myTree = tree;
	}

	public void addChild(TreeNode node) {
		children.add(node);
		if (node.myTree != null) {
			throw new IllegalStateException("re-setting tree must never happen");
		}
		if (this.myTree == null) {
			System.err.println("null");
		}
		node.myTree = this.myTree;
	}

	public Location getLocation() {
		return new Location(myTree.getMySourceFile(), line);
	}

	public ArrayList<TreeNode> getChildren() {
		return children;
	}

	public Tree getTree() {
		return myTree;
	}

	public ArrayList<Context> getContextModifier() {
		return new ArrayList<Context>();
	}

	// public abstract void dump(String indentation);
}
