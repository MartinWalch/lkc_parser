package lkc_parser.tokentree;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import edu.tum.cup2.generator.LALR1Generator;
import edu.tum.cup2.generator.exceptions.GeneratorException;
import edu.tum.cup2.parser.LRParser;
import edu.tum.cup2.parser.exceptions.LRParserException;
import edu.tum.cup2.parser.tables.LRParsingTable;
import lkc_parser.Settings;
import lkc_parser.parser.Arch;
import lkc_parser.parser.LKCParserSpec;
import lkc_parser.scanner.LKCScanner;

public class SourceFile implements Comparable<SourceFile> {
	private static final HashMap<File, SourceFile> sourceFiles = new HashMap<File, SourceFile>();

	private static SourceFile currentSourceFile = null;

	private final HashMap<Arch, ArrayList<SourceDirective>> sourceDirectives = new HashMap<Arch, ArrayList<SourceDirective>>();
	private final File file;
	private final Tree tree = new Tree(this);

	private SourceFile(File file) {
		this.file = file;
		sourceFiles.put(file, this);
	}

	public static SourceFile getCurrentSourceFile() {
		return currentSourceFile;
	}

	public static Collection<SourceFile> getAllSourceFiles() {
		return sourceFiles.values();
	}

	public void addSourceDirective(Arch arch, SourceDirective sourceDirective) {
		if (!sourceDirectives.containsKey(arch)) {
			ArrayList<SourceDirective> directives = new ArrayList<SourceDirective>();
			sourceDirectives.put(arch, directives);
			directives.add(sourceDirective);
		} else {
			sourceDirectives.get(arch).add(sourceDirective);
		}
	}

	public ArrayList<SourceDirective> getSourceDirectives(Arch arch) {
		return sourceDirectives.get(arch);
	}

	public static SourceFile getSourceFile(File file) throws FileNotFoundException {
		File canonicalFile = null;
		SourceFile res;

		try {
			canonicalFile = file.getCanonicalFile();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		res = sourceFiles.get(canonicalFile);

		if (res == null) {
			res = new SourceFile(canonicalFile);
			try {
				res.init();
			} catch (IOException e) {
				if (e instanceof FileNotFoundException) {
					throw (FileNotFoundException) e;
				}
				e.printStackTrace();
				System.exit(-1);
			}
		}

		return res;
	}

	private void init() throws IOException {
		final LKCScanner scanner;
		final LRParsingTable parserTable;
		final LRParser parser;

		currentSourceFile = this;
		scanner = new LKCScanner(new FileReader(file));
		try {
			parserTable = new LALR1Generator(new LKCParserSpec(tree)).getParsingTable();
			parser = new LRParser(parserTable);
			parser.parse(scanner);
		} catch (GeneratorException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (LRParserException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		currentSourceFile = null;
	}

	public String getPath() {
		return file.getAbsolutePath();
	}

	public Tree getTree() {
		return tree;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SourceFile other = (SourceFile) obj;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String res;
		final String basePath = Settings.getBasePath();
		if (basePath.regionMatches(0, file.getPath(), 0, basePath.length())) {
			res = file.getPath().substring(basePath.length() + 1);
		} else {
			res = file.getPath();
		}
		return res;
	}

	@Override
	public int compareTo(SourceFile o) {
		return file.compareTo(o.file);
	}
}
