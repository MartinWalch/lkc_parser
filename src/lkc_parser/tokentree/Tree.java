package lkc_parser.tokentree;

import java.util.LinkedList;

public class Tree {
	private static class RootNode extends TreeNode {
		public RootNode(Tree tree) {
			super(0, tree);
		}

//		@Override
//		public void dump(String indentation) {
//			System.err.print(indentation);
//			System.err.println("(root)");
//			for (TreeNode child : getChildren()) {
//				child.dump(indentation + "  ");
//			}
//		}
	}

	private final SourceFile mySourceFile;
	private final RootNode root = new RootNode(this);
	private LinkedList<TreeNode> stack = new LinkedList<TreeNode>();

	public Tree(SourceFile sourceFile) {
		this.mySourceFile = sourceFile;
		stack.push(root);
	}

	public SourceFile getMySourceFile() {
		return mySourceFile;
	}

	public TreeNode getRootNode() {
		return root;
	}

	/**
	 * Add a node as child to the currently active node in the tree.
	 * 
	 * @param node
	 *            the node to be added
	 */
	public void add(TreeNode node) {
		final TreeNode current = stack.peek();
		current.addChild(node);
	}

	/**
	 * Add a node as child to the currently active node in the tree and make
	 * this node the new active node.
	 * 
	 * @param node
	 *            the node to be added
	 */
	public void push(TreeNode node) {
		add(node);
		stack.push(node);
	}

	/**
	 * Return the currently active node and make its parent the new active node.
	 * 
	 * @return the last active node
	 */
	public TreeNode pop() {
		return stack.pop();
	}
}
