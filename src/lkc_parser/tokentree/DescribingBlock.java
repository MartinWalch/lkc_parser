package lkc_parser.tokentree;

import java.util.ArrayList;

import lkc_parser.hardresults.Results;
import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Symbol;
import lkc_parser.parser.data.Symbol_type;

public abstract class DescribingBlock extends TreeNode {
	private static DescribingBlock currentDescribingBlock;

	private final String name;
	private Symbol_type type;
	private final ArrayList<Context> dependsOns = new ArrayList<Context>();
	private String helpText;
	private final ArrayList<Property> properties = new ArrayList<Property>();

	public DescribingBlock(int line, Symbol symbol) {
		super(line);
		this.name = (symbol == null) ? null : symbol.getName();
	}

	public static void next(DescribingBlock nextDescribingBlock) {
		currentDescribingBlock = nextDescribingBlock;
	}

	public static void setType(Symbol_type type) {
		if (currentDescribingBlock.type != null) {
			Results.severalTypesInBlock(currentDescribingBlock, currentDescribingBlock.type, type);
		}
		currentDescribingBlock.type = type;
	}

	public static void addDependsOn(Expr expr) {
		currentDescribingBlock.dependsOns.add(Context.createDependsOnContext(expr,
				currentDescribingBlock));
	}

	public static void setHelpText(String helpText) {
		if (currentDescribingBlock.helpText != null) {
			Results.multipleHelpTexts(currentDescribingBlock);
		}
		currentDescribingBlock.helpText = helpText;
	}

	public Symbol_type getType() {
		return type;
	}

	public String getHelpText() {
		return helpText;
	}

	public String getName() {
		return name;
	}

	public void addProperty(Property property) {
		properties.add(property);
	}

	public ArrayList<Property> getProperties() {
		return properties;
	}

	public static void addPrompt(int line, String prompt, Expr if_expr) {
		currentDescribingBlock
				.addProperty(new Prompt(line, currentDescribingBlock, prompt, if_expr));
	}

	public static void addDefault(int line, Expr target, Expr if_expr) {
		currentDescribingBlock.addProperty(new Default(line, currentDescribingBlock, target,
				if_expr));
	}

	@Override
	public ArrayList<Context> getContextModifier() {
		return dependsOns;
	}

//	@Override
//	public void dump(String indentation) {
//		System.err.print(indentation);
//		System.err.println("config:" + getName());
//		for (TreeNode child : getChildren()) {
//			child.dump(indentation + "  ");
//		}	
//	}
}
