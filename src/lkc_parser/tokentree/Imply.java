package lkc_parser.tokentree;

import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Symbol;

public class Imply extends Property {
	private final int line;
	private final Symbol impliedSymbol;
	private final Expr guard;

	public Imply(int line, TreeNode node, Symbol impliedSymbol, Expr guard) {
		super(line, node);
		if (impliedSymbol == null) {
			throw new NullPointerException();
		}
		this.line = line;
		this.impliedSymbol = impliedSymbol;
		this.guard = guard;
	}

	public Symbol getImpliedSymbol() {
		return impliedSymbol;
	}

	public Expr getGuard() {
		return guard;
	}

	public int getLine() {
		return line;
	}
}
