package lkc_parser.kerneltools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GNUMake {
	public static class MakeVersionException extends Exception {
		public MakeVersionException(String string) {
			super(string);
		}
	}

	public static boolean isMinimumMakeVersion(String[] minVersion) throws IOException, MakeVersionException {
		final String presentMakeVersion = getMakeVersionString();
		if (presentMakeVersion == null) {
			throw new MakeVersionException("could not determine version of installed GNU make");
		}
		final String[] presentVersion = presentMakeVersion.split("\\.");
		final int presentVersionLength = presentVersion.length;
		final int minVersionLength = minVersion.length;
		int i = 0;

		while (i < minVersionLength) {
			final int minNumber;
			final int presentNumber;
			if (i >= presentVersionLength) {
				return false;
			}
			minNumber = Integer.parseInt(minVersion[i]);
			presentNumber = Integer.parseInt(presentVersion[i]);
			if (presentNumber < minNumber) {
				return false;
			}
			if (presentNumber > minNumber) {
				return true;
			}
			i++;
		}

		return true;
	}

	private static String getMakeVersionString() throws IOException {
		final Runtime runtime = Runtime.getRuntime();
		String res = null;
		final String commandMakeVersion = "make --version";
		final Process process = runtime.exec(commandMakeVersion);
		final BufferedReader readerFromMake = new BufferedReader(new InputStreamReader(
				process.getInputStream()));
		final Pattern patternVersionString = Pattern.compile("^GNU Make \\p{ASCII}*$");
		String line = readerFromMake.readLine();
		final Matcher matcherVersionString = patternVersionString.matcher(line);

		if (matcherVersionString.matches()) {
			final String[] versionLine = line.split(" ", 3);
			res = versionLine[2].trim();
		} else {
			while (readerFromMake.readLine() != null) {
			}
			readerFromMake.close();
			return null;
		}
		while (readerFromMake.readLine() != null) {
		}
		readerFromMake.close();
		return res;
	}
}
