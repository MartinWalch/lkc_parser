package lkc_parser.kerneltools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lkc_parser.Settings;
import lkc_parser.kerneltools.GNUMake.MakeVersionException;
import lkc_parser.parser.Arch;

public class Linux {
	public static class LinuxException extends Exception {
		public LinuxException(String string) {
			super(string);
		}
	}

	@SuppressWarnings("unchecked")
	private static HashMap<String, File> readArchesCache(File cacheFile) throws IOException,
			ClassNotFoundException {
		final FileInputStream fileInputStream = new FileInputStream(cacheFile);
		final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		final HashMap<String, File> res = (HashMap<String, File>) objectInputStream.readObject();
		fileInputStream.close();
		objectInputStream.close();
		return res;
	}

	/**
	 * Check for entry points used by the kernel configuration system. As of
	 * this writing, there are only two: the file Kconfig in the root directory
	 * of the kernel tree and arch/x86/um/Kconfig. This method calls make with
	 * all available settings for ARCH and retrieves the setting for
	 * KBUILD_KCONFIG from make. If it is set, then that value is stored as
	 * entry point, otherwise the default is used, which is taken from Settings.
	 * 
	 * @return A HashMap<String, String> mapping ARCH values to root LKC file
	 *         paths.
	 * @throws IOException
	 * @throws KernelVersionException 
	 * @throws LinuxException
	 * @throws MakeVersionException
	 */

	public static HashMap<String, File> detectEntryFiles(File kernelRootDirectory)
			throws IOException, ClassNotFoundException, MakeVersionException, LinuxException {
		final File cacheFile = new File("LKCArchesCache");
		final String relativeMakefilePath = "Makefile";
		final File makefile = new File(kernelRootDirectory, relativeMakefilePath);
		final String commandMakeLineStub = "make -p -f " + makefile + " randconfig ARCH=";
		final ArrayList<String> arches = Arch.getAvailableArchesNames();
		final Runtime runtime = Runtime.getRuntime();
		final Pattern patternKconfig = Pattern.compile("^Kconfig ?:= ?\\p{ASCII}*$");
		HashMap<String, File> res = null;

		if ((!Settings.isRefreshCache()) && cacheFile.exists()) {
			res = readArchesCache(cacheFile);
		} else {
			final boolean isNewMakeVersion = GNUMake
					.isMinimumMakeVersion(new String[] { "3", "82" });
			final boolean oldMakefileRules = kernelVersionProbablyIncompatibleWithNewMake();
			res = new HashMap<String, File>();
			if ((!oldMakefileRules) || (!isNewMakeVersion) || Settings.isForceEntryDetection()) {
				FileOutputStream fileOutputStream = new FileOutputStream(cacheFile, false);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

				for (String arch : arches) {
					final Process makeProcess = runtime.exec(commandMakeLineStub + arch);
					final BufferedReader readerFromMake = new BufferedReader(new InputStreamReader(
							makeProcess.getInputStream()));
					String line = readerFromMake.readLine();
					String resultKconfig = null;

					while (line != null) {
						final Matcher matcherKconfig = patternKconfig.matcher(line);
						if (matcherKconfig.matches()) {
							final String[] variableValuePair = line.split(":=", 2);
							if (resultKconfig != null) {
								throw new RuntimeException(
										"Make prints more than one definition for Kconfig variable!?");
							}
							resultKconfig = variableValuePair[1].trim();
							res.put(arch, new File(resultKconfig));
						}
						line = readerFromMake.readLine();
					}
					while (readerFromMake.readLine() != null) {
					}
					readerFromMake.close();

					if (!res.containsKey(arch)) {
						throw new RuntimeException("Kconfig value for " + arch
								+ " from Makefile not found");
					}
				}

				objectOutputStream.writeObject(res);
				objectOutputStream.close();
				fileOutputStream.close();
			} else {
				throw new UnsupportedOperationException(
						"Make version >= 3.82 and old Linux kernel release found. "
								+ "A quirks mode is needed for finding the architecture dependent "
								+ "root configuration files. However, this has not been "
								+ "implemented, yet.");
			}
		}

		return res;
	}

	public static String getKernelVersion() throws IOException, LinuxException {
		final String commandKernelVersion = "make kernelversion";
		final Process process = Runtime.getRuntime().exec(commandKernelVersion);
		final BufferedReader readerFromMake = new BufferedReader(new InputStreamReader(
				process.getInputStream()));
		String line = readerFromMake.readLine();

		while (readerFromMake.readLine() != null) {
		}
		readerFromMake.close();

		if (line == null) {
			throw new LinuxException("Version detection failed");
		}
		return line;
	}

	public static boolean isMinKernelVersion(int[] version) throws IOException, LinuxException {
		String presentKernelVersionFull = getKernelVersion();
		int presentVersion[] = kernelVersionSplit(presentKernelVersionFull);

		return compareVersions(presentVersion, version) >= 0;
	}

	/**
	 *
	 * @param v1
	 * @param v2
	 * @return (v1 > v2)? -1 : (v1 < v2)? 1 : 0
	 */

	public static int compareVersions(int[] v1, int[] v2) {
		boolean v2Longer = v1.length >= v2.length;
		int minLength = Math.min(v1.length, v2.length);

		for (int i = 0; i < minLength; i++) {
			if (v1[i] > v2[i]) {
				return -1;
			}
			if (v1[i] < v2[i]) {
				return 1;
			}
		}

		if (v2Longer) {
			for (int i = minLength; i < v2.length; i++) {
				if (v2[i] > 0) {
					return -1;
				}
			}
		} else {
			for (int i = minLength; i < v1.length; i++) {
				if (v1[i] > 0) {
					return 1;
				}
			}
		}

		return 0;
	}

	public static int[] kernelVersionSplit(String versionString) {
		final String presentKernelVersion;
		final String[] presentVersionStrings;
		final int[] res;

		presentKernelVersion = versionString.split("-")[0];
		presentVersionStrings = presentKernelVersion.split("\\.");
		res = new int[4];

		for (int i = 0; i < 4; i++) {
			res[i] = (presentVersionStrings.length > i) ? Integer
					.parseInt(presentVersionStrings[i]) : 0;
		}

		return res;
	}

	private static boolean kernelVersionProbablyIncompatibleWithNewMake() throws IOException, LinuxException {
		// *not* fixed in 2.6.27.62 (EOL)
		// *not* fixed in 2.6.28 - 2.6.31 (all EOL)
		// fixed in 2.6.32.27
		// *not* fixed in 2.6.33.20 (EOL)
		// fixed in 2.6.34.9 (EOL)
		// fixed in 2.6.35.10 (EOL)
		// fixed in 2.6.36.2 (EOL)
		// fixed in 2.6.37 (EOL)
		int[] presentVersion = kernelVersionSplit(getKernelVersion());

		if (presentVersion[0] < 2) {
			return true;
		}
		if (presentVersion[0] > 2) {
			return false;
		}
		if (presentVersion[1] < 6) {
			return true;
		}
		if (presentVersion[2] < 32) {
			return true;
		}
		if (presentVersion[2] > 36) {
			return false;
		}
		if (presentVersion.length < 4) {
			return true;
		}

		switch (presentVersion[2]) {
		case 32:
			return (presentVersion[3] < 27);
		case 33:
			return true;
		case 34:
			return (presentVersion[3] < 9);
		case 35:
			return (presentVersion[3] < 10);
		case 36:
			return (presentVersion[3] < 2);
		default:
			throw new RuntimeException("Code position reached that must not be reachable");
		}
	}
}
