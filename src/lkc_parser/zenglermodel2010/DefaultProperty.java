package lkc_parser.zenglermodel2010;

import static lkc_parser.tristate.ExprConverter.exprToExpression;

import lkc_parser.tokentree.Default;
import lkc_parser.tristate.Expression;

public class DefaultProperty extends ZenglerProperty {
	private final Expression value;
	private final Expression guard;

	public DefaultProperty(Default def, int index) {
		super(index, def);

		this.value = exprToExpression(def.getValue());
		this.guard = exprToExpression(def.getGuard());
	}

	public Expression getValue() {
		return value;
	}

	public Expression getGuard() {
		return guard;
	}

	public void dump() {
		System.out.print(value + ", " + guard + ", " + getIndex());
	}
}
