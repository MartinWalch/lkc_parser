package lkc_parser.zenglermodel2010;

import java.util.ArrayList;

import lkc_parser.parser.Arch;
import lkc_parser.tokentree.ChoiceBlock;
import lkc_parser.tokentree.Default;
import lkc_parser.tokentree.Prompt;
import lkc_parser.tokentree.Property;
import lkc_parser.zenglermodel2010.ZenglerModelFactory.Index;

public class ChoiceDescriptor extends ZenglerModelObject {
	private final ZenglerChoiceDatabase database;

	private Type t;
	private final boolean optional;
	private final ArrayList<PromptProperty> promptProperties = new ArrayList<PromptProperty>();
	private final ZenglerDependencies dependencies;
	private final ArrayList<DefaultProperty> defaultProperties = new ArrayList<DefaultProperty>();
	private final ArrayList<SymbolDescriptor> descriptors = new ArrayList<SymbolDescriptor>();
	private final int index;

	public ChoiceDescriptor(ZenglerChoiceDatabase database, ChoiceBlock choiceBlock,
			ZenglerDependencies dependencies, Index index) {
		super(choiceBlock);
		this.database = database;
		this.index = index.getNext();
		this.dependencies = dependencies.extend(choiceBlock.getContextModifier());

		t = Type.convert(choiceBlock.getType());
		optional = choiceBlock.isOptional();

		for (Property property : choiceBlock.getProperties()) {
			if (property instanceof Prompt) {
				promptProperties.add(new PromptProperty((Prompt) property, index.getNext()));
			} else if (property instanceof Default) {
				defaultProperties.add(new DefaultProperty((Default) property, index.getNext()));
			} else {
				throw new RuntimeException("bug");
			}
		}
	}

	public Type getType() {
		return t;
	}

	public boolean isOptional() {
		return optional;
	}

	public ArrayList<PromptProperty> getPromptProperties() {
		return promptProperties;
	}

	public ZenglerDependencies getDependencies() {
		return dependencies;
	}

	public ArrayList<DefaultProperty> getDefaultProperties() {
		return defaultProperties;
	}

	public void addDescriptor(SymbolDescriptor desc) {
		descriptors.add(desc);
	}

	public ArrayList<SymbolDescriptor> getSymbolDescriptors() {
		return new ArrayList<SymbolDescriptor>(descriptors);
	}

	public Arch getArch() {
		return database.getArch();
	}

	public int getIndex() {
		return index;
	}

	public void dump() {
		System.out.print("(" + t.toString() + ", " + (optional ? 1 : 0) + ", ");
		for (PromptProperty prompt : promptProperties) {
			prompt.dump();
		}
		System.out.print(", ");
		dependencies.dump();
		System.out.print(", {");
		for (DefaultProperty def : defaultProperties) {
			def.dump();
		}
		System.out.print("}, {");
		for (SymbolDescriptor desc : descriptors) {
			System.out.print(desc.getIndex() + ", ");
		}
		System.out.print("}, " + index + ")");
	}
}
