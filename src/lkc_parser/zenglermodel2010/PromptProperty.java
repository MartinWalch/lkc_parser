package lkc_parser.zenglermodel2010;

import static lkc_parser.tristate.ExprConverter.exprToExpression;

import lkc_parser.parser.data.Expr;
import lkc_parser.tokentree.Prompt;
import lkc_parser.tristate.Expression;

public class PromptProperty extends ZenglerProperty {
	private final Expression guard;

	public PromptProperty(Prompt prompt, int index) {
		super(index, prompt);
		final Expr inputGuard = prompt.getGuard();

		guard = exprToExpression(inputGuard);
	}

	public Expression getGuard() {
		return guard;
	}

	public void dump() {
		System.out.print("(");
		Expression.dump(guard);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
