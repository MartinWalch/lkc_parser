package lkc_parser.zenglermodel2010;

import java.util.ArrayList;

import lkc_parser.parser.Arch;
import lkc_parser.tokentree.ConfigBlock;
import lkc_parser.tokentree.Default;
import lkc_parser.tokentree.Imply;
import lkc_parser.tokentree.Prompt;
import lkc_parser.tokentree.Property;
import lkc_parser.tokentree.Range;
import lkc_parser.tokentree.Select;
import lkc_parser.zenglermodel2010.ZenglerModelFactory.Index;

public class SymbolDescriptor extends ZenglerModelObject {
	private final Type t;
	private final ArrayList<PromptProperty> promptProperties = new ArrayList<PromptProperty>();
	private final ZenglerDependencies dep;
	private final ArrayList<SelectProperty> selectProperties = new ArrayList<SelectProperty>();
	private final ArrayList<ImplyProperty> implyProperties = new ArrayList<ImplyProperty>();
	private final ArrayList<DefaultProperty> defaultProperties = new ArrayList<DefaultProperty>();
	private final ArrayList<RangeProperty> rangeProperties = new ArrayList<RangeProperty>();
	private final OptionBitVector opt;
	private final int index;

	private final ZenglerSymbol zenglerSymbol;
//private final ConfigBlock origin;
	/*
	 * The original kernel configuration system is able to store multiple
	 * symbols with the same name. So can our parser. However, it boils down to
	 * having maximally a constant symbol and a regular non-constant symbol with
	 * the same name. An additional choice option could also have the same name.
	 * However, as choice options are only used anonymously in the mainline
	 * kernel, this does not happen. Even for other branches, such a name
	 * conflict will usually be avoided, because in our tests this lead to
	 * undesired side effects. In the highly unlikely case of such a name
	 * conflict, this program will terminate, throwing an exception.
	 */

	/*
	 * type declarations are evaluated IFF
	 * 
	 * 1. source command matches
	 * 
	 * other content of symbol declarations is evaluated IFF:
	 * 
	 * 2. all surrounding if blocks evaluate to true 3. local and inherited
	 * "depends on" expressions evaluate to true
	 * 
	 * so it should be safe to mix "if" block dependencies and "depends on"
	 * dependencies
	 */

	public SymbolDescriptor(ConfigBlock configBlock, ZenglerDependencies dependencies,
			ZenglerSymbol zenglerSymbol, Index index) {
		super(configBlock);
		//this.origin = configBlock;
		final ArrayList<Property> properties = configBlock.getProperties();

		t = Type.convert(configBlock.getType());
		opt = new OptionBitVector();

		this.index = index.getNext();

		this.dep = dependencies.extend(configBlock.getContextModifier());

		for (Property property : properties) {
			if (property instanceof Prompt) {
				PromptProperty promptProperty = new PromptProperty((Prompt) property,
						index.getNext());
				promptProperties.add(promptProperty);
			} else if (property instanceof Select) {
				SelectProperty selectProperty = new SelectProperty((Select) property,
						index.getNext());
				selectProperties.add(selectProperty);
			} else if (property instanceof Imply) {
				ImplyProperty implyProperty = new ImplyProperty((Imply) property, index.getNext());
				implyProperties.add(implyProperty);
			} else if (property instanceof Default) {
				DefaultProperty defaultProperty = new DefaultProperty((Default) property,
						index.getNext());
				defaultProperties.add(defaultProperty);
			} else if (property instanceof Range) {
				RangeProperty rangeProperty = new RangeProperty((Range) property, index.getNext());
				rangeProperties.add(rangeProperty);
			} else {
				throw new RuntimeException("bug");
			}
		}

		for (String optionName : configBlock.getOptions().keySet()) {
			opt.activateBit(optionName);
		}

		this.zenglerSymbol = zenglerSymbol;
	}

	public Type getType() {
		return t;
	}

	public ArrayList<PromptProperty> getPromptProperties() {
		return promptProperties;
	}

	public ZenglerDependencies getDependencies() {
		return dep;
	}

	public ArrayList<SelectProperty> getSelectProperties() {
		return selectProperties;
	}

	public ArrayList<DefaultProperty> getDefaultProperties() {
		return defaultProperties;
	}

	public ArrayList<RangeProperty> getRangeProperties() {
		return rangeProperties;
	}

	public OptionBitVector getOpt() {
		return opt;
	}

	public String getSymbolName() {
		return zenglerSymbol.getName();
	}

	public Arch getArch() {
		return zenglerSymbol.getArch();
	}

	public int getIndex() {
		return index;
	}

	public void dump() {
		System.out.print("(" + t + ", {");
		for (PromptProperty prompt : promptProperties) {
			prompt.dump();
		}
		System.out.print("}, ");
		dep.dump();
		System.out.print(", {");
		for (SelectProperty select : selectProperties) {
			select.dump();
		}
		System.out.print("}, {");

		for (DefaultProperty def : defaultProperties) {
			def.dump();
		}
		System.out.print("}, {");
		for (RangeProperty range : rangeProperties) {
			range.dump();
		}
		System.out.print("}, ");
		opt.dump();
		System.out.print(", " + index + ")");
	}
//	public ConfigBlock getOrigin() {
//		return origin;
//	}
}
