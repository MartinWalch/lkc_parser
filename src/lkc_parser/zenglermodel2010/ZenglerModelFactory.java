package lkc_parser.zenglermodel2010;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;

import lkc_parser.hardresults.Results;
import lkc_parser.parser.Arch;
import lkc_parser.scanner.kconf_id;
import lkc_parser.tokentree.ChoiceBlock;
import lkc_parser.tokentree.ConfigBlock;
import lkc_parser.tokentree.Context;
import lkc_parser.tokentree.SourceDirective;
import lkc_parser.tokentree.SourceFile;
import lkc_parser.tokentree.TreeNode;

public class ZenglerModelFactory {
	private static Arch arch = null;
	private static ZenglerSymbolDatabase symbolDatabase = null;
	private static ZenglerChoiceDatabase choiceDatabase = null;
	private static LinkedList<ArrayList<Context>> contextStack = null;
	private static LinkedList<ChoiceDescriptor> choiceStack = null;
	private static ArrayList<SourceFile> filesList = null;
	private static boolean currentChoiceContainsDescriptor = false;
	private static ZenglerModel zenglerModel = null;
	private static Index index = null;

	public static class Index {
		private int myIndex = 0;

		public int getNext() {
			return myIndex++;
		}
	}

	public static ZenglerModel produce(Arch architecture) throws FileNotFoundException {
		arch = architecture;
		zenglerModel = new ZenglerModel(arch);
		symbolDatabase = zenglerModel.getSymbolDatabase();
		choiceDatabase = zenglerModel.getChoiceDatabase();
		contextStack = new LinkedList<ArrayList<Context>>();
		choiceStack = new LinkedList<ChoiceDescriptor>();
		filesList = new ArrayList<SourceFile>();
		currentChoiceContainsDescriptor = false;
		index = new Index();

		processSourceFile(SourceFile.getSourceFile(arch.getEntryPoint()));

		Results.getResults(arch).setIncludedFiles(filesList);

		return zenglerModel;
	}

	private static void processChoiceBlock(ChoiceBlock choiceBlock) {
		final ArrayList<Context> contexts = Context.mergeStack(contextStack);
		final ZenglerDependencies dependencies = new ZenglerDependencies(contexts);
		final ChoiceDescriptor choice = new ChoiceDescriptor(choiceDatabase, choiceBlock,
				dependencies, index);

		choiceDatabase.addChoice(choice);

		choiceStack.push(choice);
		currentChoiceContainsDescriptor = false;

		for (TreeNode node : choiceBlock.getChildren()) {
			processSubTree(node);
		}

		if (!currentChoiceContainsDescriptor) {
			Results.emptyChoice(choiceBlock);
		}
		choiceStack.pop();
	}

	private static void processConfigBlock(ConfigBlock configBlock) {
		final ZenglerDependencies dependencies = new ZenglerDependencies(
				Context.mergeStack(contextStack));
		final SymbolDescriptor desc;

		desc = symbolDatabase.newDescriptor(configBlock, dependencies, index);

		if (desc.getOpt().getBit(kconf_id.MODULES.name)) {
			arch.setModulesSymName(desc.getSymbolName());
		}

		

		if (!choiceStack.isEmpty()) {
			choiceStack.peek().addDescriptor(desc);
			currentChoiceContainsDescriptor = true;
		}
	}

	private static void processSubTree(TreeNode node) {
		if (node instanceof ChoiceBlock) {
			processChoiceBlock((ChoiceBlock) node);
		} else if (node instanceof ConfigBlock) {
			processConfigBlock((ConfigBlock) node);
		} else if (node instanceof SourceDirective) {
			SourceDirective sourceDirective = (SourceDirective) node;
			String path = sourceDirective.getPath();

			try {
				SourceFile sourceFile = SourceFile.getSourceFile(new File(arch.expandPath(path)));

				// LKC really accepts such cases and kernel developers are
				// actually doing this :(
				// if (!filesList.contains(sourceFile)) {
				processSourceFile(sourceFile);
				// }
				sourceFile.addSourceDirective(arch, sourceDirective);
			} catch (FileNotFoundException e) {
				Results.deadSourceFile(sourceDirective);
			}
		} else {
			contextStack.push(node.getContextModifier());
			for (TreeNode child : node.getChildren()) {
				processSubTree(child);
			}
			contextStack.pop();
		}
	}

	private static void processSourceFile(SourceFile sourceFile) {
		filesList.add(sourceFile);
		processSubTree(sourceFile.getTree().getRootNode());
	}
}
