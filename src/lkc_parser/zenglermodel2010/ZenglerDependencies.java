package lkc_parser.zenglermodel2010;

import java.util.ArrayList;

import lkc_parser.tokentree.Context;
import lkc_parser.tristate.Expression;

public class ZenglerDependencies {
	// TODO: finally find out whether there is (was? will be?) a difference
	// between "if" and "depends on" dependencies (↑LKML)
	private final ArrayList<Context> contexts;

	public ZenglerDependencies(ArrayList<Context> contexts) {
		this.contexts = contexts;
		// can not "clean" here, because symbol types may still be missing
	}

	public ArrayList<Context> getContexts() {
		return contexts;
	}

	/**
	 * 
	 * @param context
	 * @return new Dependencies object with the same content and context
	 *         additionally
	 */
	public ZenglerDependencies extend(ArrayList<Context> additionalContexts) {
		final ArrayList<Context> newContexts = new ArrayList<Context>(contexts);
		newContexts.addAll(additionalContexts);

		return new ZenglerDependencies(newContexts);
	}

	public void dump() {
		ArrayList<Expression> ifs = new ArrayList<Expression>();
		ArrayList<Expression> depends = new ArrayList<Expression>();
		ArrayList<Expression> visibles = new ArrayList<Expression>();

		for (Context context : contexts) {
			switch (context.getType()) {
			case IF:
				ifs.add(context.getConstraint());
				break;
			case DEPENDS:
				depends.add(context.getConstraint());
				break;
			case VISIBLE:
				visibles.add(context.getConstraint());
				break;
			}
		}

		System.out.print("({");
		for (Expression e : ifs) {
			Expression.dump(e);
			System.out.print(", ");
		}
		System.out.print("}, {");
		for (Expression e : depends) {
			Expression.dump(e);
			System.out.print(", ");
		}
		System.out.print("}, {");
		for (Expression e : visibles) {
			Expression.dump(e);
			System.out.print(", ");
		}
		System.out.print("})");
	}
}
