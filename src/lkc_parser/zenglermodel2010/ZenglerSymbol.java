package lkc_parser.zenglermodel2010;

import java.util.ArrayList;

import lkc_parser.parser.Arch;

public class ZenglerSymbol {
	private final String name;
	private final ArrayList<SymbolDescriptor> descriptors = new ArrayList<SymbolDescriptor>();

	private final ZenglerSymbolDatabase symbolDatabase;

	public ZenglerSymbol(String name, ZenglerSymbolDatabase symbolDatabase) {
		this.name = name;
		this.symbolDatabase = symbolDatabase;
	}

	public void addDescriptor(SymbolDescriptor symbolDescriptor) {
		descriptors.add(symbolDescriptor);
	}

	public String getName() {
		return name;
	}

	public Arch getArch() {
		return symbolDatabase.getArch();
	}

	public ArrayList<SymbolDescriptor> getDescriptors() {
		return descriptors;
	}

	public void dump() {
		System.out.print("(" + name + ", {");
		for (SymbolDescriptor desc : descriptors) {
			desc.dump();
			System.out.print(", ");
		}
		System.out.print("})");
	}
}
