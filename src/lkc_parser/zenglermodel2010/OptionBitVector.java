package lkc_parser.zenglermodel2010;

import java.util.HashMap;

import lkc_parser.scanner.kconf_id;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class OptionBitVector extends HashMap<String, Boolean> {
	{
		put(kconf_id.ENV.name, FALSE);
		put(kconf_id.MODULES.name, FALSE);
		put(kconf_id.DEFCONFIG_LIST.name, FALSE);
		put(kconf_id.ALLNOCONFIG_Y.name, FALSE);
	}

	public void activateBit(String name) {
		if (containsKey(name)) {
			putBit(name, TRUE);
		} else {
			// unknown option
		}
	}

	public boolean getBit(String name) {
		return get(name);
	}

	private void putBit(String name, boolean bit) {
		if (bit) {
			put(name, TRUE);
		} else {
			put(name, FALSE);
		}
	}

	public OptionBitVector or(OptionBitVector other) {
		OptionBitVector res = new OptionBitVector();
		for (String name : res.keySet()) {
			res.putBit(name, getBit(name) || other.getBit(name));
		}
		return res;
	}

	private int getBitAsInt(String name) {
		return (get(name)) ? 1 : 0;
	}

	public void dump() {
		System.out.print("(" + getBitAsInt(kconf_id.ENV.name) + ", "
				+ getBitAsInt(kconf_id.MODULES.name) + ", "
				+ getBitAsInt(kconf_id.DEFCONFIG_LIST.name) + ", "
				+ getBitAsInt(kconf_id.ALLNOCONFIG_Y.name) + ")");
	}
}
