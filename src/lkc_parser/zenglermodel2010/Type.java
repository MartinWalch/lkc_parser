package lkc_parser.zenglermodel2010;

import lkc_parser.parser.data.Symbol_type;

public enum Type {
	TRISTATE(true, false), BOOL(true, false), STRING(true, true), INT(true, true), HEX(true, true), UNKNOWN(
			false, false), INVALID(false, false);

	public final boolean isValid;
	public final boolean isStringLike;

	private Type(boolean isValid, boolean isStringLike) {
		this.isValid = isValid;
		this.isStringLike = isStringLike;
	}

	public static Type convert(Symbol_type type) {
		if (type == null) {
			return UNKNOWN;
		}
		switch (type) {
		case S_TRISTATE:
			return TRISTATE;
		case S_BOOLEAN:
			return BOOL;
		case S_STRING:
			return STRING;
		case S_INT:
			return INT;
		case S_HEX:
			return HEX;
		case S_UNKNOWN:
			return UNKNOWN;
		case S_OTHER:
			System.err.println("(WW) Symbol_type " + Symbol_type.S_OTHER
					+ " found. Resolving to INVALID.");
			return INVALID;
		default:
			throw new RuntimeException("code missing for type \"" + Symbol_type.S_OTHER + "\"");
		}
	}

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
