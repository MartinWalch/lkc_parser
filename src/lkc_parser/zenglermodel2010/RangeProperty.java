package lkc_parser.zenglermodel2010;

import static lkc_parser.tristate.ExprConverter.exprToExpression;

import lkc_parser.parser.data.Expr;
import lkc_parser.tokentree.Range;
import lkc_parser.tristate.Expression;
import lkc_parser.tristate.ExpressionSymbol;

public class RangeProperty extends ZenglerProperty {
	private final ExpressionSymbol from;
	private final ExpressionSymbol to;
	private final Expression guard;

	public RangeProperty(Range range, int index) {
		super(index, range);
		final Expr inputGuard = range.getGuard();

		from = (ExpressionSymbol) exprToExpression(range.getFrom());
		to = (ExpressionSymbol) exprToExpression(range.getTo());
		guard = exprToExpression(inputGuard);
	}

	public ExpressionSymbol getFrom() {
		return from;
	}

	public ExpressionSymbol getTo() {
		return to;
	}

	/**
	 * 
	 * @return guard as Expression, null if there is no guard
	 */
	public Expression getGuard() {
		return guard;
	}

	public void dump() {
		System.out.print("(");
		Expression.dump(from);
		System.out.print(", ");
		Expression.dump(to);
		System.out.print(", ");
		Expression.dump(guard);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
