package lkc_parser.zenglermodel2010;

import lkc_parser.parser.Arch;

public class ZenglerModel {
	private final Arch arch;
	private final ZenglerSymbolDatabase symbolDatabase;
	private final ZenglerChoiceDatabase choiceDatabase;

	public ZenglerModel(Arch arch) {
		this.arch = arch;
		this.symbolDatabase = new ZenglerSymbolDatabase(this);
		this.choiceDatabase = new ZenglerChoiceDatabase(this);
	}

	public Arch getArch() {
		return arch;
	}

	public ZenglerSymbolDatabase getSymbolDatabase() {
		return symbolDatabase;
	}

	public ZenglerChoiceDatabase getChoiceDatabase() {
		return choiceDatabase;
	}

	public void dump() {
		symbolDatabase.dump();
		choiceDatabase.dump();
	}
}
