package lkc_parser.zenglermodel2010;

import java.util.ArrayList;

import lkc_parser.parser.Arch;

public class ZenglerChoiceDatabase {
	private final ZenglerModel zenglerModel;
	private final ArrayList<ChoiceDescriptor> database = new ArrayList<ChoiceDescriptor>();

	public ZenglerChoiceDatabase(ZenglerModel zenglerModel) {
		this.zenglerModel = zenglerModel;
	}

	public void addChoice(ChoiceDescriptor choice) {
		database.add(choice);
	}

	public ArrayList<ChoiceDescriptor> getDatabase() {
		return new ArrayList<ChoiceDescriptor>(database);
	}

	public Arch getArch() {
		return zenglerModel.getArch();
	}

	public void dump() {
		for (ChoiceDescriptor desc : database) {
			desc.dump();
		}
		System.out.println();
	}
}
