package lkc_parser.zenglermodel2010;

import lkc_parser.tokentree.Location;
import lkc_parser.tokentree.TreeNode;

public abstract class ZenglerModelObject {
	private final TreeNode origin;

	ZenglerModelObject(TreeNode origin) {
		this.origin = origin;
	}

	public Location getLocation() {
		return origin.getLocation();
	}

	public TreeNode getTreeNode() {
		return origin;
	}
}
