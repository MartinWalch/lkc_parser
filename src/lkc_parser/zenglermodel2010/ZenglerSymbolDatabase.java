package lkc_parser.zenglermodel2010;

import java.util.HashMap;

import lkc_parser.parser.Arch;
import lkc_parser.tokentree.ConfigBlock;
import lkc_parser.zenglermodel2010.ZenglerModelFactory.Index;

public class ZenglerSymbolDatabase {
	private final ZenglerModel zenglerModel;
	private final HashMap<String, ZenglerSymbol> database = new HashMap<String, ZenglerSymbol>();

	public ZenglerSymbolDatabase(ZenglerModel zenglerModel) {
		this.zenglerModel = zenglerModel;
	}

	public SymbolDescriptor newDescriptor(ConfigBlock configBlock,
			ZenglerDependencies dependencies, Index index) {
		final ZenglerSymbol symbol;
		final String symbolName = configBlock.getName();
		SymbolDescriptor res;

		if (!database.containsKey(symbolName)) {
			database.put(symbolName, new ZenglerSymbol(symbolName, this));
		}
		symbol = database.get(symbolName);
		res = new SymbolDescriptor(configBlock, dependencies, symbol, index);
		symbol.addDescriptor(res);

		return res;
	}

	public HashMap<String, ZenglerSymbol> getDatabase() {
		return database;
	}

	public ZenglerSymbol lookup(String name) {
		return database.get(name);
	}

	public Arch getArch() {
		return zenglerModel.getArch();
	}

	public void dump() {
		System.out.print("{");
		for (ZenglerSymbol symbol : database.values()) {
			symbol.dump();
			System.out.print(", ");
		}
		System.out.println("}");
	}
}
