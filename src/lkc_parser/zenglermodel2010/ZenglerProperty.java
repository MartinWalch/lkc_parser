package lkc_parser.zenglermodel2010;

import lkc_parser.tokentree.Location;
import lkc_parser.tokentree.Property;

public abstract class ZenglerProperty {
	private final int index;
	private final Property origin;

	public ZenglerProperty(int index, Property origin) {
		this.index = index;
		this.origin = origin;
	}

	public int getIndex() {
		return index;
	}

	public Property getOrigin() {
		return origin;
	}

	public Location getLocation() {
		return origin.getLocation();
	}
}
