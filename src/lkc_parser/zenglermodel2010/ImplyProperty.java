package lkc_parser.zenglermodel2010;

import static lkc_parser.tristate.ExprConverter.exprToExpression;

import lkc_parser.tokentree.Imply;
import lkc_parser.tristate.Expression;

public class ImplyProperty extends ZenglerProperty {
	private final String symbol;
	private final Expression guard;

	public ImplyProperty(Imply imply, int index) {
		super(index, imply);
		assert (imply != null);

		this.symbol = imply.getImpliedSymbol().getName();
		this.guard = exprToExpression(imply.getGuard());
	}

	public String getSymbol() {
		return symbol;
	}

	public Expression getGuard() {
		return guard;
	}

	public void dump() {
		System.out.print("(" + symbol);
		System.out.print(", ");
		Expression.dump(guard);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
