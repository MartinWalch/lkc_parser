package lkc_parser.zenglermodel2010;

import static lkc_parser.tristate.ExprConverter.exprToExpression;

import lkc_parser.tokentree.Select;
import lkc_parser.tristate.Expression;

public class SelectProperty extends ZenglerProperty {
	private final String symbol;
	private final Expression guard;

	public SelectProperty(Select select, int index) {
		super(index, select);
		assert (select != null);

		this.symbol = select.getSelectedSymbol().getName();
		this.guard = exprToExpression(select.getGuard());
	}

	public String getSymbol() {
		return symbol;
	}

	public Expression getGuard() {
		return guard;
	}

	public void dump() {
		System.out.print("(" + symbol);
		System.out.print(", ");
		Expression.dump(guard);
		System.out.print(", ");
		System.out.print(getIndex());
		System.out.print(")");
	}
}
