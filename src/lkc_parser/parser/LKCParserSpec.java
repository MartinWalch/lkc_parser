package lkc_parser.parser;

import static lkc_parser.parser.data.Expr.expr_alloc_comp;
import static lkc_parser.parser.data.Expr.expr_alloc_one;
import static lkc_parser.parser.data.Expr.expr_alloc_symbol;
import static lkc_parser.parser.data.Expr.expr_alloc_two;
import static lkc_parser.parser.data.Expr_type.E_AND;
import static lkc_parser.parser.data.Expr_type.E_EQUAL;
import static lkc_parser.parser.data.Expr_type.E_GEQ;
import static lkc_parser.parser.data.Expr_type.E_GTH;
import static lkc_parser.parser.data.Expr_type.E_LEQ;
import static lkc_parser.parser.data.Expr_type.E_LTH;
import static lkc_parser.parser.data.Expr_type.E_NOT;
import static lkc_parser.parser.data.Expr_type.E_OR;
import static lkc_parser.parser.data.Expr_type.E_UNEQUAL;
import static lkc_parser.parser.data.Symbol.sym_lookup;

import java.util.ArrayList;

import edu.tum.cup2.grammar.NonTerminal;
import edu.tum.cup2.grammar.Terminal;
import edu.tum.cup2.semantics.Action;
import edu.tum.cup2.semantics.SymbolValue;
import edu.tum.cup2.spec.CUP2Specification;
import lkc_parser.hardresults.Results;
import lkc_parser.parser.data.Expr;
import lkc_parser.parser.data.Expr_type;
import lkc_parser.parser.data.Symbol;
import lkc_parser.parser.data.Symbol.SYMBOL;
import lkc_parser.parser.data.Symbol_type;
import lkc_parser.scanner.kconf_id;
import lkc_parser.tokentree.ChoiceBlock;
import lkc_parser.tokentree.ConfigBlock;
import lkc_parser.tokentree.DescribingBlock;
import lkc_parser.tokentree.IfBlock;
import lkc_parser.tokentree.Location;
import lkc_parser.tokentree.MenuBlock;
import lkc_parser.tokentree.SourceDirective;
import lkc_parser.tokentree.Tree;
import lkc_parser.tokentree.TreeNode;

// Import last. Known bug in Oracle javac 
import static lkc_parser.scanner.LKCScanner.zconf_lineno;
import static lkc_parser.parser.LKCParserSpec.NonTerminals.*;
import static lkc_parser.parser.LKCParserSpec.Terminals.*;

public class LKCParserSpec extends CUP2Specification {

	public enum Terminals implements Terminal {
		T_MAINMENU,
		T_ENDMENU,
		T_MENU,
		T_SOURCE,
		T_CHOICE,
		T_ENDCHOICE,
		T_COMMENT,
		T_CONFIG,
		T_MENUCONFIG,
		T_HELP,
		T_HELPTEXT,
		T_IF,
		T_ENDIF,
		T_DEPENDS,
		T_OPTIONAL,
		T_PROMPT,
		T_TYPE,
		T_DEFAULT,
		T_SELECT,
		T_IMPLY,
		T_RANGE,
		T_VISIBLE,
		T_OPTION,
		T_ON,
		T_WORD,
		T_WORD_QUOTE,
		T_UNEQUAL,
		T_LESS,
		T_LESS_EQUAL,
		T_GREATER,
		T_GREATER_EQUAL,
		T_CLOSE_PAREN,
		T_OPEN_PAREN,
		T_EOL,
		T_OR,
		T_AND,
		T_EQUAL,
		T_NOT,
		T_OPT_MODULES,
		T_OPT_DEFCONFIG_LIST,
		T_OPT_ENV,
		T_OPT_ALLNOCONFIG_Y
		;
	}

	public enum NonTerminals implements NonTerminal {
		prompt,
		symbol,
		expr,
		if_expr,
		end,
		if_entry,
		menu_entry,
		choice_entry,
		symbol_option_arg,
		word_opt,
		input,
		kstart,
		stmt_list,
		common_stmt,
		config_entry_start,
		config_stmt,
		menuconfig_entry_start,
		menuconfig_stmt,
		config_option_list,
		config_option,
		symbol_option,
		symbol_option_list,
		choice,
		choice_end,
		choice_stmt,
		choice_option_list,
		choice_option,
		choice_block,
		if_end,
		if_stmt,
		if_block,
		mainmenu_stmt,
		menu,
		menu_end,
		menu_stmt,
		menu_block,
		source_stmt,
		comment,
		comment_stmt,
		help_start,
		help,
		depends_list,
		depends,
		visibility_list,
		visible,
		prompt_stmt_opt,
		nl
		;
	}

	public static class T_CHOICE extends SymbolValue<kconf_id> {}
	public static class T_COMMENT extends SymbolValue<kconf_id> {}
	public static class T_CONFIG extends SymbolValue<kconf_id> {}
	public static class T_DEFAULT extends SymbolValue<kconf_id> {}
	public static class T_DEPENDS extends SymbolValue<kconf_id> {}
	public static class T_ENDCHOICE extends SymbolValue<kconf_id> {}
	public static class T_ENDIF extends SymbolValue<kconf_id> {}
	public static class T_ENDMENU extends SymbolValue<kconf_id> {}
	public static class T_HELP extends SymbolValue<kconf_id> {}
	public static class T_HELPTEXT extends SymbolValue<String> {}
	public static class T_IF extends SymbolValue<kconf_id> {}
	public static class T_IMPLY extends SymbolValue<kconf_id> {}
	public static class T_MAINMENU extends SymbolValue<kconf_id> {}
	public static class T_MENUCONFIG extends SymbolValue<kconf_id> {}
	public static class T_MENU extends SymbolValue<kconf_id> {}
	public static class T_ON extends SymbolValue<kconf_id> {}
	public static class T_OPT_DEFCONFIG_LIST extends SymbolValue<kconf_id> {}
	public static class T_OPT_ENV extends SymbolValue<kconf_id> {}
	public static class T_OPTIONAL extends SymbolValue<kconf_id> {}
	public static class T_OPTION extends SymbolValue<kconf_id> {}
	public static class T_OPT_MODULES extends SymbolValue<kconf_id> {}
	public static class T_PROMPT extends SymbolValue<kconf_id> {}
	public static class T_RANGE extends SymbolValue<kconf_id> {}
	public static class T_SELECT extends SymbolValue<kconf_id> {}
	public static class T_SOURCE extends SymbolValue<kconf_id> {}
	public static class T_TYPE extends SymbolValue<kconf_id> {}
	public static class T_VISIBLE extends SymbolValue<kconf_id> {}
	public static class T_WORD extends SymbolValue<String> {}
	public static class T_WORD_QUOTE extends SymbolValue<String> {}

	public static class comment extends SymbolValue<String> {}
	public static class depends extends SymbolValue<Expr> {}
	public static class depends_list extends SymbolValue<ArrayList<Expr>> {}
	public static class end extends SymbolValue<kconf_id> {}
	public static class expr extends SymbolValue<Expr> {}
	public static class help extends SymbolValue<String> {}
	public static class if_expr extends SymbolValue<Expr> {}
	public static class mainmenu_stmt extends SymbolValue<String> {}
	public static class menu extends SymbolValue<String> {}
	public static class prompt extends SymbolValue<String> {}
	public static class symbol extends SymbolValue<Symbol> {}
	public static class symbol_option_arg extends SymbolValue<String> {}
	public static class visible extends SymbolValue<Expr> {}
	public static class visibility_list extends SymbolValue<ArrayList<Expr>> {}
	public static class word_opt extends SymbolValue<String> {}

	private Tree tree;

	@SuppressWarnings("unused")
	public LKCParserSpec(final Tree tree) {
		this.tree = tree;

		precedences(left(T_EOL), /* part of a hack to resolve shift/reduce conflicts */
					nonassoc(T_NOT),
					left(T_GREATER_EQUAL),
					left(T_GREATER),
					left(T_LESS_EQUAL),
					left(T_LESS),
					left(T_UNEQUAL),
					left(T_EQUAL),
					left(T_AND),
					left(T_OR),
					left(T_MAINMENU));

		grammar(prod(input,
					rhs(nl, kstart),
					rhs(kstart)),

				prod(kstart,
						rhs(mainmenu_stmt, stmt_list),
						rhs(stmt_list)),

				prod(stmt_list,
						rhs(),
						rhs(stmt_list, common_stmt),
						rhs(stmt_list, choice_stmt),
						rhs(stmt_list, menu_stmt),
						rhs(stmt_list, mainmenu_stmt),
						new Action() {
							public void a(String mainmenu_stmt) {
								Results.legacyMainmenuStatement(getCurrentLocation(), mainmenu_stmt);
							}
						}),

				prod(common_stmt,
						rhs(T_EOL),
						rhs(if_stmt),
						rhs(comment_stmt),
						rhs(config_stmt),
						rhs(menuconfig_stmt),
						rhs(source_stmt)),

				/* config/menuconfig entry */

				prod(config_entry_start,
						rhs(T_CONFIG, T_WORD, T_EOL),
						new Action() {
							public void a(kconf_id t_config, String t_word) {
								Symbol sym = sym_lookup(t_word, 0, getCurrentLocation());

								sym.setFlags(sym.getFlags() | Symbol.SYMBOL.OPTIONAL);
								ConfigBlock.next(zconf_lineno(), sym, tree);
							}
						}),

				prod(config_stmt,
						rhs(config_entry_start, config_option_list)),

				prod(menuconfig_entry_start,
						rhs(T_MENUCONFIG, T_WORD, T_EOL),
						new Action() {
							public void a(kconf_id t_menuconfig, String t_word) {
								Symbol sym = sym_lookup(t_word, 0, getCurrentLocation());

								sym.setFlags(sym.getFlags() | Symbol.SYMBOL.OPTIONAL);
								ConfigBlock.next(zconf_lineno(), sym, tree);
							}
						}),

				prod(menuconfig_stmt,
						rhs(menuconfig_entry_start, config_option_list)),

				prod(config_option_list,
						rhs(),
						rhs(config_option_list, config_option),
						rhs(config_option_list, symbol_option),
						rhs(config_option_list, depends),
						new Action() {
							public void a(Expr depends) {
								DescribingBlock.addDependsOn(depends);
							}
						},
						rhs(config_option_list, help),
						new Action() {
							public void a(String help) {
								DescribingBlock.setHelpText(help);
							}
						},
						prec(rhs(config_option_list, T_EOL), T_EOL)),

				prod(config_option,
						rhs(T_TYPE, prompt_stmt_opt, T_EOL),
						new Action() {
							public void a(kconf_id t_type) {
								switch (t_type) {
								case BOOL:
								case BOOLEAN:
									Results.bool++;
									break;
								case TRISTATE:
									Results.tristate++;
									break;
								case STRING:
									Results.string++;
									break;
								case INT:
									Results.intType++;
									break;
								case HEX:
									Results.hex++;
									break;
								default:
									throw new RuntimeException("Unexpected type");
								}
								DescribingBlock.setType(t_type.stype);
							}
						}),

				prod(config_option,
						rhs(T_PROMPT, prompt, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_prompt, String prompt, Expr if_expr) {
								ConfigBlock.addPrompt(zconf_lineno(), prompt, if_expr);
								Results.prompt++;
							}
						}),

				prod(config_option,
						rhs(T_DEFAULT, expr, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_default, Expr expr, Expr if_expr) {
								// do not drop, this is important in combination with visibility evaluation
								DescribingBlock.addDefault(zconf_lineno(), expr, if_expr);
								Symbol_type symbol_type = t_default.stype;
								if (symbol_type != Symbol_type.S_UNKNOWN) {
									DescribingBlock.setType(symbol_type);
								}
								if (expr.type == Expr_type.E_SYMBOL) {
									Symbol sym = (Symbol) expr.left;
									if ((sym.getFlags() & SYMBOL.CONST) != 0) {
										if (sym.getName().equals("")) {
											// violates the only assertion in LKC
											// (Is here an assertion the right mean anyway?)
											Results.emtpyStringAsDefault(getCurrentLocation());
										}
									}
								}
								Results.def++;
							}
						}),

				prod(config_option,
						rhs(T_SELECT, T_WORD, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_select, String t_word, Expr if_expr) {
								Symbol sym = sym_lookup(t_word, 0, getCurrentLocation());
								ConfigBlock.addSelect(zconf_lineno(), sym, if_expr);
								Results.select++;
							}
						}),

				prod(config_option,
						rhs(T_IMPLY, T_WORD, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_imply, String t_word, Expr if_expr) {
								Symbol sym = sym_lookup(t_word, 0, getCurrentLocation());
								ConfigBlock.addImply(zconf_lineno(), sym, if_expr);
								Results.imply++;
							}
						}),

				prod(config_option,
						rhs(T_RANGE, symbol, symbol, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_range, Symbol from, Symbol to, Expr if_expr) {
								ConfigBlock.addRange(zconf_lineno(), from, to, if_expr);
								Results.range++;
							}
						}),

				prod(symbol_option,
						rhs(T_OPTION, symbol_option_list, T_EOL)),

				prod(symbol_option_list,
						rhs(),
						rhs(symbol_option_list, T_WORD, symbol_option_arg),
						new Action() {
							public void a(String t_word, String symbol_option_arg) {
								ConfigBlock.addOption(t_word, symbol_option_arg);

//								kconf_id id = LKCScanner.kconf_id_lookup(t_word);
//								if (id != null && ((id.flags & TF.TF_OPTION) != 0)) {
//									menu_add_option(id.token, symbol_option_arg, getCurrentLocation());
//								}
							}
						}),

				prod(symbol_option_arg,
						rhs(),
						new Action() {
							public String a() {
								return null;
							}
						},
						rhs(T_EQUAL, prompt),
						new Action() {
							public String a(String prompt) {
								return prompt;
							}
						}),

				prod(choice,
						rhs(T_CHOICE, word_opt, T_EOL),
						new Action() {
							public void a(kconf_id t_choice, String word_opt) {
								Symbol symbol = null;
								ChoiceBlock choiceBlock;
								if (word_opt != null) {
									//symbol = sym_lookup(word_opt, SYMBOL.CHOICE, currentLocation);
									throw new UnsupportedOperationException(
											"Found a choice block associated with a symbol. As "
											+ "of this writing, this does not exist in mainline "
											+ "kernel and is not fully implemented in this program "
											+ "as it is hard to handle it properly.");
								}
								choiceBlock = ChoiceBlock.choiceLookup(zconf_lineno(), symbol);
								ChoiceBlock.push(choiceBlock);
								tree.push(choiceBlock);
							}
						}),

				prod(choice_entry,
						rhs(choice, choice_option_list)),

				prod(choice_end,
						rhs(end),
						new Action() {
							public void a(kconf_id end) {
								assert(end == kconf_id.ENDCHOICE);
								TreeNode node = tree.pop();
								assert(node instanceof ChoiceBlock);
								ChoiceBlock.pop();
							}
						}),

				prod(choice_stmt,
						rhs(choice_entry, choice_block, choice_end)),

				prod(choice_option_list,
						rhs(),
						rhs(choice_option_list, choice_option),
						rhs(choice_option_list, depends),
						new Action() {
							public void a(Expr depends) {
								DescribingBlock.addDependsOn(depends);
							}
						},
						rhs(choice_option_list, help),
						new Action() {
							public void a(String help) {
								DescribingBlock.setHelpText(help);
							}
						},
						rhs(choice_option_list, T_EOL)),

				prod(choice_option,
						rhs(T_PROMPT, prompt, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_prompt, String prompt, Expr if_expr) {
								DescribingBlock.addPrompt(zconf_lineno(), prompt, if_expr);
								Results.prompt++;
							}
						}),

				prod(choice_option,
						rhs(T_TYPE, prompt_stmt_opt, T_EOL),
						new Action () {
							public void a(kconf_id type) {
								DescribingBlock.setType(type.stype);
							}
						}),

				prod(choice_option,
						rhs(T_OPTIONAL, T_EOL),
						new Action() {
							public void a(kconf_id t_optional) {
								ChoiceBlock.setOptional();
								Results.optional++;
							}
						}),

				prod(choice_option,
						rhs(T_DEFAULT, T_WORD, if_expr, T_EOL),
						new Action() {
							public void a(kconf_id t_default, String t_word, Expr if_expr) {
								Symbol sym = sym_lookup(t_word, 0, getCurrentLocation());
								Expr target = Expr.expr_alloc_symbol(sym);
								DescribingBlock.addDefault(zconf_lineno(), target, if_expr);
								Results.def++;
							}
						}),

				prod(choice_block,
						rhs(),
						rhs(choice_block, common_stmt)),

				prod(if_entry,
						rhs(T_IF, expr, nl),
						new Action() {
							public void a(kconf_id t_if, Expr expr) {
								//tree.push(new IfBlock(zconf_lineno(), menu_check_dep(expr)));
								tree.push(new IfBlock(zconf_lineno(), expr));
								Results.ifBlock++;
							}
						}),

				prod(if_end,
						rhs(end),
						new Action() {
							public void a(kconf_id end) {
								assert(end == kconf_id.ENDIF);
								TreeNode node = tree.pop();
								assert (node instanceof IfBlock);
							}
						}),

				prod(if_stmt,
						rhs(if_entry, if_block, if_end)),

				prod(if_block,
						rhs(),
						rhs(if_block, common_stmt),
						rhs(if_block, menu_stmt),
						rhs(if_block, choice_stmt)),

				/* mainmenu entry */

				prod(mainmenu_stmt,
						rhs(T_MAINMENU, prompt, nl),
						new Action() {
							public String a(kconf_id t_mainmenu, String prompt) {
								return prompt;
							}
						}),

				/* menu entry */

				prod(menu,
						rhs(T_MENU, prompt, T_EOL),
						new Action() {
							public String a(kconf_id t_menu, String prompt) {
								return prompt;
							}
						}),

				prod(menu_entry,
						rhs(menu, visibility_list, depends_list),
						new Action() {
							public void a(String menu, ArrayList<Expr> visibility_list, ArrayList<Expr> depends_list) {
								MenuBlock menuBlock = new MenuBlock(zconf_lineno(), menu);
								for (Expr visibility: visibility_list) {
									menuBlock.addVisibleIfConstraint(visibility);
								}
								for (Expr depends: depends_list) {
									menuBlock.addDependsOnConstraint(depends);
								}
								tree.push(menuBlock);
							}
						}),

				prod(menu_end,
						rhs(end),
						new Action() {
							public void a(kconf_id end) {
								assert(end == kconf_id.ENDMENU);
								TreeNode node = tree.pop();
								assert (node instanceof MenuBlock);
							}
						}),

				prod(menu_stmt,
						rhs(menu_entry, menu_block, menu_end)),

				prod(menu_block,
						rhs(),
						rhs(menu_block, common_stmt),
						rhs(menu_block, menu_stmt),
						rhs(menu_block, choice_stmt)),

				prod(source_stmt,
						rhs(T_SOURCE, prompt),
						new Action() {
							public void a(kconf_id t_source, String prompt) {
								// Do not do this here. At least with the 
								// current grammar it should be safe to do
								// this in the scanner
								//zconf_nextfile(prompt);
								tree.add(new SourceDirective(zconf_lineno(), prompt));
							}
						}),

				/* comment entry */

				prod(comment,
						rhs(T_COMMENT, prompt, T_EOL),
						new Action() {
							public String a(kconf_id t_comment, String prompt) {
								return prompt;
							}
						}),

				prod(comment_stmt,
						rhs(comment, depends_list),
						new Action() {
							public void a(String comment, ArrayList<Expr> depends_list) {
								// drop, as we have no use for it
							}
						}),

				/* help option */

				prod(help_start,
						rhs(T_HELP),
						new Action() {
							public void a(kconf_id t_help) {
								// Do not do this here. At least with the 
								// current grammar it should be safe to do
								// this in the scanner
								//zconf_starthelp();
							}
						}),

				prod(help,
						rhs(help_start, T_HELPTEXT),
						new Action() {
							public String a(String t_helptext) {
								return t_helptext;
							}
						}),

				/* depends option */

				prod(depends_list,
						rhs(),
						new Action() {
							public ArrayList<Expr> a() {
								return new ArrayList<Expr>();
							}
						},
						rhs(depends_list, depends),
						new Action() {
							public ArrayList<Expr> a(ArrayList<Expr> depends_list, Expr depends) {
								assert (depends != null);
								assert (depends_list != null);
								depends_list.add(depends);
								return depends_list;
							}
						},
						prec(rhs(depends_list, T_EOL), T_EOL),
						new Action() {
							public ArrayList<Expr> a(ArrayList<Expr> depends_list) {
								return depends_list;
							}
						}),

				prod(depends,
						rhs(T_DEPENDS, T_ON, expr, T_EOL),
						new Action() {
							public Expr a(kconf_id t_depends, kconf_id t_on, Expr expr) {
								//return menu_check_dep(expr);
								Results.dependsOn++;
								return expr;
							}
						}),

				/* visibility option */

				prod(visibility_list,
						rhs(),
						new Action() {
							public ArrayList<Expr> a() {
								return new ArrayList<Expr>();
							}
						},
						rhs(visibility_list, visible),
						new Action() {
							public ArrayList<Expr> a(ArrayList<Expr> visibility_list, Expr visible) {
								visibility_list.add(visible);
								return visibility_list;
							}
						},
						rhs(visibility_list, T_EOL),
						new Action() {
							public ArrayList<Expr> a(ArrayList<Expr> visibility_list) {
								return visibility_list;
							}
						}),

				prod(visible,
						/* this T_EOL is not in the original parser */
						rhs(T_VISIBLE, if_expr, T_EOL),
						new Action() {
							public Expr a(kconf_id t_visible, Expr if_expr) {
								Results.visible++;
								return if_expr;
							}
						}),

				/* prompt statement */

				prod(prompt_stmt_opt,
						rhs(),
						rhs(prompt, if_expr),
						new Action() {
							public void a(String prompt, Expr if_expr) {
								DescribingBlock.addPrompt(zconf_lineno(), prompt, if_expr);
							}
						}),

				prod(prompt,
						rhs(T_WORD),
						new Action() {
							public String a(String t_word) {
								return t_word;
							}
						},
						rhs(T_WORD_QUOTE),
						new Action() {
							public String a(String t_word) {
								return t_word;
							}
						}),

				prod(end,
						rhs(T_ENDMENU, T_EOL),
						new Action() {
							public kconf_id a(kconf_id t_endmenu) {
								return t_endmenu;
							}
						},
						rhs(T_ENDCHOICE, T_EOL),
						new Action() {
							public kconf_id a(kconf_id t_endchoice) {
								return t_endchoice;
							}
						},
						rhs(T_ENDIF, T_EOL),
						new Action() {
							public kconf_id a(kconf_id t_endif) {
								return t_endif;
							}
						}),

				prod(nl,
						rhs(T_EOL),
						rhs(nl, T_EOL)),

				prod(if_expr,
						rhs(),
						new Action() {
							public Expr a() {
								return null;
							}
						},
						rhs(T_IF, expr),
						new Action() {
							public Expr a(kconf_id t_if, Expr expr) {
								Results.ifGuard++;
								return expr;
							}
						}),

				prod(expr,
						rhs(symbol),
						new Action() {
							public Expr a(Symbol symbol) {
								return expr_alloc_symbol(symbol);
							}
						},
						rhs(symbol, T_LESS, symbol),
						new Action() {
							public Expr a(Symbol left, Symbol right) {
								return expr_alloc_comp(E_LTH, left, right);
							}
						},
						rhs(symbol, T_LESS_EQUAL, symbol),
						new Action() {
							public Expr a(Symbol left, Symbol right) {
								return expr_alloc_comp(E_LEQ, left, right);
							}
						},
						rhs(symbol, T_GREATER, symbol),
						new Action() {
							public Expr a(Symbol left, Symbol right) {
								return expr_alloc_comp(E_GTH, left, right);
							}
						},
						rhs(symbol, T_GREATER_EQUAL, symbol),
						new Action() {
							public Expr a(Symbol left, Symbol right) {
								return expr_alloc_comp(E_GEQ, left, right);
							}
						},
						rhs(symbol, T_EQUAL, symbol),
						new Action() {
							public Expr a(Symbol left, Symbol right) {
								return expr_alloc_comp(E_EQUAL, left, right);
							}
						},
						rhs(symbol, T_UNEQUAL, symbol),
						new Action() {
							public Expr a(Symbol left, Symbol right) {
								return expr_alloc_comp(E_UNEQUAL, left, right);
							}
						},
						rhs(T_OPEN_PAREN, expr, T_CLOSE_PAREN),
						new Action() {
							public Expr a(Expr expr) {
								return expr;
							}
						},
						rhs(T_NOT, expr),
						new Action() {
							public Expr a(Expr expr) {
								return expr_alloc_one(E_NOT, expr);
							}
						},
						rhs(expr, T_OR, expr),
						new Action() {
							public Expr a(Expr left, Expr right) {
								return expr_alloc_two(E_OR, left, right);
							}
						},

						rhs(expr, T_AND, expr),
						new Action() {
							public Expr a(Expr left, Expr right) {
								return expr_alloc_two(E_AND, left, right);
							}
						}),

				prod(symbol,
						rhs(T_WORD),
						new Action() {
							public Symbol a(String t_word) {
								return sym_lookup(t_word, 0, getCurrentLocation());
							}
						},
						rhs(T_WORD_QUOTE),
						new Action() {
							public Symbol a(String t_word) {
								return sym_lookup(t_word, SYMBOL.CONST, getCurrentLocation());
							}
						}),

				prod(word_opt,
						rhs(),
						new Action() {
							public String a() {
								return null;
							}
						},
						rhs(T_WORD),
						new Action() {
							public String a(String t_word) {
								return t_word;
							}
						})
			);
	}

	protected Location getCurrentLocation() {
		return new Location(tree.getMySourceFile(), zconf_lineno());
	}
}
