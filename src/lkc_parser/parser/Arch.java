package lkc_parser.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lkc_parser.Settings;
import lkc_parser.attributesmodel.AttributesModel;
import lkc_parser.attributesmodel.AttributesModelFactory;
import lkc_parser.attributesmodel.TypesMap;
import lkc_parser.hardresults.Results;
import lkc_parser.kerneltools.Linux;
import lkc_parser.parser.data.Symbol;
import lkc_parser.propositional.PFormulaFactory;
import lkc_parser.propositional.PPOF;
import lkc_parser.propositional.PVariable;
import lkc_parser.propositional.PVariableDatabase;
import lkc_parser.propositional.export.DIMACSExporter;
import lkc_parser.propositional.export.DIMACSExporter.ProblemType;
import lkc_parser.tristatestar.TriSPOF;
import lkc_parser.tristatestar.TriSPOFFactory;
import lkc_parser.zenglermodel2010.Type;
import lkc_parser.zenglermodel2010.ZenglerModel;
import lkc_parser.zenglermodel2010.ZenglerModelFactory;

public class Arch implements Comparable<Arch> {
	private static String kernelVersionString;
	private final String name;
	private final File entryPoint;
	private final HashMap<String, HashSet<String>> envValues = new HashMap<String, HashSet<String>>();
	private String modulesSymName;

	public Arch(String name, File entryPoint) {
		HashSet<String> subarch = new HashSet<String>();
		HashSet<String> arch = new HashSet<String>();
		HashSet<String> srcarch = new HashSet<String>();
		HashSet<String> kernelversion = new HashSet<String>();

		this.name = name;
		this.entryPoint = entryPoint;

		subarch.add(name);
		arch.addAll(subarch);
		srcarch.add(name);
		kernelversion.add(kernelVersionString);

		if (name.equals("x86")) {
			arch.add("i386");
			arch.add("x86_64");
		} else if (name.equals("sparc")) {
			arch.add("sparc32");
			arch.add("sparc64");
		} else if (name.equals("sh")) {
			arch.add("sh64");
		} else if (name.equals("tile")) {
			arch.add("tilepro");
			arch.add("tilegx");
		} else if (name.equals("um")) {
			subarch.add("i386");
			subarch.add("x86");
			subarch.add("x86_64");
		}

		this.envValues.put("SUBARCH", subarch);
		this.envValues.put("ARCH", arch);
		this.envValues.put("SRCARCH", srcarch);
		this.envValues.put("KERNELVERSION", kernelversion);
	}

	public static ArrayList<String> getAvailableArchesNames() {
		ArrayList<String> res = new ArrayList<String>(64);
		File[] directoryListing = new File(Settings.getBasePath(), "arch").listFiles();
		if ((directoryListing != null) && (directoryListing.length > 0)) {
			// drop .gitignore and maybe other artifacts
			// Hopefully, there will never be an architecture named starting
			// with a dot ('.').
			for (File f : directoryListing) {
				if ((f.getName().charAt(0) != '.') && (f.isDirectory())) {
					res.add(f.getName());
				}
			}
		}
		return res;
	}

	public static void setKernelVersionString(String newKernelVersionString) {
		kernelVersionString = newKernelVersionString;
	}

	public static String getKernelVersionString() {
		return kernelVersionString;
	}

	public static boolean parse4_2Predicates() {
		int[] version = Linux.kernelVersionSplit(kernelVersionString);
		return (Linux.compareVersions(new int[] { 4, 2 }, version) >= 0);
	}

	public String getName() {
		return name;
	}

	public File getEntryPoint() {
		return entryPoint;
	}

	public String getModulesSymName() {
		if (modulesSymName == null) {
			modulesSymName = "MODULES";
		}
		return modulesSymName;
	}

	public void setModulesSymName(String modulesSymName) {
		if (this.modulesSymName != null) {
			throw new RuntimeException(
					"modulesSym already set or earlier accessed and therefore set to default value \"MODULES\"");
		}
		this.modulesSymName = modulesSymName;
	}

	// TODO: Bring more structure into this. Much of this does not belong here.
	public void calculateAll() throws IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException, IOException {

		final ZenglerModel zenglerModel;
		final AttributesModel attributesModel;
		final TypesMap typesMap;
		final TriSPOF tpof;
		final PFormulaFactory pPOFFactory;
		final PPOF ppof;
		final PVariableDatabase triSToPropVarTable;
		final Results results = Results.registerArch(this);
		// reduce memory footprint and do not initialize before needed
		final DIMACSExporter dimacsExporter;
		final LinkedHashMap<String, Integer> dimacsTable;
		final BufferedWriter output;

		Symbol.reset();
		zenglerModel = ZenglerModelFactory.produce(this);
		attributesModel = AttributesModelFactory.produce(zenglerModel);
		typesMap = attributesModel.getTypesMap();
		tpof = TriSPOFFactory.produce(attributesModel);
		pPOFFactory = new PFormulaFactory(attributesModel.getTypesMap());
		ppof = pPOFFactory.toPPOF(tpof);
		triSToPropVarTable = pPOFFactory.getvDB();

		dimacsExporter = new DIMACSExporter(ProblemType.GENERIC);
		dimacsExporter.exportPPOF(ppof.getFormula(), name + ".sat");
		dimacsTable = dimacsExporter.getVariablesTable();

		results.evaluateZenglerModel(zenglerModel);
		results.evaluateAttributesModel(attributesModel);
		results.evaluateTPOF(tpof);
		results.evaluatePPOF(ppof);

		output = new BufferedWriter(new FileWriter(name + ".csv"));

		for (Entry<String, Type> entry : typesMap.entrySet()) {
			final String symbolName = entry.getKey();
			final Type symbolType = entry.getValue();

			if ((symbolType == Type.TRISTATE) || (symbolType == Type.BOOL)) {
				final PVariable p0 = triSToPropVarTable.getP0(symbolName);
				final PVariable p1 = triSToPropVarTable.getP1(symbolName);

				output.write(symbolName + "," + symbolType + "," + p0 + ","
						+ dimacsTable.get(p0.name) + "\n");
				output.write(symbolName + "," + symbolType + "," + p1 + ","
						+ dimacsTable.get(p1.name) + "\n");
			}
		}
		output.close();
	}

	public String expandPath(String path) {
		String res;
		Pattern pattern = Pattern.compile("\\$[\\p{Alnum}_]*");
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			final String group = matcher.group();
			final String symbolIdentifier = group.substring(1);

			if (!symbolIdentifier.equals("SRCARCH")) {
				throw new IllegalArgumentException(
						"Found an unexpected substitution in a source statement.");
			}

			res = path.replace(group, name);

			if (pattern.matcher(res).find()) {
				throw new IllegalArgumentException(
						"Fatal condition: found more than one or a recursive substitution in a source statement. However, this program is dumb and can not handle it. Fix me and re-run the program.");
			}
		} else {
			res = path;
		}
		return res;
	}

	public HashSet<String> getValuesForEnv(String envName) {
		HashSet<String> res = envValues.get(envName);
		if (res == null) {
			throw new RuntimeException("environment variable \"" + envName + "\" not implemented");
		}

		return res;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Arch o) {
		return name.compareTo(o.name);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arch other = (Arch) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
