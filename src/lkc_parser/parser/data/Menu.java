package lkc_parser.parser.data;

import static lkc_parser.parser.data.Expr.expr_alloc_and;
import static lkc_parser.parser.data.Expr.expr_alloc_symbol;
import static lkc_parser.parser.data.Symbol.symbol_mod;
import static lkc_parser.parser.data.Symbol.symbol_no;

public class Menu {
	// static struct expr *menu_check_dep(struct expr *e)
	// {
	// if (!e)
	// return e;
	//
	// switch (e->type) {
	// case E_NOT:
	// e->left.expr = menu_check_dep(e->left.expr);
	// break;
	// case E_OR:
	// case E_AND:
	// e->left.expr = menu_check_dep(e->left.expr);
	// e->right.expr = menu_check_dep(e->right.expr);
	// break;
	// case E_SYMBOL:
	// /* change 'm' into 'm' && MODULES */
	// if (e->left.sym == &symbol_mod)
	// return expr_alloc_and(e, expr_alloc_symbol(modules_sym));
	// break;
	// default:
	// break;
	// }
	// return e;
	// }

	public static Expr menu_check_dep(Expr e, Symbol modules_sym) {
		switch (e.type) {
		case E_NOT:
			e.left = menu_check_dep((Expr) e.left, modules_sym);
			break;
		case E_OR:
		case E_AND:
			e.left = menu_check_dep((Expr) e.left, modules_sym);
			e.right = menu_check_dep((Expr) e.right, modules_sym);
			break;
		case E_SYMBOL:
			/* change 'm' into 'm' && MODULES */
			if ((Symbol) e.left == symbol_mod) {
				if (modules_sym == symbol_no) {
					throw new RuntimeException();
				}
				return expr_alloc_and(e, expr_alloc_symbol(modules_sym));
			}
			break;
		default:
			break;
		}
		return e;
	}

	// void menu_add_option(int token, char *arg)
	// {
	// switch (token) {
	// case T_OPT_MODULES:
	// if (modules_sym)
	// zconf_error("symbol '%s' redefines option 'modules'"
	// " already defined by symbol '%s'",
	// current_entry->sym->name,
	// modules_sym->name
	// );
	// modules_sym = current_entry->sym;
	// break;
	// case T_OPT_DEFCONFIG_LIST:
	// if (!sym_defconfig_list)
	// sym_defconfig_list = current_entry->sym;
	// else if (sym_defconfig_list != current_entry->sym)
	// zconf_error("trying to redefine defconfig symbol");
	// break;
	// case T_OPT_ENV:
	// prop_add_env(arg);
	// break;
	// case T_OPT_ALLNOCONFIG_Y:
	// current_entry->sym->flags |= SYMBOL_ALLNOCONFIG_Y;
	// break;
	// }
	// }
}
