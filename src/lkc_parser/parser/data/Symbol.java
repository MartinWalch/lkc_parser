package lkc_parser.parser.data;

import java.util.ArrayList;
import java.util.HashMap;

import lkc_parser.tokentree.Location;

public class Symbol implements Expr_data, Comparable<Symbol> {
	public enum SYMBOL {
		;
		public static final int CONST = 0x0001; /* symbol is const */
		public static final int CHECK = 0x0008; /*
												 * used during dependency
												 * checking
												 */
		public static final int CHOICE = 0x0010; /*
												 * start of a choice block (null
												 * name)
												 */
		public static final int CHOICEVAL = 0x0020; /*
													 * used as a value in a
													 * choice block
													 */
		public static final int VALID = 0x0080; /*
												 * set when symbol.curr is
												 * calculated
												 */
		public static final int OPTIONAL = 0x0100; /*
													 * choice is optional -
													 * values can be 'n'
													 */
		public static final int WRITE = 0x0200; /* ? */
		public static final int CHANGED = 0x0400; /* ? */
		public static final int AUTO = 0x1000; /*
												 * value from environment
												 * variable
												 */
		public static final int CHECKED = 0x2000; /*
												 * used during dependency
												 * checking
												 */
		public static final int WARNED = 0x8000; /* warning has been issued */

		/* Set when symbol.def[] is used */
		public static final int DEF = 0x10000; /* First bit of DEF */
		public static final int DEF_USER = 0x10000; /*
													 * symbol.def[S_DEF_USER] is
													 * valid
													 */
		public static final int DEF_AUTO = 0x20000; /*
													 * symbol.def[S_DEF_AUTO] is
													 * valid
													 */
		public static final int DEF3 = 0x40000; /* symbol.def[S_DEF_3] is valid */
		public static final int DEF4 = 0x80000; /* symbol.def[S_DEF_4] is valid */

		/* choice values need to be set before calculating this symbol value */
		public static final int SYMBOL_NEED_SET_CHOICE_VALUES = 0x100000;

		/* Set symbol to y if allnoconfig; used for symbols that hide others */
		public static final int SYMBOL_ALLNOCONFIG_Y = 0x200000;

	}

	enum tristate {
		no, mod, yes;
	}

	private final static HashMap<String, ArrayList<Location>> variableSymbolsOccurrences = new HashMap<String, ArrayList<Location>>();
	private final static HashMap<String, ArrayList<Location>> constantSymbolsOccurrences = new HashMap<String, ArrayList<Location>>();

	// ArrayList, most times with size 1. There may be different Symbols with
	// the same name.
	private final static HashMap<String, ArrayList<Symbol>> symbol_hash = new HashMap<String, ArrayList<Symbol>>();

	// struct symbol *next;
	private String name;
	// private Symbol_type type;
	// private symbol_value curr;
	// struct symbol_value def[S_DEF_COUNT];
	// tristate visible;
	private int flags;
	// struct property *prop;
	// struct expr_value dir_dep;
	// struct expr_value rev_dep;

	public static final Symbol symbol_yes = new Symbol();
	public static final Symbol symbol_mod = new Symbol();
	public static final Symbol symbol_no = new Symbol();
	public static final Symbol symbol_empty = new Symbol();

	static {
		symbol_yes.name = "y";
		// symbol_yes.curr = symbol_yes.new symbol_value("y", tristate.yes);
		symbol_yes.flags = SYMBOL.CONST | SYMBOL.VALID;

		symbol_mod.name = "m";
		// symbol_mod.curr = symbol_mod.new symbol_value("m", tristate.mod);
		symbol_mod.flags = SYMBOL.CONST | SYMBOL.VALID;

		symbol_no.name = "n";
		// symbol_no.curr = symbol_no.new symbol_value("n", tristate.no);
		symbol_no.flags = SYMBOL.CONST | SYMBOL.VALID;

		symbol_empty.name = "";
		// symbol_empty.curr = symbol_empty.new symbol_value("", tristate.no);
		symbol_empty.flags = SYMBOL.VALID;
	}

	public static void reset() {
		symbol_hash.clear();
	}

	private static boolean isXDigit(byte character) {
		return (Character.isDigit(character) || (('a' <= character) && (character <= 'f')) || (('A' <= character) && (character <= 'F')));
	}

	public static boolean looksLikeInt(String string) {
		final byte[] strB = string.getBytes();
		int pos = 0;
		char ch;

		if (strB.length == 0) {
			return false;
		}
		ch = (char) strB[pos++];
		if (ch == '-') {
			if (strB.length == 1) {
				return false;
			}
			ch = (char) strB[pos++];
		}
		if (!Character.isDigit(ch))
			return false;
		if (ch == '0' && (strB.length > pos))
			return false;
		while (pos < strB.length) {
			if (!Character.isDigit(strB[pos++]))
				return false;
		}
		return true;
	}

	public static boolean looksLikeHexWithoutPrefix(String string) {
		return (looksLikeHex("0x" + string));
	}

	public static boolean looksLikeHex(String string) {
		final byte[] strB = string.getBytes();
		int pos = 0;

		if ((strB.length > 2) && (strB[0] == '0') && ((strB[1] == 'x') || (strB[1] == 'X'))) {
			pos += 2;
		}

		if (pos >= strB.length) {
			return false;
		}

		do {
			if (!isXDigit(strB[pos++]))
				return false;
		} while (pos < strB.length);
		return true;
	}

	public static boolean looksLikeConstant(String string) {
		final byte[] strB = string.getBytes();

		for (byte b : strB) {
			if (b == '.') {
				return true;
			}
		}

		return looksMuchLikeNumber(string);
	}

	public static boolean looksMuchLikeNumber(String string) {
		return looksLikeInt(string) || (looksLikeHex(string) && !looksLikeHexWithoutPrefix(string));
	}

	/**
	 * 
	 * @param name
	 *            The name of the symbol to look up, just as the char* in the
	 *            original configuration system.
	 * @param flags
	 *            Flags that are grouped in an int variable. This is adapted
	 *            exactly from the original Linux kernel configuration system.
	 * @return
	 */
	public static Symbol sym_lookup(String name, int flags, Location location) {
		Symbol symbol;
		String new_name;
		ArrayList<Symbol> list;

		if (name != null) {
			if (name.length() == 1) {
				switch (name.charAt(0)) {
				case 'y':
					return symbol_yes;
				case 'm':
					return symbol_mod;
				case 'n':
					return symbol_no;
				}
			}

			if (symbol_hash.containsKey(name)) {
				list = symbol_hash.get(name);
				for (Symbol s : list) {
					if (flags != 0) {
						if ((s.flags & flags) != 0) {
							return sym_lookupReturn(s, location);
						}
					} else if ((s.flags & (SYMBOL.CONST | SYMBOL.CHOICE)) == 0) {
						return sym_lookupReturn(s, location);
					}
				}
			}
			new_name = name;
		} else {
			new_name = null;
		}

		symbol = new Symbol();
		symbol.name = new_name;
		// symbol.type = S_UNKNOWN;
		symbol.flags |= flags;

		if (!symbol_hash.containsKey(name)) {
			symbol_hash.put(name, new ArrayList<Symbol>(2));
		}
		list = symbol_hash.get(name);
		list.add(0, symbol);

		return sym_lookupReturn(symbol, location);
	}

	private static Symbol sym_lookupReturn(Symbol symbol, Location location) {
		assert ((symbol != null) && (location != null));

		final String name = symbol.name;

		if ((name != null) && ((symbol.flags & SYMBOL.CONST) == 0)) {
			if ((symbol.flags & SYMBOL.CONST) == 0) {
				if (!variableSymbolsOccurrences.containsKey(name)) {
					variableSymbolsOccurrences.put(name, new ArrayList<Location>());
				}
				variableSymbolsOccurrences.get(name).add(location);
			} else {
				if (!constantSymbolsOccurrences.containsKey(name)) {
					constantSymbolsOccurrences.put(name, new ArrayList<Location>());
				}
				constantSymbolsOccurrences.get(name).add(location);
			}
		}

		return symbol;
	}

	public static HashMap<String, ArrayList<Location>> getVariableSymbolsOccurrences() {
		return variableSymbolsOccurrences;
	}

	public static HashMap<String, ArrayList<Location>> getConstantSymbolsOccurrences() {
		return constantSymbolsOccurrences;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public int compareTo(Symbol o) {
		int res = name.compareTo(o.name);
		if (res == 0) {
			return flags - o.flags;
		}
		return res;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + flags;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Symbol other = (Symbol) obj;
		if (flags != other.flags)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
