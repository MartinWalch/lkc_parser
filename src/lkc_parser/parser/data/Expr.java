package lkc_parser.parser.data;

import static lkc_parser.parser.data.Expr_type.E_AND;
import static lkc_parser.parser.data.Expr_type.E_EQUAL;
import static lkc_parser.parser.data.Expr_type.E_GEQ;
import static lkc_parser.parser.data.Expr_type.E_GTH;
import static lkc_parser.parser.data.Expr_type.E_LEQ;
import static lkc_parser.parser.data.Expr_type.E_LTH;
import static lkc_parser.parser.data.Expr_type.E_NOT;
import static lkc_parser.parser.data.Expr_type.E_OR;
import static lkc_parser.parser.data.Expr_type.E_SYMBOL;
import static lkc_parser.parser.data.Expr_type.E_UNEQUAL;
import static lkc_parser.parser.data.Symbol.symbol_mod;
import static lkc_parser.parser.data.Symbol.symbol_no;
import static lkc_parser.parser.data.Symbol.symbol_yes;

import java.util.HashMap;

import lkc_parser.parser.data.Symbol.SYMBOL;
import lkc_parser.zenglermodel2010.Type;


public final class Expr implements Expr_data {
	private static class ExprP {
		private Expr e;

		ExprP(Expr e) {
			this.e = e;
		}

		Expr_data get() {
			return e;
		}

		void set(Expr e) {
			this.e = e;
		}
	}

	private static int trans_count;

	public Expr_type type;
	public Expr_data left, right;

	private Expr(Expr_type type, Expr_data left, Expr_data right) {
		this.type = type;
		this.left = left;
		this.right = right;
	}

	public Expr(Expr e) {
		this(e.type, e.left, e.right);
	}

	public static final Expr TRUE = expr_alloc_symbol(symbol_yes);
	public static final Expr FALSE = expr_alloc_symbol(symbol_no);

	public static Expr expr_alloc_symbol(Symbol sym) {
		Expr res = new Expr(E_SYMBOL, sym, null);
		return res;
	}

	public static Expr expr_alloc_one(Expr_type type, Expr ce) {
		Expr res = new Expr(type, ce, null);
		return res;
	}

	public static Expr expr_alloc_two(Expr_type type, Expr e1, Expr e2) {
		Expr res = new Expr(type, e1, e2);
		return res;
	}

	public static Expr expr_alloc_comp(Expr_type type, Symbol s1, Symbol s2) {
		Expr res = new Expr(type, s1, s2);
		return res;
	}

	public static Expr expr_alloc_and(Expr e1, Expr e2) {
		if (e1 == null) {
			return e2;
		}
		return (e2 != null) ? expr_alloc_two(E_AND, e1, e2) : e1;
	}

	private static Expr expr_alloc_or(Expr e1, Expr e2) {
		if (e1 == null) {
			return e2;
		}
		return (e2 != null) ? expr_alloc_two(E_OR, e1, e2) : e1;
	}

	private static Expr expr_copy(Expr org) {
		Expr e;

		if (org == null) {
			return null;
		}

		e = new Expr(org);
		switch (org.type) {
		case E_SYMBOL:
			e.left = org.left;
			break;
		case E_NOT:
			e.left = expr_copy((Expr) org.left);
			break;
		case E_EQUAL:
		case E_GEQ:
		case E_GTH:
		case E_LEQ:
		case E_LTH:
		case E_UNEQUAL:
			e.left = (Symbol) org.left;
			e.right = (Symbol) org.right;
			break;
		case E_AND:
		case E_OR:
		case E_LIST:
			e.left = expr_copy((Expr) org.left);
			e.right = expr_copy((Expr) org.right);
			break;
		default:
			System.err.println("can't copy type " + e.type + " \n");
			// free(e);
			e = null;
			break;
		}

		return e;
	}

//	static void __expr_eliminate_eq(enum expr_type type, struct expr **ep1, struct expr **ep2)
//	{
//		if (e1->type == type) {
//			__expr_eliminate_eq(type, &e1->left.expr, &e2);
//			__expr_eliminate_eq(type, &e1->right.expr, &e2);
//			return;
//		}
//		if (e2->type == type) {
//			__expr_eliminate_eq(type, &e1, &e2->left.expr);
//			__expr_eliminate_eq(type, &e1, &e2->right.expr);
//			return;
//		}
//		if (e1->type == E_SYMBOL && e2->type == E_SYMBOL &&
//		    e1->left.sym == e2->left.sym &&
//		    (e1->left.sym == &symbol_yes || e1->left.sym == &symbol_no))
//			return;
//		if (!expr_eq(e1, e2))
//			return;
//		trans_count++;
//		expr_free(e1); expr_free(e2);
//		switch (type) {
//		case E_OR:
//			e1 = expr_alloc_symbol(&symbol_no);
//			e2 = expr_alloc_symbol(&symbol_no);
//			break;
//		case E_AND:
//			e1 = expr_alloc_symbol(&symbol_yes);
//			e2 = expr_alloc_symbol(&symbol_yes);
//			break;
//		default:
//			;
//		}
//	}

	private static void __expr_eliminate_eq(Expr_type type, ExprP ep1, ExprP ep2) {
		Expr e1 = (Expr) ep1.get();
		Expr e2 = (Expr) ep2.get();

		if (e1.type == type) {
			// TODO: can we be sure that there is no other pointer in LKC that
			// here becomes stale?
			ExprP e1LeftP = new ExprP((Expr) ((Expr) ep1.get()).left);
			ExprP e1RightP;
			__expr_eliminate_eq(type, e1LeftP, ep2);
			e1.left = e1LeftP.get();
			e1RightP = new ExprP((Expr) ((Expr) ep1.get()).right);
			__expr_eliminate_eq(type, e1RightP, ep2);
			e1.right = e1RightP.get();
			return;
		}
		if (e2.type == type) {
			ExprP e2LeftP = new ExprP((Expr) ((Expr) ep2.get()).left);
			ExprP e2RightP;
			__expr_eliminate_eq(type, ep1, e2LeftP);
			e2.left = e2LeftP.get();
			e2RightP = new ExprP((Expr) ((Expr) ep1.get()).right);
			__expr_eliminate_eq(type, ep1, e2RightP);
			e2.right = e2RightP.get();
			return;
		}
		if (e1.type == E_SYMBOL && e2.type == E_SYMBOL && (Symbol) e1.left == (Symbol) e2.left
				&& ((Symbol) e1.left == symbol_yes || (Symbol) e1.left == symbol_no))
			return;
		if (!expr_eq(e1, e2))
			return;
		trans_count++;

		switch (type) {
		case E_OR:
			ep1.set(expr_alloc_symbol(symbol_no));
			ep2.set(expr_alloc_symbol(symbol_no));
			break;
		case E_AND:
			ep1.set(expr_alloc_symbol(symbol_yes));
			ep2.set(expr_alloc_symbol(symbol_yes));
			break;
		default:
		}
	}

	
//	void expr_eliminate_eq(struct expr **ep1, struct expr **ep2)
//	{
//		if (!e1 || !e2)
//			return;
//		switch (e1->type) {
//		case E_OR:
//		case E_AND:
//			__expr_eliminate_eq(e1->type, ep1, ep2);
//		default:
//			;
//		}
//		if (e1->type != e2->type) switch (e2->type) {
//		case E_OR:
//		case E_AND:
//			__expr_eliminate_eq(e2->type, ep1, ep2);
//		default:
//			;
//		}
//		e1 = expr_eliminate_yn(e1);
//		e2 = expr_eliminate_yn(e2);
//	}
	private static void expr_eliminate_eq(ExprP ep1, ExprP ep2) {
		Expr e1 = (Expr) ep1.get();
		Expr e2 = (Expr) ep2.get();

		if (e1 == null || e2 == null)
			return;
		switch (e1.type) {
		case E_OR:
		case E_AND:
			__expr_eliminate_eq(e1.type, ep1, ep2);
			e1 = (Expr) ep1.get();
			e2 = (Expr) ep2.get();
		default:
		}
		// TODO: update e1Type?
		if (e1.type != e2.type) {
			switch (e2.type) {
			case E_OR:
			case E_AND:
				__expr_eliminate_eq(e2.type, ep1, ep2);
				e1 = (Expr) ep1.get();
				e2 = (Expr) ep2.get();
			default:
			}
		}
		ep1.set(expr_eliminate_yn(e1));
		ep2.set(expr_eliminate_yn(e2));
	}

//	int expr_eq(struct expr *e1, struct expr *e2)
//	{
//		int res, old_count;
//
//		if (e1->type != e2->type)
//			return 0;
//		switch (e1->type) {
//		case E_EQUAL:
//		case E_UNEQUAL:
//			return e1->left.sym == e2->left.sym && e1->right.sym == e2->right.sym;
//		case E_SYMBOL:
//			return e1->left.sym == e2->left.sym;
//		case E_NOT:
//			return expr_eq(e1->left.expr, e2->left.expr);
//		case E_AND:
//		case E_OR:
//			e1 = expr_copy(e1);
//			e2 = expr_copy(e2);
//			old_count = trans_count;
//			expr_eliminate_eq(&e1, &e2);
//			res = (e1->type == E_SYMBOL && e2->type == E_SYMBOL &&
//			       e1->left.sym == e2->left.sym);
//			expr_free(e1);
//			expr_free(e2);
//			trans_count = old_count;
//			return res;
//		case E_LIST:
//		case E_RANGE:
//		case E_NONE:
//			/* panic */;
//		}
//
//		if (DEBUG_EXPR) {
//			expr_fprint(e1, stdout);
//			printf(" = ");
//			expr_fprint(e2, stdout);
//			printf(" ?\n");
//		}
//
//		return 0;
//	}
	private static boolean expr_eq(Expr e1, Expr e2) {
		ExprP ep1, ep2;
		boolean res;
		int old_count;

		if (e1.type != e2.type)
			return false;
		switch (e1.type) {
		case E_EQUAL:
		case E_GEQ:
		case E_GTH:
		case E_LEQ:
		case E_LTH:
		case E_UNEQUAL:
			return (((Symbol)e1.left == (Symbol)e2.left) && ((Symbol)e1.right == (Symbol)e2.right));
		case E_SYMBOL:
			return ((Symbol) e1.left == (Symbol)e2.left);
		case E_NOT:
			return expr_eq((Expr) e1.left, (Expr) e2.left);
		case E_AND:
		case E_OR:
			e1 = expr_copy(e1);
			e2 = expr_copy(e2);
			old_count = trans_count;
			ep1= new ExprP(e1);
			ep2= new ExprP(e2);
			expr_eliminate_eq(ep1, ep2);
			e1 = (Expr) ep1.get();
			e2 = (Expr) ep2.get();

			res = (e1.type == E_SYMBOL && e2.type == E_SYMBOL &&
			       (Symbol) e1.left == (Symbol) e2.left);
			trans_count = old_count;
			return res;
		case E_LIST:
		case E_RANGE:
		case E_NONE:
			/* panic */
		}

//		if (DEBUG_EXPR) {
//			expr_fprint(e1, stdout);
//			printf(" = ");
//			expr_fprint(e2, stdout);
//			printf(" ?\n");
//		}

		return false;
	}


//	struct expr *expr_eliminate_yn(struct expr *e)
//	{
//		struct expr *tmp;
//
//		if (e) switch (e->type) {
//		case E_AND:
//			e->left.expr = expr_eliminate_yn(e->left.expr);
//			e->right.expr = expr_eliminate_yn(e->right.expr);
//			if (e->left.expr->type == E_SYMBOL) {
//				if (e->left.expr->left.sym == &symbol_no) {
//					expr_free(e->left.expr);
//					expr_free(e->right.expr);
//					e->type = E_SYMBOL;
//					e->left.sym = &symbol_no;
//					e->right.expr = NULL;
//					return e;
//				} else if (e->left.expr->left.sym == &symbol_yes) {
//					free(e->left.expr);
//					tmp = e->right.expr;
//					*e = *(e->right.expr);  //??? TODO: why not tmp = e; e = e->right.expr;?
//					free(tmp);
//					return e;
//				}
//			}
//			if (e->right.expr->type == E_SYMBOL) {
//				if (e->right.expr->left.sym == &symbol_no) {
//					expr_free(e->left.expr);
//					expr_free(e->right.expr);
//					e->type = E_SYMBOL;
//					e->left.sym = &symbol_no;
//					e->right.expr = NULL;
//					return e;
//				} else if (e->right.expr->left.sym == &symbol_yes) {
//					free(e->right.expr);
//					tmp = e->left.expr;
//					*e = *(e->left.expr);
//					free(tmp);
//					return e;
//				}
//			}
//			break;
//		case E_OR:
//			e->left.expr = expr_eliminate_yn(e->left.expr);
//			e->right.expr = expr_eliminate_yn(e->right.expr);
//			if (e->left.expr->type == E_SYMBOL) {
//				if (e->left.expr->left.sym == &symbol_no) {
//					free(e->left.expr);
//					tmp = e->right.expr;
//					*e = *(e->right.expr);
//					free(tmp);
//					return e;
//				} else if (e->left.expr->left.sym == &symbol_yes) {
//					expr_free(e->left.expr);
//					expr_free(e->right.expr);
//					e->type = E_SYMBOL;
//					e->left.sym = &symbol_yes;
//					e->right.expr = NULL;
//					return e;
//				}
//			}
//			if (e->right.expr->type == E_SYMBOL) {
//				if (e->right.expr->left.sym == &symbol_no) {
//					free(e->right.expr);
//					tmp = e->left.expr;
//					*e = *(e->left.expr);
//					free(tmp);
//					return e;
//				} else if (e->right.expr->left.sym == &symbol_yes) {
//					expr_free(e->left.expr);
//					expr_free(e->right.expr);
//					e->type = E_SYMBOL;
//					e->left.sym = &symbol_yes;
//					e->right.expr = NULL;
//					return e;
//				}
//			}
//			break;
//		default:
//			;
//		}
//		return e;
//	}

	private static Expr expr_eliminate_yn(Expr e) {
		if (e != null) {
			switch (e.type) {
			case E_AND:
				e.left = expr_eliminate_yn((Expr) e.left);
				e.right = expr_eliminate_yn((Expr) e.right);
				if (((Expr) e.left).type == E_SYMBOL) {
					if ((Symbol) ((Expr) e.left).left == symbol_no) {
						e.type = E_SYMBOL;
						e.left = symbol_no;
						e.right = null;
						return e;
					} else if ((Symbol) ((Expr) e.left).left == symbol_yes) {
						return (Expr) e.right;
					}
				}
				if (((Expr) e.right).type == E_SYMBOL) {
					if ((Symbol) ((Expr) e.right).left == symbol_no) {
						e.type = E_SYMBOL;
						e.left = symbol_no;
						e.right = null;
						return e;
					} else if ((Symbol) ((Expr) e.right).left == symbol_yes) {
						return (Expr) e.left;
					}
				}
				break;
			case E_OR:
				e.left = expr_eliminate_yn((Expr) e.left);
				e.right = expr_eliminate_yn((Expr) e.right);
				if (((Expr) e.left).type == E_SYMBOL) {
					if ((Symbol) ((Expr) e.left).left == symbol_no) {
						return (Expr) e.right;
					} else if ((Symbol) ((Expr) e.left).left == symbol_yes) {
						e.type = E_SYMBOL;
						e.left = symbol_yes;
						e.right = null;
						return e;
					}
				}
				if (((Expr) e.right).type == E_SYMBOL) {
					if ((Symbol) ((Expr) e.right).left == symbol_no) {
						return (Expr) e.left;
					} else if ((Symbol) ((Expr) e.right).left == symbol_yes) {
						e.type = E_SYMBOL;
						e.left = symbol_yes;
						e.right = null;
						return e;
					}
				}
				break;
			default:
			}
		}
		return e;
	}
	/*
	 * bool FOO!=n => FOO
	 */
//	struct expr *expr_trans_bool(struct expr *e)
//	{
//		if (!e)
//			return NULL;
//		switch (e->type) {
//		case E_AND:
//		case E_OR:
//		case E_NOT:
//			e->left.expr = expr_trans_bool(e->left.expr);
//			e->right.expr = expr_trans_bool(e->right.expr);
//			break;
//		case E_UNEQUAL:
//			// FOO!=n -> FOO
//			if (e->left.sym->type == S_TRISTATE) {
//				if (e->right.sym == &symbol_no) {
//					e->type = E_SYMBOL;
//					e->right.sym = NULL;
//				}
//			}
//			break;
//		default:
//			;
//		}
//		return e;
//	}
	private static Expr expr_trans_bool(Expr e, HashMap<String, Type> typeMap) {
		if (e == null)
			return null;
		switch (e.type) {
		case E_AND:
		case E_OR:
		case E_NOT:
			e.left = expr_trans_bool((Expr) e.left, typeMap);
			e.right = expr_trans_bool((Expr) e.right, typeMap);
			break;
		case E_UNEQUAL:
			// FOO!=n -> FOO
			if (typeMap.get(((Symbol) e.left).getName()) == Type.TRISTATE) {
				if ((Symbol) e.right == symbol_no) {
					e.type = E_SYMBOL;
					e.right = null;
				}
			}
			break;
		default:
		}
		return e;
	}

	/*
	 * e1 || e2 -> ?
	 */
//	static struct expr *expr_join_or(struct expr *e1, struct expr *e2)
//	{
//		struct expr *tmp;
//		struct symbol *sym1, *sym2;
//
//		if (expr_eq(e1, e2))
//			return expr_copy(e1);
//		if (e1->type != E_EQUAL && e1->type != E_UNEQUAL && e1->type != E_SYMBOL && e1->type != E_NOT)
//			return NULL;
//		if (e2->type != E_EQUAL && e2->type != E_UNEQUAL && e2->type != E_SYMBOL && e2->type != E_NOT)
//			return NULL;
//		if (e1->type == E_NOT) {
//			tmp = e1->left.expr;
//			if (tmp->type != E_EQUAL && tmp->type != E_UNEQUAL && tmp->type != E_SYMBOL)
//				return NULL;
//			sym1 = tmp->left.sym;
//		} else
//			sym1 = e1->left.sym;
//		if (e2->type == E_NOT) {
//			if (e2->left.expr->type != E_SYMBOL)
//				return NULL;
//			sym2 = e2->left.expr->left.sym;
//		} else
//			sym2 = e2->left.sym;
//		if (sym1 != sym2)
//			return NULL;
//		if (sym1->type != S_BOOLEAN && sym1->type != S_TRISTATE)
//			return NULL;
//		if (sym1->type == S_TRISTATE) {
//			if (e1->type == E_EQUAL && e2->type == E_EQUAL &&
//			    ((e1->right.sym == &symbol_yes && e2->right.sym == &symbol_mod) ||
//			     (e1->right.sym == &symbol_mod && e2->right.sym == &symbol_yes))) {
//				// (a='y') || (a='m') -> (a!='n')
//				return expr_alloc_comp(E_UNEQUAL, sym1, &symbol_no);
//			}
//			if (e1->type == E_EQUAL && e2->type == E_EQUAL &&
//			    ((e1->right.sym == &symbol_yes && e2->right.sym == &symbol_no) ||
//			     (e1->right.sym == &symbol_no && e2->right.sym == &symbol_yes))) {
//				// (a='y') || (a='n') -> (a!='m')
//				return expr_alloc_comp(E_UNEQUAL, sym1, &symbol_mod);
//			}
//			if (e1->type == E_EQUAL && e2->type == E_EQUAL &&
//			    ((e1->right.sym == &symbol_mod && e2->right.sym == &symbol_no) ||
//			     (e1->right.sym == &symbol_no && e2->right.sym == &symbol_mod))) {
//				// (a='m') || (a='n') -> (a!='y')
//				return expr_alloc_comp(E_UNEQUAL, sym1, &symbol_yes);
//			}
//		}
//		if (sym1->type == S_BOOLEAN && sym1 == sym2) {
//			if ((e1->type == E_NOT && e1->left.expr->type == E_SYMBOL && e2->type == E_SYMBOL) ||
//			    (e2->type == E_NOT && e2->left.expr->type == E_SYMBOL && e1->type == E_SYMBOL))
//				return expr_alloc_symbol(&symbol_yes);
//		}
//
//		if (DEBUG_EXPR) {
//			printf("optimize (");
//			expr_fprint(e1, stdout);
//			printf(") || (");
//			expr_fprint(e2, stdout);
//			printf(")?\n");
//		}
//		return NULL;
//	}

	private static Expr expr_join_or(Expr e1, Expr e2, HashMap<String, Type> typeMap) {
		Expr tmp;
		Symbol sym1, sym2;

		if (expr_eq(e1, e2))
			return expr_copy(e1);
		if (e1.type != E_EQUAL && e1.type != E_UNEQUAL && e1.type != E_SYMBOL && e1.type != E_NOT)
			return null;
		if (e2.type != E_EQUAL && e2.type != E_UNEQUAL && e2.type != E_SYMBOL && e2.type != E_NOT)
			return null;
		if (e1.type == E_NOT) {
			tmp = (Expr) e1.left;
			if (tmp.type != E_EQUAL && tmp.type != E_UNEQUAL && tmp.type != E_SYMBOL)
				return null;
			sym1 = (Symbol) tmp.left;
		} else {
			sym1 = (Symbol) e1.left;
		}
		if (e2.type == E_NOT) {
			if (((Expr) e2.left).type != E_SYMBOL)
				return null;
			sym2 = (Symbol) ((Expr) e2.left).left;
		} else
			sym2 = (Symbol) e2.left;
		if (sym1 != sym2)
			return null;
		if (typeMap.get(sym1.getName()) != Type.BOOL && typeMap.get(sym1.getName()) != Type.TRISTATE)
			return null;
		if (typeMap.get(sym1.getName()) == Type.TRISTATE) {
			if (e1.type == E_EQUAL && e2.type == E_EQUAL &&
			    (((Symbol)e1.right == symbol_yes && (Symbol) e2.right == symbol_mod) ||
			     ((Symbol)e1.right == symbol_mod && (Symbol) e2.right == symbol_yes))) {
				// (a='y') || (a='m') -> (a!='n')
				return expr_alloc_comp(E_UNEQUAL, sym1, symbol_no);
			}
			if (e1.type == E_EQUAL && e2.type == E_EQUAL &&
			    (((Symbol) e1.right == symbol_yes && (Symbol) e2.right == symbol_no) ||
			     ((Symbol) e1.right == symbol_no && (Symbol) e2.right == symbol_yes))) {
				// (a='y') || (a='n') -> (a!='m')
				return expr_alloc_comp(E_UNEQUAL, sym1, symbol_mod);
			}
			if (e1.type == E_EQUAL && e2.type == E_EQUAL &&
			    (((Symbol) e1.right == symbol_mod && (Symbol) e2.right == symbol_no) ||
			     ((Symbol) e1.right == symbol_no && (Symbol) e2.right == symbol_mod))) {
				// (a='m') || (a='n') -> (a!='y')
				return expr_alloc_comp(E_UNEQUAL, sym1, symbol_yes);
			}
		}
		if (typeMap.get(sym1.getName()) == Type.BOOL && sym1 == sym2) {
			if ((e1.type == E_NOT && ((Expr) e1.left).type == E_SYMBOL && e2.type == E_SYMBOL) ||
			    (e2.type == E_NOT && ((Expr) e2.left).type == E_SYMBOL && e1.type == E_SYMBOL))
				return expr_alloc_symbol(symbol_yes);
		}

		return null;
	}

//	static struct expr *expr_join_and(struct expr *e1, struct expr *e2)
//	{
//		struct expr *tmp;
//		struct symbol *sym1, *sym2;
//
//		if (expr_eq(e1, e2))
//			return expr_copy(e1);
//		if (e1->type != E_EQUAL && e1->type != E_UNEQUAL && e1->type != E_SYMBOL && e1->type != E_NOT)
//			return NULL;
//		if (e2->type != E_EQUAL && e2->type != E_UNEQUAL && e2->type != E_SYMBOL && e2->type != E_NOT)
//			return NULL;
//		if (e1->type == E_NOT) {
//			tmp = e1->left.expr;
//			if (tmp->type != E_EQUAL && tmp->type != E_UNEQUAL && tmp->type != E_SYMBOL)
//				return NULL;
//			sym1 = tmp->left.sym;
//		} else
//			sym1 = e1->left.sym;
//		if (e2->type == E_NOT) {
//			if (e2->left.expr->type != E_SYMBOL)
//				return NULL;
//			sym2 = e2->left.expr->left.sym;
//		} else
//			sym2 = e2->left.sym;
//		if (sym1 != sym2)
//			return NULL;
//		if (sym1->type != S_BOOLEAN && sym1->type != S_TRISTATE)
//			return NULL;
//
//		if ((e1->type == E_SYMBOL && e2->type == E_EQUAL && e2->right.sym == &symbol_yes) ||
//		    (e2->type == E_SYMBOL && e1->type == E_EQUAL && e1->right.sym == &symbol_yes))
//			// (a) && (a='y') -> (a='y')
//			return expr_alloc_comp(E_EQUAL, sym1, &symbol_yes);
//
//		if ((e1->type == E_SYMBOL && e2->type == E_UNEQUAL && e2->right.sym == &symbol_no) ||
//		    (e2->type == E_SYMBOL && e1->type == E_UNEQUAL && e1->right.sym == &symbol_no))
//			// (a) && (a!='n') -> (a)
//			return expr_alloc_symbol(sym1);
//
//		if ((e1->type == E_SYMBOL && e2->type == E_UNEQUAL && e2->right.sym == &symbol_mod) ||
//		    (e2->type == E_SYMBOL && e1->type == E_UNEQUAL && e1->right.sym == &symbol_mod))
//			// (a) && (a!='m') -> (a='y')
//			return expr_alloc_comp(E_EQUAL, sym1, &symbol_yes);
//
//		if (sym1->type == S_TRISTATE) {
//			if (e1->type == E_EQUAL && e2->type == E_UNEQUAL) {
//				// (a='b') && (a!='c') -> 'b'='c' ? 'n' : a='b'
//				sym2 = e1->right.sym;
//				if ((e2->right.sym->flags & SYMBOL_CONST) && (sym2->flags & SYMBOL_CONST))
//					return sym2 != e2->right.sym ? expr_alloc_comp(E_EQUAL, sym1, sym2)
//								     : expr_alloc_symbol(&symbol_no);
//			}
//			if (e1->type == E_UNEQUAL && e2->type == E_EQUAL) {
//				// (a='b') && (a!='c') -> 'b'='c' ? 'n' : a='b'
//				sym2 = e2->right.sym;
//				if ((e1->right.sym->flags & SYMBOL_CONST) && (sym2->flags & SYMBOL_CONST))
//					return sym2 != e1->right.sym ? expr_alloc_comp(E_EQUAL, sym1, sym2)
//								     : expr_alloc_symbol(&symbol_no);
//			}
//			if (e1->type == E_UNEQUAL && e2->type == E_UNEQUAL &&
//				   ((e1->right.sym == &symbol_yes && e2->right.sym == &symbol_no) ||
//				    (e1->right.sym == &symbol_no && e2->right.sym == &symbol_yes)))
//				// (a!='y') && (a!='n') -> (a='m')
//				return expr_alloc_comp(E_EQUAL, sym1, &symbol_mod);
//
//			if (e1->type == E_UNEQUAL && e2->type == E_UNEQUAL &&
//				   ((e1->right.sym == &symbol_yes && e2->right.sym == &symbol_mod) ||
//				    (e1->right.sym == &symbol_mod && e2->right.sym == &symbol_yes)))
//				// (a!='y') && (a!='m') -> (a='n')
//				return expr_alloc_comp(E_EQUAL, sym1, &symbol_no);
//
//			if (e1->type == E_UNEQUAL && e2->type == E_UNEQUAL &&
//				   ((e1->right.sym == &symbol_mod && e2->right.sym == &symbol_no) ||
//				    (e1->right.sym == &symbol_no && e2->right.sym == &symbol_mod)))
//				// (a!='m') && (a!='n') -> (a='y') // TODO: report typo (was (a='m'))
//				return expr_alloc_comp(E_EQUAL, sym1, &symbol_yes);
//
//			if ((e1->type == E_SYMBOL && e2->type == E_EQUAL && e2->right.sym == &symbol_mod) ||
//			    (e2->type == E_SYMBOL && e1->type == E_EQUAL && e1->right.sym == &symbol_mod) ||
//			    (e1->type == E_SYMBOL && e2->type == E_UNEQUAL && e2->right.sym == &symbol_yes) ||
//			    (e2->type == E_SYMBOL && e1->type == E_UNEQUAL && e1->right.sym == &symbol_yes))
//				return NULL;
//		}
//
//		if (DEBUG_EXPR) {
//			printf("optimize (");
//			expr_fprint(e1, stdout);
//			printf(") && (");
//			expr_fprint(e2, stdout);
//			printf(")?\n");
//		}
//		return NULL;
//	}

	private static Expr expr_join_and(Expr e1, Expr e2, HashMap<String, Type> typeMap) {
		Expr tmp;
		Symbol sym1, sym2;

		if (expr_eq(e1, e2))
			return expr_copy(e1);
		if (e1.type != E_EQUAL && e1.type != E_UNEQUAL && e1.type != E_SYMBOL && e1.type != E_NOT)
			return null;
		if (e2.type != E_EQUAL && e2.type != E_UNEQUAL && e2.type != E_SYMBOL && e2.type != E_NOT)
			return null;
		if (e1.type == E_NOT) {
			tmp = (Expr) e1.left;
			if (tmp.type != E_EQUAL && tmp.type != E_UNEQUAL && tmp.type != E_SYMBOL)
				return null;
			sym1 = (Symbol) tmp.left;
		} else {
			sym1 = (Symbol) e1.left;
		}
		if (e2.type == E_NOT) {
			if (((Expr) e2.left).type != E_SYMBOL) {
				return null;
			}
			sym2 = (Symbol) ((Expr) e2.left).left;
		} else
			sym2 = (Symbol) e2.left;
		if (sym1 != sym2) {
			return null;
		}
		if (typeMap.get(sym1.getName()) != Type.BOOL
				&& typeMap.get(sym1.getName()) != Type.TRISTATE) {
			return null;
		}

		if ((e1.type == E_SYMBOL && e2.type == E_EQUAL && (Symbol) e2.right == symbol_yes)
				|| (e2.type == E_SYMBOL && e1.type == E_EQUAL && (Symbol) e1.right == symbol_yes))
			// (a) && (a='y') -> (a='y')
			return expr_alloc_comp(E_EQUAL, sym1, symbol_yes);

		if ((e1.type == E_SYMBOL && e2.type == E_UNEQUAL && (Symbol) e2.right == symbol_no)
				|| (e2.type == E_SYMBOL && e1.type == E_UNEQUAL && (Symbol) e1.right == symbol_no))
			// (a) && (a!='n') -> (a)
			return expr_alloc_symbol(sym1);

		if ((e1.type == E_SYMBOL && e2.type == E_UNEQUAL && (Symbol) e2.right == symbol_mod)
				|| (e2.type == E_SYMBOL && e1.type == E_UNEQUAL && (Symbol) e1.right == symbol_mod))
			// (a) && (a!='m') -> (a='y')
			return expr_alloc_comp(E_EQUAL, sym1, symbol_yes);

		if (typeMap.get(sym1.getName()) == Type.TRISTATE) {
			if (e1.type == E_EQUAL && e2.type == E_UNEQUAL) {
				// (a='b') && (a!='c') -> 'b'='c' ? 'n' : a='b'
				sym2 = (Symbol) e1.right;
				if (((((Symbol) e2.right).getFlags() & SYMBOL.CONST) != 0)
						&& ((sym2.getFlags() & SYMBOL.CONST) != 0))
					return sym2 != (Symbol) e2.right ? expr_alloc_comp(E_EQUAL, sym1, sym2)
							: expr_alloc_symbol(symbol_no);
			}
			if (e1.type == E_UNEQUAL && e2.type == E_EQUAL) {
				// (a='b') && (a!='c') -> 'b'='c' ? 'n' : a='b'
				sym2 = (Symbol) e2.right;
				if (((((Symbol) e1.right).getFlags() & SYMBOL.CONST) != 0)
						&& ((sym2.getFlags() & SYMBOL.CONST) != 0))
					return sym2 != (Symbol) e1.right ? expr_alloc_comp(E_EQUAL, sym1, sym2)
							: expr_alloc_symbol(symbol_no);
			}
			if (e1.type == E_UNEQUAL
					&& e2.type == E_UNEQUAL
					&& (((Symbol) e1.right == symbol_yes && (Symbol) e2.right == symbol_no) || ((Symbol) e1.right == symbol_no && (Symbol) e2.right == symbol_yes)))
				// (a!='y') && (a!='n') -> (a='m')
				return expr_alloc_comp(E_EQUAL, sym1, symbol_mod);

			if (e1.type == E_UNEQUAL
					&& e2.type == E_UNEQUAL
					&& (((Symbol) e1.right == symbol_yes && (Symbol) e2.right == symbol_mod) || ((Symbol) e1.right == symbol_mod && (Symbol) e2.right == symbol_yes)))
				// (a!='y') && (a!='m') -> (a='n')
				return expr_alloc_comp(E_EQUAL, sym1, symbol_no);

			if (e1.type == E_UNEQUAL
					&& e2.type == E_UNEQUAL
					&& (((Symbol) e1.right == symbol_mod && (Symbol) e2.right == symbol_no) || ((Symbol) e1.right == symbol_no && (Symbol) e2.right == symbol_mod)))
				// (a!='m') && (a!='n') -> (a='m')
				return expr_alloc_comp(E_EQUAL, sym1, symbol_yes);

			if ((e1.type == E_SYMBOL && e2.type == E_EQUAL && (Symbol) e2.right == symbol_mod)
					|| (e2.type == E_SYMBOL && e1.type == E_EQUAL && (Symbol) e1.right == symbol_mod)
					|| (e1.type == E_SYMBOL && e2.type == E_UNEQUAL && (Symbol) e2.right == symbol_yes)
					|| (e2.type == E_SYMBOL && e1.type == E_UNEQUAL && (Symbol) e1.right == symbol_yes))
				return null;
		}

		return null;
	}

//	static void expr_eliminate_dups1(enum expr_type type, struct expr **ep1, struct expr **ep2)
//	{
//	#define e1 (*ep1)
//	#define e2 (*ep2)
//		struct expr *tmp;
//
//		printf("ep2    : %lu\n", ep2);
//		printf("&(*ep2): %lu\n", &(*ep2));
//		printf("ep1    : %lu\n", ep1);
//		printf("&(*ep1): %lu\n", &(*ep1));
//
//		
//		if (e1->type == type) {
//			expr_eliminate_dups1(type, &e1->left.expr, &e2);
//			expr_eliminate_dups1(type, &e1->right.expr, &e2);
//			return;
//		}
//		if (e2->type == type) {
//			expr_eliminate_dups1(type, &e1, &e2->left.expr);
//			expr_eliminate_dups1(type, &e1, &e2->right.expr);
//			return;
//		}
//		if (e1 == e2)
//			return;
//
//		switch (e1->type) {
//		case E_OR: case E_AND:
//			expr_eliminate_dups1(e1->type, &e1, &e1);
//		default:
//			;
//		}
//
//		switch (type) {
//		case E_OR:
//			tmp = expr_join_or(e1, e2);
//			if (tmp) {
//				expr_free(e1); expr_free(e2);
//				e1 = expr_alloc_symbol(&symbol_no);
//				e2 = tmp;
//				trans_count++;
//			}
//			break;
//		case E_AND:
//			tmp = expr_join_and(e1, e2);
//			if (tmp) {
//				expr_free(e1); expr_free(e2);
//				e1 = expr_alloc_symbol(&symbol_yes);
//				e2 = tmp;
//				trans_count++;
//			}
//			break;
//		default:
//			;
//		}
//	#undef e1
//	#undef e2
//	}

	private static void expr_eliminate_dups1(Expr_type type, ExprP ep1, ExprP ep2,
			HashMap<String, Type> typeMap) {
		Expr tmp;
		Expr e1 = (Expr) ep1.get();
		Expr e2 = (Expr) ep2.get();

		if (e1.type == type) {
			ExprP e1LeftP = new ExprP((Expr) ((Expr) ep1.get()).left);
			ExprP e1RightP;

			expr_eliminate_dups1(type, e1LeftP, ep2, typeMap);
			e1.left = e1LeftP.get();

			e1RightP = new ExprP((Expr) ((Expr) ep1.get()).right);
			expr_eliminate_dups1(type, e1RightP, ep2, typeMap);
			e1.right = e1RightP.get();

			return;
		}
		if (e2.type == type) {
			ExprP e2LeftP = new ExprP((Expr) ((Expr) ep2.get()).left);
			ExprP e2RightP;

			expr_eliminate_dups1(type, ep1, e2LeftP, typeMap);
			e2.left = e2LeftP.get();

			e2RightP = new ExprP((Expr) ((Expr) ep2.get()).right);
			expr_eliminate_dups1(type, ep1, e2RightP, typeMap);
			e2.right = e2RightP.get();
			return;
		}
		if (e1 == e2)
			return;

		switch (e1.type) {
		case E_OR:
		case E_AND:
			expr_eliminate_dups1(e1.type, ep1, ep1, typeMap); // TODO: will this work?
			e1 = (Expr) ep1.get();
		default:
		}

		switch (type) {
		case E_OR:
			tmp = expr_join_or(e1, e2, typeMap);
			if (tmp != null) {
				ep1.set(expr_alloc_symbol(symbol_no));
				ep2.set(tmp);
				trans_count++;
			}
			break;
		case E_AND:
			tmp = expr_join_and(e1, e2, typeMap);
			if (tmp != null) {
				ep1.set(expr_alloc_symbol(symbol_yes));
				ep2.set(tmp);
				trans_count++;
			}
			break;
		default:
		}
	}

	private static Expr expr_eliminate_dups(Expr e, HashMap<String, Type> typeMap) {
		int oldcount;
		if (e == null)
			return e;

		oldcount = trans_count;
		while (true) {
			trans_count = 0;
			switch (e.type) {
			case E_OR:
			case E_AND:
				ExprP ep = new ExprP(e);
				expr_eliminate_dups1(e.type, ep, ep, typeMap);
			default:
			}
			if (trans_count == 0)
				break;
			e = expr_eliminate_yn(e);
		}
		trans_count = oldcount;
		return e;
	}

	private static Expr expr_transform(Expr e, HashMap<String, Type> typeMap) {
		Expr tmp;
		Expr res = e;
		Symbol leftSym, rightSym;
		Expr leftExpr;

		if (res == null)
			return null;
		switch (res.type) {
		case E_EQUAL:
		case E_GEQ:
		case E_GTH:
		case E_LEQ:
		case E_LTH:
		case E_UNEQUAL:
		case E_SYMBOL:
		case E_LIST:
			break;
		default:
			res.left = expr_transform((Expr) res.left, typeMap);
			res.right = expr_transform((Expr) res.right, typeMap);
		}

		switch (res.type) {
		case E_EQUAL:
			assert (res.left instanceof Symbol);
			assert (res.right instanceof Symbol);
			leftSym = (Symbol) res.left;
			rightSym = (Symbol) res.right;
			if (typeMap.get(leftSym.getName()) != Type.BOOL)
				break;
			if (rightSym == symbol_no) {
				res.type = E_NOT;
				res.left = expr_alloc_symbol(leftSym);
				res.right = null;
				break;
			}
			if (rightSym == symbol_mod) {
				res.type = E_SYMBOL;
				res.left = symbol_no;
				res.right = null;
				break;
			}
			if (rightSym == symbol_yes) {
				res.type = E_SYMBOL;
				res.right = null;
				break;
			}
			break;
		case E_UNEQUAL:
			leftSym = (Symbol) res.left;
			rightSym = (Symbol) res.right;
			if (typeMap.get(leftSym.getName()) != Type.BOOL)
				break;
			if (rightSym == symbol_no) {
				res.type = E_SYMBOL;
				res.right = null;
				break;
			}
			if (rightSym == symbol_mod) {
				res.type = E_SYMBOL;
				res.left = symbol_yes;
				res.right = null;
				break;
			}
			if (rightSym == symbol_yes) {
				res.type = E_NOT;
				res.left = expr_alloc_symbol(leftSym);
				res.right = null;
				break;
			}
			break;
		case E_NOT:
			leftExpr = (Expr) res.left;
			switch (leftExpr.type) {
			case E_NOT:
				// !!a -> a
				res = (Expr) leftExpr.left;
				res = expr_transform(e, typeMap);
				break;
			case E_EQUAL:
			case E_UNEQUAL:
				// !a='x' -> a!='x'
				res = leftExpr;
				res.type = (res.type == E_EQUAL) ? E_UNEQUAL : E_EQUAL;
				break;
			case E_LEQ:
			case E_GEQ:
				// !a<='x' -> a>'x'
				res = leftExpr;
				res.type = (res.type == E_LEQ) ? E_GTH : E_LTH;
				break;
			case E_LTH:
			case E_GTH:
				// !a<'x' -> a>='x'
				res = leftExpr;
				res.type = (res.type == E_LTH) ? E_GEQ : E_LEQ;
				break;
			case E_OR:
				// !(a || b) -> !a && !b
				tmp = leftExpr;
				res.type = E_AND;
				res.right = expr_alloc_one(E_NOT, (Expr) tmp.right);
				tmp.type = E_NOT;
				tmp.right = null;
				res = expr_transform(e, typeMap);
				break;
			case E_AND:
				// !(a && b) -> !a || !b
				tmp = leftExpr;
				res.type = E_OR;
				res.right = expr_alloc_one(E_NOT, (Expr) tmp.right);
				tmp.type = E_NOT;
				tmp.right = null;
				res = expr_transform(res, typeMap);
				break;
			case E_SYMBOL:
				if (leftExpr.left == symbol_yes) {
					// !'y' -> 'n'
					res = leftExpr;
					res.type = E_SYMBOL;
					res.left = symbol_no;
					break;
				}
				if (leftExpr.left == symbol_mod) {
					// !'m' -> 'm'
					res = leftExpr;
					res.type = E_SYMBOL;
					res.left = symbol_mod;
					break;
				}
				if (leftExpr.left == symbol_no) {
					// !'n' -> 'y'
					res = leftExpr;
					res.type = E_SYMBOL;
					res.left = symbol_yes;
					break;
				}
				break;
			default:
			}
			break;
		default:
		}
		return res;
	}

//	int expr_contains_symbol(struct expr *dep, struct symbol *sym)
//	{
//		if (!dep)
//			return 0;
//
//		switch (dep->type) {
//		case E_AND:
//		case E_OR:
//			return expr_contains_symbol(dep->left.expr, sym) ||
//			       expr_contains_symbol(dep->right.expr, sym);
//		case E_SYMBOL:
//			return dep->left.sym == sym;
//		case E_EQUAL:
//		case E_UNEQUAL:
//			return dep->left.sym == sym ||
//			       dep->right.sym == sym;
//		case E_NOT:
//			return expr_contains_symbol(dep->left.expr, sym);
//		default:
//			;
//		}
//		return 0;
//	}

	private static int expr_contains_symbol(Expr dep, Symbol sym) {
		if (dep == null)
			return 0;

		switch (dep.type) {
		case E_AND:
		case E_OR:
			return (expr_contains_symbol((Expr) dep.left, sym) != 0 || expr_contains_symbol(
					(Expr) dep.right, sym) != 0) ? 1 : 0;
		case E_SYMBOL:
			return (dep.left == sym) ? 1 : 0;
		case E_EQUAL:
		case E_GEQ:
		case E_GTH:
		case E_LEQ:
		case E_LTH:
		case E_UNEQUAL:
			return (dep.left == sym || dep.right == sym) ? 1 : 0;
		case E_NOT:
			return expr_contains_symbol((Expr) dep.left, sym);
		default:
		}
		return 0;
	}

//	bool expr_depends_symbol(struct expr *dep, struct symbol *sym)
//	{
//		if (!dep)
//			return false;
//
//		switch (dep->type) {
//		case E_AND:
//			return expr_depends_symbol(dep->left.expr, sym) ||
//			       expr_depends_symbol(dep->right.expr, sym);
//		case E_SYMBOL:
//			return dep->left.sym == sym;
//		case E_EQUAL:
//			if (dep->left.sym == sym) {
//				if (dep->right.sym == &symbol_yes || dep->right.sym == &symbol_mod)
//					return true;
//			}
//			break;
//		case E_UNEQUAL:
//			if (dep->left.sym == sym) {
//				if (dep->right.sym == &symbol_no)
//					return true;
//			}
//			break;
//		default:
//			;
//		}
//	 	return false;
//	}

	private boolean expr_depends_symbol(Expr dep, Symbol sym) {
		if (dep == null)
			return false;

		switch (dep.type) {
		case E_AND:
			return expr_depends_symbol((Expr) dep.left, sym)
					|| expr_depends_symbol((Expr) dep.right, sym);
		case E_SYMBOL:
			return dep.left == sym;
		case E_EQUAL:
			if (dep.left == sym) {
				if (dep.right == symbol_yes || dep.right == symbol_mod)
					return true;
			}
			break;
		case E_UNEQUAL:
			if (dep.left == sym) {
				if (dep.right == symbol_no)
					return true;
			}
			break;
		default:
		}
		return false;
	}
}
