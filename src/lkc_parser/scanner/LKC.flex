package lkc_parser.scanner;

import static lkc_parser.parser.LKCParserSpec.Terminals.*;
import static lkc_parser.scanner.TF.*;

import java.util.HashMap;

import lkc_parser.hardresults.Results;
import lkc_parser.parser.Arch;
import lkc_parser.tokentree.SourceFile;
%%

%class LKCScanner
%8bit
%public
%cup2
%line
%column

%init{
	lkcScanner = this;
%init}

%{
	private static LKCScanner lkcScanner;

	private static final HashMap<String, kconf_id> kconf_id_lookup_table;

	private static ScannerToken<?> lastToken;

	private static final int START_STRSIZE = 16;

	private static StringBuilder textBuffer;

	private static int last_ts, first_ts;

	private static int str = 0;
	private static int ts, i;

	private static boolean parse4_2Predicates = Arch.parse4_2Predicates();

	static {
		kconf_id[] values = kconf_id.values();
		kconf_id_lookup_table = new HashMap<String, kconf_id>(values.length * 2);

		for (kconf_id kconfId : values) {
			kconf_id_lookup_table.put(kconfId.name, kconfId);
		}
	}

	static kconf_id kconf_id_lookup(String str) {
		kconf_id res = kconf_id_lookup_table.get(str);
		// System.err.println("(DD) lookup \"" + str + "\"");
		/*if (res != null) {
			switch (res) {
				 case DEFCONFIG_LIST: System.err.println("returning DEFCONFIG_LIST"); break;
				 case MODULES: System.err.println("returning MODULES"); break;
				 case ENV: System.err.println("returning ENV"); break;
				 case ALLNOCONFIG_Y: System.err.println("returning ALLNOCONFIG_Y"); break;
				 default: break;
			}
		}*/
		return res;
	}

	private static void new_string() {
		textBuffer = new StringBuilder(START_STRSIZE);
	}

	private static void append_string(String str, @SuppressWarnings("unused") int size) {
		textBuffer.append(str);
	}

	private static String text() {
		return textBuffer.toString();
	}

	public static void zconf_starthelp() {
		new_string();
		last_ts = 0;
		first_ts = 0;
		debugyybegin(HELP);
	}

	private static void zconf_endhelp() {
		// zconflval.string = text;
		debugyybegin(YYINITIAL);
	}

	private static ScannerToken<Object> symbol(Terminal type) {
		ScannerToken<Object> res = lkcScanner.token(type);
		lastToken = res;
		return res;
	}

	private static <T> ScannerToken<T> symbol(Terminal type, T value) {
		ScannerToken<T> res = lkcScanner.token(type, value);
		lastToken = res;
		return res;
	}

	private static void debugyybegin(int newState) {
		// System.err.println("(DD) Lexer entering state number: " + newState);
  		lkcScanner.yybegin(newState);
	}

	public static int zconf_lineno() {
		return lkcScanner.yyline;
	}
%}

n		= [A-Za-z0-9_]

FlexDot		= [^\n]
WhiteSpace	= [ \t]

%xstate COMMAND
%xstate HELP
%xstate STRING
%xstate PARAM

%xstate EOFDUMMY
%%

<YYINITIAL> {

({WhiteSpace}* "#" {FlexDot}* "\n") |
({WhiteSpace}* "\n") {
				return symbol(T_EOL);
			}
{WhiteSpace}* "#" {FlexDot}* { }

{WhiteSpace}+	{
				debugyybegin(COMMAND);
		}
{FlexDot}	{
				yypushback(1);
				debugyybegin(COMMAND);
		}
<<EOF>>		{
				debugyybegin(EOFDUMMY);
				if ((lastToken == null) || !(lastToken.getSymbol().equals(T_EOL) || lastToken.getSymbol().equals(T_HELPTEXT))) {
					Results.noNewlineAtEOF(SourceFile.getCurrentSourceFile());
				}
				return symbol(T_EOL);
		}
}

// As we do not make recursive CPP style inclusions here, we run into problems
// with files that do not have a trailing newline.
// We fix this with this artificial EOFDUMMY state to get out a last T_EOL before EOF.

<EOFDUMMY> {
	<<EOF>>	{
			return token(SpecialTerminals.EndOfInputStream);
		}
}

<COMMAND> {
	{n}+	{
				kconf_id id = kconf_id_lookup(yytext());
				debugyybegin(PARAM);
				if ((id != null) && ((id.flags & TF_COMMAND) != 0)) {
					if (id == kconf_id.HELP) {
						// LKC does this in the parser
						zconf_starthelp();
					}

					return symbol(id.token, id);
				}
				// System.err.println("(DD) fall through in COMMAND to T_WORD with \"" + yytext() + "\"");
				return symbol(T_WORD, yytext());
			}
	{FlexDot} { }
	"\n"	{
				debugyybegin(YYINITIAL);
				return symbol(T_EOL);
			}
}

<PARAM> {
	"&&"	{ return symbol(T_AND);}
	"||"	{ return symbol(T_OR); }
	"("		{ return symbol(T_OPEN_PAREN); }
	")"		{ return symbol(T_CLOSE_PAREN); }
	"!"		{ return symbol(T_NOT); }
	"="		{ return symbol(T_EQUAL); }
	"!="	{ return symbol(T_UNEQUAL); }
	"<="	{ return (parse4_2Predicates)? symbol(T_LESS_EQUAL): symbol(T_EQUAL); } 
	">="	{ return (parse4_2Predicates)? symbol(T_GREATER_EQUAL): symbol(T_EQUAL); }
	"<"		{ if (parse4_2Predicates) { return symbol(T_LESS); } }
	">"		{ if (parse4_2Predicates) { return symbol(T_GREATER); } }
	("\"") | ("'") {
				str = yytext().charAt(0);
				new_string();
				debugyybegin(STRING);
			}
	"\n"	{
				debugyybegin(YYINITIAL);
				return symbol(T_EOL);
			}
	"---"	{ /* ignore */ }
	({n}|[-/.])+ {
				kconf_id id = kconf_id_lookup(yytext());

				if ((id != null) && ((id.flags & TF_PARAM)!= 0)) {
					return symbol(id.token, id);
				}

				return symbol(T_WORD, yytext());
			}
	"#" {FlexDot}*	{ /* comment */ }
	"\\" "\n" { }
	{FlexDot} { /* drop, usually only whitespace */ }
	<<EOF>>	{ debugyybegin(YYINITIAL); }
}

<STRING> {
	[^'\"\\\n]+ "\n" {
					yypushback(1);
					append_string(yytext(), yylength());
					return symbol(T_WORD_QUOTE, text());
				}
	[^'\"\\\n]+	{
					append_string(yytext(), yylength());
				}
	"\\" {FlexDot}? "\n" {
					yypushback(1);
					append_string(yytext().substring(1), yylength() - 1);
					return symbol(T_WORD_QUOTE, text());
				}
	\\.?			{
					append_string(yytext().substring(1), yylength() - 1);
				}
	"'"|"\""	{
					if (str == yytext().charAt(0)) {
						debugyybegin(PARAM);
						return symbol(T_WORD_QUOTE, text());
					} else
						append_string(yytext(), 1);
				}
	"\n"		{
					debugyybegin(YYINITIAL);
					return symbol(T_EOL);
				}
	<<EOF>>		{
					debugyybegin(YYINITIAL);
				}
}

<HELP> {
	{WhiteSpace}+	{
						ts = 0;
						for (i = 0; i < yylength(); i++) {
							if (yytext().charAt(i) == '\t') {
								ts = (ts & ~7) + 8;
							} else {
								ts++;
							}
						}
						last_ts = ts;
						if (first_ts != 0) {
							if (ts < first_ts) {
								zconf_endhelp();
								return symbol(T_HELPTEXT, text());
							}
							ts -= first_ts;
							while (ts > 8) {
								append_string("        ", 8);
								ts -= 8;
							}
							append_string("        ", ts);
						}
					}
	{WhiteSpace}* "\n" [^ \t\n] {
						yypushback(1);
						zconf_endhelp();
						return symbol(T_HELPTEXT, text());
					}
	{WhiteSpace}* "\n" {
						append_string("\n", 1);
					}
	[^ \t\n] {FlexDot}* {
						int truncatedLength = yylength();
						while (truncatedLength > 0) {
							if ((yytext().charAt(truncatedLength - 1) != ' ') && (yytext().charAt(truncatedLength - 1) != '\t'))
								break;
							truncatedLength--;
						}

						append_string(yytext(), yylength());
						if (first_ts == 0) {
							first_ts = last_ts;
						}
					}
	<<EOF>>			{
						if (textBuffer.charAt(textBuffer.length() - 1) != '\n') {
							Results.noNewlineAtEOF(SourceFile.getCurrentSourceFile());
						}
						zconf_endhelp();
						return symbol(T_HELPTEXT, text());
					}
}
