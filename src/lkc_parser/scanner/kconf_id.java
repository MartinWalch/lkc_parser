package lkc_parser.scanner;

import static lkc_parser.parser.LKCParserSpec.Terminals.*;
import static lkc_parser.parser.data.Symbol_type.*;
import static lkc_parser.scanner.TF.*;

import edu.tum.cup2.grammar.Terminal;
import lkc_parser.parser.data.Symbol_type;

public enum kconf_id {
	MAINMENU("mainmenu", T_MAINMENU, TF_COMMAND),
	MENU("menu", T_MENU, TF_COMMAND),
	ENDMENU("endmenu", T_ENDMENU, TF_COMMAND),
	SOURCE("source", T_SOURCE, TF_COMMAND),
	CHOICE("choice", T_CHOICE, TF_COMMAND),
	ENDCHOICE("endchoice", T_ENDCHOICE, TF_COMMAND),
	COMMENT("comment", T_COMMENT, TF_COMMAND),
	CONFIG("config", T_CONFIG, TF_COMMAND),
	MENUCONFIG("menuconfig", T_MENUCONFIG, TF_COMMAND),
	HELP("help", T_HELP, TF_COMMAND),
	IF("if", T_IF, TF_COMMAND | TF_PARAM),
	ENDIF("endif", T_ENDIF, TF_COMMAND),
	DEPENDS("depends", T_DEPENDS, TF_COMMAND),
	OPTIONAL("optional", T_OPTIONAL, TF_COMMAND),
	DEFAULT("default", T_DEFAULT, TF_COMMAND, S_UNKNOWN),
	PROMPT("prompt", T_PROMPT, TF_COMMAND),
	TRISTATE("tristate", T_TYPE, TF_COMMAND, S_TRISTATE),
	DEF_TRISTATE("def_tristate", T_DEFAULT, TF_COMMAND, S_TRISTATE),
	BOOL("bool", T_TYPE, TF_COMMAND, S_BOOLEAN),
	BOOLEAN("boolean", T_TYPE, TF_COMMAND, S_BOOLEAN),
	DEF_BOOL("def_bool", T_DEFAULT, TF_COMMAND, S_BOOLEAN),
	INT("int", T_TYPE, TF_COMMAND, S_INT),
	HEX("hex", T_TYPE, TF_COMMAND, S_HEX),
	STRING("string", T_TYPE, TF_COMMAND, S_STRING),
	SELECT("select", T_SELECT, TF_COMMAND),
	IMPLY("imply", T_IMPLY, TF_COMMAND),
	RANGE("range", T_RANGE, TF_COMMAND),
	VISIBLE("visible", T_VISIBLE, TF_COMMAND),
	OPTION("option", T_OPTION, TF_COMMAND),
	ON("on", T_ON, TF_PARAM),
	MODULES("modules", T_OPT_MODULES, TF_OPTION),
	DEFCONFIG_LIST("defconfig_list", T_OPT_DEFCONFIG_LIST, TF_OPTION),
	ENV("env", T_OPT_ENV, TF_OPTION),
	ALLNOCONFIG_Y("allnoconfig_y", T_OPT_ALLNOCONFIG_Y, TF_OPTION);

	public final String name;
	public final Terminal token;
	public final int flags;
	public final Symbol_type stype;

	private kconf_id(String name, Terminal token, int flags) {
		this(name, token, flags, S_UNKNOWN);
	}

	private kconf_id(String name, Terminal token, int flags, Symbol_type stype) {
		this.name = name;
		this.token = token;
		this.flags = flags;
		this.stype = stype;
	}
}
